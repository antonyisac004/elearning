<table cellpadding="0" cellspacing="0" class="es-header" align="center" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;background-color:transparent;background-repeat:repeat;background-position:center top;">
    <tbody>
        <tr style="border-collapse:collapse;">
            <td align="center" style="padding:0;Margin:0;">
                <table class="es-header-body" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#fff;" width="600" cellspacing="0" cellpadding="0" bgcolor="#fff" align="center">
                    <tbody>
                        <tr style="border-collapse:collapse;">
                            <td align="left" style="Margin:0;padding-top:15px;padding-bottom:15px;padding-left:35px;padding-right:35px;">
                                <!--[if mso]><table width="530" cellpadding="0" cellspacing="0"><tr><td width="340" valign="top"><![endif]-->
                                <table class="es-left" cellspacing="0" cellpadding="0" align="center" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left;">
                                    <tbody>
                                        <tr style="border-collapse:collapse;">
                                            <td class="es-m-p0r es-m-p20b" width="100%" valign="top" align="center" style="padding:0;Margin:0;">
                                                <table width="100%" cellspacing="0" cellpadding="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                                                    <tbody>
                                                        <tr style="border-collapse:collapse;">
                                                            <td class="es-m-txt-c" align="center" style="padding:0;Margin:0;">
                                                                <img src="{{ asset('storage/media/logo.png') }}" width="200">
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>