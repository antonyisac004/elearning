<table cellpadding="0" cellspacing="0" class="es-content" align="center"
    style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;">
    <tbody>
        <tr style="border-collapse:collapse;">
            <td align="center" style="padding:0;Margin:0;">
                <table class="es-content-body"
                    style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#21ab4c;border-bottom:10px solid #21ab4c;"
                    width="600" cellspacing="0" cellpadding="0" bgcolor="#1b9ba3" align="center">
                    <tbody>
                        <tr style="border-collapse:collapse;">
                            <td align="left" style="padding:0;Margin:0;">
                                <table width="100%" cellspacing="0" cellpadding="0"
                                    style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                                    <tbody>
                                        <tr style="border-collapse:collapse;">
                                            <td width="600" valign="top" align="center" style="padding:0;Margin:0;">
                                                <table width="100%" cellspacing="0" cellpadding="0"
                                                    style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                                                    <tbody>
                                                        <tr style="border-collapse:collapse;">
                                                            <td style="padding:0;Margin:0;">
                                                                <table class="es-menu" width="40%" cellspacing="0"
                                                                    cellpadding="0" align="center"
                                                                    style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                                                                    <tbody>
                                                                        <tr class="links-images-top"
                                                                            style="border-collapse:collapse;">
                                                                            
                                                                            <td style="Margin:0;padding-top:15px;padding-bottom:5px;border:0;"
                                                                                width="15%" bgcolor="transparent"
                                                                                align="center">
                                                                                <a target="_blank"
                                                                                    style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:'open sans', 'helvetica neue', helvetica, arial, sans-serif;font-size:20px;text-decoration:none;display:block;color:#FFFFFF;"
                                                                                    href="#"><img
                                                                                        src="{{ asset('storage/media/facebook.png') }}"
                                                                                        alt="" title="" height="27"
                                                                                        align="absmiddle"
                                                                                        style="display:inline-block !important;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;padding-bottom:5px;"><br></a>
                                                                            </td>

                                                                            <td style="Margin:0;padding-top:15px;padding-bottom:5px;border:0;"
                                                                                width="15%" bgcolor="transparent"
                                                                                align="center">
                                                                                <a target="_blank"
                                                                                    style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:'open sans', 'helvetica neue', helvetica, arial, sans-serif;font-size:20px;text-decoration:none;display:block;color:#FFFFFF;"
                                                                                    href="#"><img
                                                                                        src="{{ asset('storage/media/instagram.png') }}"
                                                                                        alt="" title="" height="27"
                                                                                        align="absmiddle"
                                                                                        style="display:inline-block !important;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;padding-bottom:5px;"><br></a>
                                                                            </td>

                                                                            <td style="Margin:0;padding-top:15px;padding-bottom:5px;border:0;"
                                                                                width="15%" bgcolor="transparent"
                                                                                align="center">
                                                                                <a target="_blank"
                                                                                    style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:'open sans', 'helvetica neue', helvetica, arial, sans-serif;font-size:20px;text-decoration:none;display:block;color:#FFFFFF;"
                                                                                    href="#"><img
                                                                                        src="{{ asset('storage/media/twitter.png') }}"
                                                                                        alt="" title="" height="27"
                                                                                        align="absmiddle"
                                                                                        style="display:inline-block !important;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;padding-bottom:5px;"><br></a>
                                                                            </td>

                                                                            <td style="Margin:0;padding-top:15px;padding-bottom:5px;border:0;"
                                                                                width="15%" bgcolor="transparent"
                                                                                align="center">
                                                                                <a target="_blank"
                                                                                    style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:'open sans', 'helvetica neue', helvetica, arial, sans-serif;font-size:20px;text-decoration:none;display:block;color:#FFFFFF;"
                                                                                    href="#"><img
                                                                                        src="{{ asset('storage/media/youtube.png') }}"
                                                                                        alt="" title="" height="27"
                                                                                        align="absmiddle"
                                                                                        style="display:inline-block !important;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;padding-bottom:5px;"><br></a>
                                                                            </td>
                                                                           
                                                                           
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>