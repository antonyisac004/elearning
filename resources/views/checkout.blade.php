@extends('layouts.app')

@section('content')
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>

                <!-- Page Content -->
                <div class="bg-body-dark bg-pattern" style="background-image: url('<?php echo url('/'); ?>/assets/admin/media/various/bg-pattern-inverse.png');">
                    <div class="row mx-0 justify-content-center">
                        <div class="hero-static col-lg-6 col-xl-12">
                            <div class="content content-full overflow-hidden">
                                <!-- Header -->
                                <div class="py-30 text-center">
                                    <a class="link-effect font-w700" href="#">
        
                                    <span class="font-size-xl text-primary-dark">{{ config('app.name') }}</span>
                                    </a>
                                    <h1 class="h4 font-w700 mt-30 mb-10">Confirm Payment</h1>
                                    
                                    
                                </div>
                                <!-- END Header -->

                                <!-- Sign up Form -->
                               
                                <form class="js-validation-register rzp-footer-form" action="{!!route('dopayment')!!}" method="POST">
                                @csrf
                                    <div class="block block-themed block-rounded block-shadow">
                                        <div class="block-header bg-gd-emerald">
                                            <h3 class="block-title">Confirm Payment</h3>
                                            <div class="block-options">
                                                <button type="button" class="btn-block-option">
                                                <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                                                </button>
                                            </div>
                                        </div>
                                        <div class="block-content">

                                            <div class="form-group row ">
                                                <div class="col-12" >
                                                    <label for="package_id">Package : Gold</label>
                                                    <br>
                                                    <label for="package_id">Amount : $30</label>
                                                    

                                                </div>
                                            </div>
                                            

                                            <div class="form-group row mb-0">

                                                <input type="hidden" name="amount" id="amount" value="30"/>
                                                <div class="pay">
                                                <div class="col-sm-12 text-sm-right push pay">
                                                    <button type="button" class="btn btn-alt-success" id="paybtn" >
                                                        <i class="fa fa-plus mr-10"></i> Confirm Payment
                                                    </button>
                                                </div>
                                                </div>
                                            </div>

                                        </div>
                                           
                                    </div>
                                    
                                </form>
                                <br><br>
                                <div id="paymentDetail" style="display: none">
                                    <center>
                                        <div>paymentID: <span id="paymentID"></span></div>
                                        <div>paymentDate: <span id="paymentDate"></span></div>
                                    </center>
                                </div>
                                <!-- END Sign up Form -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END Page Content -->

            </main>
            <!-- END Main Container -->
        </div>
        <!-- END Page Container -->
@endsection
@section('js_after')
<!-- Razorpay -->


        <script>
        $('.js-validation-register').validate({
                errorClass: 'invalid-feedback animated fadeInDown',
                errorElement: 'div',
                errorPlacement: (error, e) => {
                    $(e).parents('.form-group > div').append(error);
                },
                highlight: e => {
                    $(e).closest('.form-group').removeClass('is-invalid').addClass('is-invalid');
                },
                success: e => {
                    $(e).closest('.form-group').removeClass('is-invalid');
                    $(e).remove();
                },
                rules: {
                    first_name:{
                        required: true, 
                    },
                    last_name:{
                        required: true, 
                    }
                    email: {
                        required: true,
                        email: true
                    },
                    phone: {
                        required: true,
                        minlength: 10
                    },
                    password: {
                        required: true,
                        minlength: 8
                    },
                    password_confirmation: {
                        required: true,
                        minlength: 8
                    }
                },
                messages: {
                    first_name: {
                        required: 'Please enter first name',
                    },
                    last_name: {
                        required: 'Please enter last name',
                    },
                    email: {
                        required: 'Please enter an email address',
                        email: 'Please provide a valid email address'
                    },
                    phone: {
                        required: 'Please enter phone',
                        minlength: 'Your phone number must be at least 10 numbers long'
                    },
                    password: {
                        required: 'Please provide a password',
                        minlength: 'Your password must be at least 8 characters long'
                    }
                    password_confirmation: {
                        required: 'Please confirm password provided above',
                        minlength: 'Your password must be at least 8 characters long'
                    }
                }
            });
    </script>
    <script>
    $('#rzp-footer-form').submit(function (e) {
        var button = $(this).find('button');
        var parent = $(this);
        button.attr('disabled', 'true').html('Please Wait...');
        $.ajax({
            method: 'get',
            url: this.action,
            data: $(this).serialize(),
            complete: function (r) {
                console.log('complete');
                console.log(r);
            }
        })
        return false;
    })
</script>

<script>
    function padStart(str) {
        return ('0' + str).slice(-2)
    }

    function demoSuccessHandler(transaction) {
        // You can write success code here. If you want to store some data in database.
        $("#paymentDetail").removeAttr('style');
        $('#paymentID').text(transaction.razorpay_payment_id);
        var paymentDate = new Date();
        $('#paymentDate').text(
                padStart(paymentDate.getDate()) + '.' + padStart(paymentDate.getMonth() + 1) + '.' + paymentDate.getFullYear() + ' ' + padStart(paymentDate.getHours()) + ':' + padStart(paymentDate.getMinutes())
                );

        $.ajax({
            method: 'post',
            url: "{!!route('dopayment')!!}",
            data: {
                "_token": "{{ csrf_token() }}",
                "razorpay_payment_id": transaction.razorpay_payment_id
            },
            complete: function (r) {
                console.log('complete');
                console.log(r);
            }
        })
    }
</script>
<script>
    var options = {
        key: "{{ env('RAZORPAY_KEY') }}",
        amount: '{{ $package_data->amount}}',
        name: '',
        description: 'TVS Keyboard',
        image: 'https://i.imgur.com/n5tjHFD.png',
        handler: demoSuccessHandler
    }
</script>
<script>
    window.r = new Razorpay(options);
    document.getElementById('paybtn').onclick = function () {
        
        r.open()
    }
</script>
@endsection
    