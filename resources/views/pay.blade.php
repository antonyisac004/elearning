<!DOCTYPE html>
<html lang="en">

<head>

	<!-- META ============================================= -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="keywords" content="" />
	<meta name="author" content="" />
	<meta name="robots" content="" />
	
	<!-- DESCRIPTION -->
	<meta name="description" content="Elearning : Membership| Subscription" />
	
	<!-- OG -->
	<meta property="og:title" content="Elearning : Membership| Subscription" />
	<meta property="og:description" content="Elearning : Membership| Subscription" />
	<meta property="og:image" content="" />
	<meta name="format-detection" content="telephone=no">
	
	<!-- FAVICONS ICON ============================================= -->
	<link rel="icon" href="assets/images/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" type="image/x-icon" href="assets/images/favicon.png" />
	
	<!-- PAGE TITLE HERE ============================================= -->
	<title>Elearning : Membership| Subscription </title>

	
	
	<!-- MOBILE SPECIFIC ============================================= -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<!--[if lt IE 9]>
	<script src="assets/js/html5shiv.min.js"></script>
	<script src="assets/js/respond.min.js"></script>
	<![endif]-->
	
	<!-- All PLUGINS CSS ============================================= -->
	<link rel="stylesheet" type="text/css" href="<?php echo url('/'); ?>/assets/web/css/assets.css">
	
	<!-- TYPOGRAPHY ============================================= -->
	<link rel="stylesheet" type="text/css" href="<?php echo url('/'); ?>/assets/web/css/typography.css">
	
	<!-- SHORTCODES ============================================= -->
	<link rel="stylesheet" type="text/css" href="<?php echo url('/'); ?>/assets/web/css/shortcodes/shortcodes.css">
	
	<!-- STYLESHEETS ============================================= -->
	<link rel="stylesheet" type="text/css" href="<?php echo url('/'); ?>/assets/web/css/style.css">
	<link class="skin" rel="stylesheet" type="text/css" href="<?php echo url('/'); ?>/assets/web/css/color/color-1.css">
	
	

</head>
<body id="bg">
<div class="page-wraper">
<div id="loading-icon-bx"></div>

	<!-- Header Top ==== -->
    <header class="header rs-nav">
		<div class="top-bar">
			<div class="container">
				<div class="row d-flex justify-content-between">
					<div class="topbar-left">
						<ul>
							<li><a href="javascript:;"><i class="fa fa-question-circle"></i>Ask a Question</a></li>
							<li><a href="javascript:;"><i class="fa fa-envelope-o"></i>Support@website.com</a></li>
						</ul>
					</div>
					<div class="topbar-right">
						<ul>
							<!-- <li>
								<select class="header-lang-bx">
									<option data-icon="flag flag-uk">English UK</option>
									<option data-icon="flag flag-us">English US</option>
								</select>
							</li> -->
							<li><a href="{{ route('user.login') }}">Login</a></li>
							<li><a href="{{ route('membership.index') }}">Membership</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="sticky-header navbar-expand-lg">
            <div class="menu-bar clearfix">
                <div class="container clearfix">
					<!-- Header Logo ==== -->
					<div class="menu-logo">
						<a href="<?php echo url('/'); ?>"><img src="<?php echo url('/'); ?>/assets/web/images/logo.png" alt=""></a>
					</div>
					<!-- Mobile Nav Button ==== -->
                    <button class="navbar-toggler collapsed menuicon justify-content-end" type="button" data-toggle="collapse" data-target="#menuDropdown" aria-controls="menuDropdown" aria-expanded="false" aria-label="Toggle navigation">
						<span></span>
						<span></span>
						<span></span>
					</button>
					<!-- Author Nav ==== -->
                    <div class="secondary-menu">
                        <div class="secondary-inner">
                            <ul>
								<li><a href="javascript:;" class="btn-link"><i class="fa fa-facebook"></i></a></li>
								<li><a href="javascript:;" class="btn-link"><i class="fa fa-twitter"></i></a></li>
								<li><a href="javascript:;" class="btn-link"><i class="fa fa-whatsapp"></i></a></li>
								<!-- Search Button ==== -->
								<!-- <li class="search-btn"><button id="quik-search-btn" type="button" class="btn-link"><i class="fa fa-search"></i></button></li> -->
							</ul>
						</div>
                    </div>
					<!-- Search Box ==== -->
                    <div class="nav-search-bar">
                        <form action="#">
                            <input name="search" value="" type="text" class="form-control" placeholder="Type to search">
                            <span><i class="ti-search"></i></span>
                        </form>
						<span id="search-remove"><i class="ti-close"></i></span>
                    </div>
					<!-- Navigation Menu ==== -->
                    <div class="menu-links navbar-collapse collapse justify-content-start" id="menuDropdown">
						<div class="menu-logo">
							<a href="<?php echo url('/'); ?>"><img src="<?php echo url('/'); ?>/assets/web/images/logo.png" alt=""></a>
						</div>
                        <ul class="nav navbar-nav">	
							<li><a href="<?php echo url('/'); ?>">Home </a></li>
							<li><a href="javascript:;">About</a>
							</li>
							<!-- <li class="add-mega-menu"><a href="javascript:;">Our Courses <i class="fa fa-chevron-down"></i></a>
								<ul class="sub-menu add-menu">
									<li class="add-menu-left">
										<h5 class="menu-adv-title">Our Courses</h5>
										<ul>
											<li><a href="courses.html">Courses </a></li>
											<li><a href="courses-details.html">Courses Details</a></li>
											<li><a href="profile.html">Instructor Profile</a></li>
											<li><a href="event.html">Upcoming Event</a></li>
										</ul>
									</li>
									<li class="add-menu-right">
										<img src="<?php echo url('/'); ?>/assets/web/images/adv/adv.jpg" alt=""/>
									</li>
								</ul>
							</li> -->
							<!-- <li><a href="javascript:;">Blog <i class="fa fa-chevron-down"></i></a>
								<ul class="sub-menu">
									<li><a href="blog-classic-grid.html">Blog Classic</a></li>
									<li><a href="blog-classic-sidebar.html">Blog Classic Sidebar</a></li>
									<li><a href="blog-list-sidebar.html">Blog List Sidebar</a></li>
									<li><a href="blog-standard-sidebar.html">Blog Standard Sidebar</a></li>
									<li><a href="blog-details.html">Blog Details</a></li>
								</ul>
							</li> -->
							<li class="active nav-dashboard"><a href="{{ route('membership.index') }}">Membership</a></li>
							<li class="nav-dashboard"><a href="javascript:;">Contact</a></li>
						</ul>
						<div class="nav-social-link">
							<a href="javascript:;"><i class="fa fa-facebook"></i></a>
							<a href="javascript:;"><i class="fa fa-google-plus"></i></a>
							<a href="javascript:;"><i class="fa fa-linkedin"></i></a>
						</div>
                    </div>
					<!-- Navigation Menu END ==== -->
                </div>
            </div>
        </div>
    </header>
    <!-- header END ==== -->
    <!-- Content -->
    <div class="page-content bg-white">
        <!-- inner page banner -->
        <!-- <div class="page-banner ovbl-dark" style="background-image:url(assets/images/banner/banner2.jpg);">
            <div class="container">
                <div class="page-banner-entry">
                    <h1 class="text-white">Membership</h1>
				 </div>
            </div>
        </div> -->
		<!-- Breadcrumb row -->
		<div class="breadcrumb-row">
			<div class="container">
				<ul class="list-inline">
					<li><a href="<?php echo url('/'); ?>">Home</a></li>
					<li>Confirm Payment</li>
				</ul>
			</div>
		</div>
		<!-- Breadcrumb row END -->
        <!-- inner page banner END -->
		<div class="content-block">
            <!-- About Us -->
			<div class="section-area section-sp2">
                <div class="container">
					<div class="row">
						<div class="col-md-12 heading-bx text-center">
							 <!-- Sign up Form -->
							<div class="profile-head">
								<h3><i class="fa fa-credit-card" aria-hidden="true"></i> Confirm Payment</h3>
							</div>
							<h5><div id="message" align="center"></div></h5>
                            @php $amount=(float)$package_data->price @endphp
							<form class="edit-profile js-validation-register rzp-footer-form" action="{!!route('dopayment')!!}" method="POST">
								@csrf
								<input type="hidden" name="renewal" id="renewal" value="{{$user->renewal}}">
								<div class="">
									<div class="form-group row">
										<div class="col-12 col-sm-8 col-md-8 col-lg-9 ml-auto">
											<!-- <h3>Confirm Payment</h3> -->
										</div>
									</div>
									<div class="form-group row">
										<label class="col-12 col-sm-4 col-md-4 col-lg-3 col-form-label">Package :</label>
										<div class="col-12 col-sm-8 col-md-8 col-lg-7">
											{{ @$package_data->name }}
										</div>
									</div>
									@if (session('ref')=="")
									<div class="form-group row">
										<label class="col-12 col-sm-4 col-md-4 col-lg-3 col-form-label">Amount :</label>
										<div class="col-12 col-sm-8 col-md-8 col-lg-7">
											$ {{ $package_price}}
										</div>
									</div>
									@else
									<div class="form-group row">
										<div class="col-12 col-sm-8 col-md-8 col-lg-9 ml-auto">
											<h3>Referral discount applied</h3>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-12 col-sm-4 col-md-4 col-lg-3 col-form-label">Your Payable Amount :</label>
										<div class="col-12 col-sm-8 col-md-8 col-lg-7">
											$ {{ $package_price}}
										</div>
									</div>
									@endif
								</div>
								<div class="row" style="float:right;">
									<input type="hidden" name="amount" id="amount" value="{{ $package_price}}"/>
									<div class="pay">
										<div class="col-12 col-sm-8 col-md-8 col-lg-7">
											<button type="button" class="btn" id="paybtn"><i class="fa fa-plus mr-10"></i> Proceed to Payment</button>
										</div>
									</div>	
								</div>
												
							</form>
							<br><br>
                            <div id="paymentDetail" style="display: none">
                                    <center>
                                        <div>paymentID: <span id="paymentID"></span></div>
                                        <div>paymentDate: <span id="paymentDate"></span></div>
                                    </center>
                            </div>
							<!-- END Sign up Form -->

						</div>
					</div>
				</div>
            </div> 
		</div>
    </div>
    <!-- Content END-->
	<!-- Footer ==== -->
    <footer>
        <div class="footer-top">
			<div class="pt-exebar">
				<div class="container">
					<div class="d-flex align-items-stretch">
						<div class="pt-logo mr-auto">
							<a href="<?php echo url('/'); ?>"><img src="<?php echo url('/'); ?>/assets/web/images/logo-white.png" alt=""/></a>
						</div>
						<div class="pt-social-link">
							<ul class="list-inline m-a0">
								<li><a href="#" class="btn-link"><i class="fa fa-facebook"></i></a></li>
								<li><a href="#" class="btn-link"><i class="fa fa-twitter"></i></a></li>
								<li><a href="#" class="btn-link"><i class="fa fa-linkedin"></i></a></li>
								<li><a href="#" class="btn-link"><i class="fa fa-google-plus"></i></a></li>
							</ul>
						</div>
						<div class="pt-btn-join">
							<a href="{{ route('membership.index') }}" class="btn ">Join Now</a>
						</div>
					</div>
				</div>
			</div>
            <div class="container">
                <div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 text-center"> <a>Elearning</a></div>
					<!-- <div class="col-lg-4 col-md-12 col-sm-12 footer-col-4">
                        <div class="widget">
                            <h5 class="footer-title">Sign Up For A Newsletter</h5>
							<p class="text-capitalize m-b20">Weekly Breaking news analysis and cutting edge advices on job searching.</p>
                            <div class="subscribe-form m-b20">
								<form class="subscription-form" action="http://educhamp.themetrades.com/demo/assets/script/mailchamp.php" method="post">
									<div class="ajax-message"></div>
									<div class="input-group">
										<input name="email" required="required"  class="form-control" placeholder="Your Email Address" type="email">
										<span class="input-group-btn">
											<button name="submit" value="Submit" type="submit" class="btn"><i class="fa fa-arrow-right"></i></button>
										</span> 
									</div>
								</form>
							</div>
                        </div>
                    </div>
					<div class="col-12 col-lg-5 col-md-7 col-sm-12">
						<div class="row">
							<div class="col-4 col-lg-4 col-md-4 col-sm-4">
								<div class="widget footer_widget">
									<h5 class="footer-title">Company</h5>
									<ul>
										<li><a href="index.html">Home</a></li>
										<li><a href="about-1.html">About</a></li>
										<li><a href="faq-1.html">FAQs</a></li>
										<li><a href="contact-1.html">Contact</a></li>
									</ul>
								</div>
							</div>
							<div class="col-4 col-lg-4 col-md-4 col-sm-4">
								<div class="widget footer_widget">
									<h5 class="footer-title">Get In Touch</h5>
									<ul>
										<li><a href="http://educhamp.themetrades.com/admin/index.html">Dashboard</a></li>
										<li><a href="blog-classic-grid.html">Blog</a></li>
										<li><a href="portfolio.html">Portfolio</a></li>
										<li><a href="event.html">Event</a></li>
									</ul>
								</div>
							</div>
							<div class="col-4 col-lg-4 col-md-4 col-sm-4">
								<div class="widget footer_widget">
									<h5 class="footer-title">Courses</h5>
									<ul>
										<li><a href="courses.html">Courses</a></li>
										<li><a href="courses-details.html">Details</a></li>
										<li><a href="membership.html">Membership</a></li>
										<li><a href="profile.html">Profile</a></li>
									</ul>
								</div>
							</div>
						</div>
                    </div>
					<div class="col-12 col-lg-3 col-md-5 col-sm-12 footer-col-4">
                        <div class="widget widget_gallery gallery-grid-4">
                            <h5 class="footer-title">Our Gallery</h5>
                            <ul class="magnific-image">
								<li><a href="assets/images/gallery/pic1.jpg" class="magnific-anchor"><img src="assets/images/gallery/pic1.jpg" alt=""></a></li>
								<li><a href="assets/images/gallery/pic2.jpg" class="magnific-anchor"><img src="assets/images/gallery/pic2.jpg" alt=""></a></li>
								<li><a href="assets/images/gallery/pic3.jpg" class="magnific-anchor"><img src="assets/images/gallery/pic3.jpg" alt=""></a></li>
								<li><a href="assets/images/gallery/pic4.jpg" class="magnific-anchor"><img src="assets/images/gallery/pic4.jpg" alt=""></a></li>
								<li><a href="assets/images/gallery/pic5.jpg" class="magnific-anchor"><img src="assets/images/gallery/pic5.jpg" alt=""></a></li>
								<li><a href="assets/images/gallery/pic6.jpg" class="magnific-anchor"><img src="assets/images/gallery/pic6.jpg" alt=""></a></li>
								<li><a href="assets/images/gallery/pic7.jpg" class="magnific-anchor"><img src="assets/images/gallery/pic7.jpg" alt=""></a></li>
								<li><a href="assets/images/gallery/pic8.jpg" class="magnific-anchor"><img src="assets/images/gallery/pic8.jpg" alt=""></a></li>
							</ul>
                        </div>
                    </div> -->
                </div>
            </div>
        </div>
        <!-- <div class="footer-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 text-center"> <a>Elearning</a></div>
                </div>
            </div>
        </div> -->
    </footer>
    <!-- Footer END ==== -->
    <button class="back-to-top fa fa-chevron-up" ></button>
</div>
<!-- External JavaScripts -->
<!--Razor pay Scripts Starts -->
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<!--Razor pay Scripts Ends  -->
<!-- <script src="<?php echo url('/'); ?>/assets/web/js/jquery.min.js"></script> -->
<script src="<?php echo url('/'); ?>/assets/web/vendors/bootstrap/js/popper.min.js"></script>
<script src="<?php echo url('/'); ?>/assets/web/vendors/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo url('/'); ?>/assets/web/vendors/bootstrap-select/bootstrap-select.min.js"></script>
<script src="<?php echo url('/'); ?>/assets/web/vendors/bootstrap-touchspin/jquery.bootstrap-touchspin.js"></script>
<script src="<?php echo url('/'); ?>/assets/web/vendors/magnific-popup/magnific-popup.js"></script>
<script src="<?php echo url('/'); ?>/assets/web/vendors/counter/waypoints-min.js"></script>
<script src="<?php echo url('/'); ?>/assets/web/vendors/counter/counterup.min.js"></script>
<script src="<?php echo url('/'); ?>/assets/web/vendors/imagesloaded/imagesloaded.js"></script>
<script src="<?php echo url('/'); ?>/assets/web/vendors/masonry/masonry.js"></script>
<script src="<?php echo url('/'); ?>/assets/web/vendors/masonry/filter.js"></script>
<script src="<?php echo url('/'); ?>/assets/web/vendors/owl-carousel/owl.carousel.js"></script>
<script src="<?php echo url('/'); ?>/assets/web/js/functions.js"></script>
<script src="<?php echo url('/'); ?>/assets/web/js/contact.js"></script>
<!-- <script src='assets/vendors/switcher/switcher.js'></script> -->
<script>
    $('#rzp-footer-form').submit(function (e) {
        var button = $(this).find('button');
        var parent = $(this);
        button.attr('disabled', 'true').html('Please Wait...');
        $.ajax({
            method: 'get',
            url: this.action,
            data: $(this).serialize(),
            complete: function (r) {
                console.log('complete');
                console.log(r);
            }
        })
        return false;
    })
</script>

<script>
    function padStart(str) {
        return ('0' + str).slice(-2)
    }

    function demoSuccessHandler(transaction) {
        // You can write success code here. If you want to store some data in database.
        // alert('Successfully Registered');
        var dollars = parseInt("{{$package_price}}");
        var rupees = dollars * 100;
        $("#paymentDetail").removeAttr('style');
        $('#paymentID').text(transaction.razorpay_payment_id);
        var paymentDate = new Date();
        $('#paymentDate').text(
                padStart(paymentDate.getDate()) + '.' + padStart(paymentDate.getMonth() + 1) + '.' + paymentDate.getFullYear() + ' ' + padStart(paymentDate.getHours()) + ':' + padStart(paymentDate.getMinutes())
                );

        $.ajax({
            method: 'post',
            url: "{!!route('dopayment')!!}",
            data: {
                "_token": "{{ csrf_token() }}",
                "razorpay_payment_id": transaction.razorpay_payment_id,
                "user_id": "{{ $user->id }}",
                "amount" : dollars,
            },
            complete: function (r) {
                console.log('complete');
                console.log(r);
                document.getElementById("message").innerHTML="Successfully Registered! <br><br><a href='{{ route('user.login')}}' class='btn btn-success'>Click Here</a><br><br> to login with your credentials";
				$(".pay").hide();
				

                // window.location.href = "{{ route('user.login')}}";
            }
        })
    }
// History API Prevent back button after payment
if( window.history && window.history.pushState ){

history.pushState( "nohb", null, "" );
$(window).on( "popstate", function(event){
  if( !event.originalEvent.state ){
	history.pushState( "nohb", null, "" );
	return;
  }
});
}
</script>
<script>
    var dollars = parseInt("{{$package_price}}");
    var rupees = dollars * 100;
    var options = {
        key: "{{ 'rzp_test_H9hYopAQ48evZv' }}",
        amount: rupees,
        name: "{{ env('APP_NAME') }} ",
        description: "{{$package_data->name}}",
        image: "https://static.wixstatic.com/media/b96419_ee02e4331ebc41ecbf8394f0aaee8b55~mv2.jpg/v1/fill/w_160,h_120,al_c,q_80,usm_0.66_1.00_0.01/new%20logo%202.webp",
        handler: demoSuccessHandler
    }
</script>
<script>
    window.r = new Razorpay(options);
    document.getElementById('paybtn').onclick = function () {
        r.open()
    }
    var renewal=new Date(document.getElementById("renewal").value);
    var today=new Date();
    if(renewal>today)
    {
        alert("Your subscription is still valid!");
        document.getElementById("paybtn").disabled = true;
    }
</script>

</body>

</html>
