<!doctype html>
<html lang="en" class="no-focus">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

        <title>Infoniz</title>

        <meta name="description" content="Infoniz">
        <meta name="author" content="pixelcave">
        <meta name="robots" content="noindex, nofollow">

        <!-- Open Graph Meta -->
        <meta property="og:title" content="Infoniz">
        <meta property="og:site_name" content="Codebase">
        <meta property="og:description" content="Infoniz">
        <meta property="og:type" content="website">
        <meta property="og:url" content="">
        <meta property="og:image" content="">

        <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <link rel="shortcut icon" href="<?php echo url('/'); ?>/assets/admin/media/favicons/favicon.png">
        <link rel="icon" type="image/png" sizes="192x192" href="<?php echo url('/'); ?>/assets/admin/media/favicons/favicon-192x192.png">
        <link rel="apple-touch-icon" sizes="180x180" href="<?php echo url('/'); ?>assets/admin/media/favicons/apple-touch-icon-180x180.png">
        <!-- END Icons -->

        <!-- Stylesheets -->

        <!-- Fonts and Codebase framework -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Muli:300,400,400i,600,700">
        <link rel="stylesheet" id="css-main" href="<?php echo url('/'); ?>/assets/admin/css/codebase.min.css">

        <!-- END Stylesheets -->
    </head>
    <body>

        <!-- Page Container -->
      
        <div id="page-container" class="main-content-boxed">

            <!-- Main Container -->
            <main id="main-container">

            

                 <!-- Hero -->
                 <div class="bg-primary">
                    <div class="bg-pattern bg-black-op-25" style="background-image: url('<?php echo url('/'); ?>/assets/admin/media/various/bg-pattern.png');">
                        <div class="content content-top text-center">
                            <div class="py-5">
                               <a href="{{ url('/home') }}"> <h1 class="font-w700 text-white mb-10">{{ config('app.name') }}</h1></a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END Hero -->

                <!-- Pricing Tables -->
                <div class="content">
                    <div class="row py-30">
                        @if($packages->count())
                        @foreach ($packages as $package)
                        <div class="col-md-6 col-xl-4">
                            <!-- Startup Plan -->
                            <div class="block block-link-pop block-rounded block-bordered text-center"  >
                                <div class="block-header bg-primary">
                                    <h3 class="block-title text-white">{{ @$package->name }}</h3>
                                </div>
                                <div class="block-content bg-primary">
                                    <div class="h1 font-w700 mb-10 text-white">${{ @$package->price }}</div>
                                    <div class="h5 text-muted text-white-op">per month</div>
                                </div>
                                <div class="block-content">
                                    @foreach ($package->packageFeatures as $feature)
                                    <p><strong>
                                        @if(@$feature->enabled==1)
                                        <i class="fa fa-check-circle fa-lg"
                                         aria-hidden="true" style="color:green;"></i>
                                        @else
                                        <i class="fa fa-times-circle fa-lg" 
                                        aria-hidden="true" style="color:red;"></i>
                                        @endif
                                    {{@$feature->feature}}</strong> </p>
                                    @endforeach
                                </div>

                                <div class="block-content block-content-full">
                                    <a href="{{ route('register.index',$package->slug) }}">
                                    <!-- <form  method="POST" action="{{ route('register.index',$package->slug) }}">
                                    @csrf   -->
                                    <!-- <input type="hidden" name="package_id" value="{{ $package->id }}">        -->
                                    <div class="col-sm-12">
                                        <button type="submit" class="btn btn-hero btn-sm btn-rounded btn-noborder btn-primary">
                                            <i class="fa fa-shopping-cart mr-10"></i> Buy Now
                                        </button>
                                    </div>
                                    <!-- </form> -->
                                    </a>
                                </div>
                               

                                
                            </div>
                            <!-- END Startup Plan -->
                        </div>
                        @endforeach
                        @else

                        <div class="col-md-12 col-xl-12">
                            No Data Available
                        <div>    

                        @endif
                    </div>
                </div>
                <!-- END Pricing Tables -->

            </main>
            <!-- END Main Container -->
        </div>
        <!-- END Page Container -->

     
        <script src="<?php echo url('/'); ?>/assets/admin/js/codebase.core.min.js"></script>

        
        <script src="<?php echo url('/'); ?>/assets/admin/js/codebase.app.min.js"></script>

        <!-- Page JS Plugins -->
        <script src="<?php echo url('/'); ?>/assets/admin/js/plugins/jquery-countdown/jquery.countdown.min.js"></script>

        <!-- Page JS Code -->
        <script src="<?php echo url('/'); ?>/assets/admin/js/pages/op_coming_soon.min.js"></script>
    </body>
</html>