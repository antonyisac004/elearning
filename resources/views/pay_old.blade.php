@extends('layouts.app')

@section('content')
<!--Razor pay Scripts Starts -->
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<!--Razor pay Scripts Ends  -->
<div id="main-content">
 <!-- Page Content -->
 <div class="bg-body-dark bg-pattern" style="background-image: url('<?php echo url('/'); ?>/assets/admin/media/various/bg-pattern-inverse.png');">
                    <div class="row mx-0 justify-content-center">
                        <div class="hero-static col-lg-6 col-xl-12">
                            <div class="content content-full overflow-hidden">
                                <!-- Header -->
                                <div class="py-30 text-center">
                                    <a class="link-effect font-w700" href="#">
        
                                    <span class="font-size-xl text-primary-dark">{{ config('app.name') }}</span>
                                    </a>
                                    <h1 class="h4 font-w700 mt-30 mb-10">Confirm Payment</h1>
                                </div>
                                <!-- END Header -->

                                {{-- confirmation message --}}
                                <h5><div id="message" align="center"></div></h5>
                                @php $amount=(float)$package_data->price @endphp

                                <!-- Sign up Form -->
                               
                                <form class="js-validation-register rzp-footer-form" action="{!!route('dopayment')!!}" method="POST">
                                    @csrf
                                    <input type="hidden" name="renewal" id="renewal" value="{{$user->renewal}}">
                                    <div class="block block-themed block-rounded block-shadow">
                                        <div class="block-header bg-gd-emerald">
                                            <h3 class="block-title">Confirm Payment</h3>
                                            <div class="block-options">
                                                <button type="button" class="btn-block-option">
                                                <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                                                </button>
                                            </div>
                                        </div>
                                        <div class="block-content">
                                            <div class="form-group row ">
                                                <div class="col-12" >
                                                    <label for="package_id">Package : {{ @$package_data->name }}</label>
                                                    <br>
                                                    @if (session('ref')=="")
                                                        <label for="package_id">Amount :  $ {{ $package_price}}</label>
                                                    @else
                                                        <p class="alert alert-info">Referral discount applied</p>
                                                        <label for="package_id">Your Payable Amount :  $ {{ $package_price}}</label>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group row mb-0">
                                                <input type="hidden" name="amount" id="amount" value="{{ $package_price}}"/>
                                                <!-- <div class="pay">
                                                <button class="razorpay-payment-button btn filled small" id="paybtn" type="button">Pay with Razorpay</button>                        
                                                </div> -->
                                                <div class="pay">
                                                    <div class="col-sm-12 text-sm-right push pay">
                                                        <button type="button" class="btn btn-alt-success" id="paybtn" >
                                                            <i class="fa fa-plus mr-10"></i> Confirm Payment
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </form>
                                <br><br>
                                <div id="paymentDetail" style="display: none">
                                    <center>
                                        <div>paymentID: <span id="paymentID"></span></div>
                                        <div>paymentDate: <span id="paymentDate"></span></div>
                                    </center>
                                </div>
                                <!-- END Sign up Form -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END Page Content -->

<script>
    $('#rzp-footer-form').submit(function (e) {
        var button = $(this).find('button');
        var parent = $(this);
        button.attr('disabled', 'true').html('Please Wait...');
        $.ajax({
            method: 'get',
            url: this.action,
            data: $(this).serialize(),
            complete: function (r) {
                console.log('complete');
                console.log(r);
            }
        })
        return false;
    })
</script>

<script>
    function padStart(str) {
        return ('0' + str).slice(-2)
    }

    function demoSuccessHandler(transaction) {
        // You can write success code here. If you want to store some data in database.
        // alert('Successfully Registered');
        var dollars = parseInt("{{$package_price}}");
        var rupees = dollars * 100;
        $("#paymentDetail").removeAttr('style');
        $('#paymentID').text(transaction.razorpay_payment_id);
        var paymentDate = new Date();
        $('#paymentDate').text(
                padStart(paymentDate.getDate()) + '.' + padStart(paymentDate.getMonth() + 1) + '.' + paymentDate.getFullYear() + ' ' + padStart(paymentDate.getHours()) + ':' + padStart(paymentDate.getMinutes())
                );

        $.ajax({
            method: 'post',
            url: "{!!route('dopayment')!!}",
            data: {
                "_token": "{{ csrf_token() }}",
                "razorpay_payment_id": transaction.razorpay_payment_id,
                "user_id": "{{ $user->id }}",
                "amount" : dollars,
            },
            complete: function (r) {
                console.log('complete');
                console.log(r);
                document.getElementById("message").innerHTML="Successfully Registered! <br><br><a href='{{ route('user.login')}}' class='btn btn-success'>Click Here</a><br><br> to login with your credentials"
                // window.location.href = "{{ route('user.login')}}";
            }
        })
    }
</script>
<script>
    var dollars = parseInt("{{$package_price}}");
    var rupees = dollars * 100;
    var options = {
        key: "{{ env('RAZORPAY_KEY') }}",
        amount: rupees,
        name: "{{ env('APP_NAME') }} ",
        description: "{{$package_data->name}}",
        image: "https://static.wixstatic.com/media/b96419_ee02e4331ebc41ecbf8394f0aaee8b55~mv2.jpg/v1/fill/w_160,h_120,al_c,q_80,usm_0.66_1.00_0.01/new%20logo%202.webp",
        handler: demoSuccessHandler
    }
</script>
<script>
    window.r = new Razorpay(options);
    document.getElementById('paybtn').onclick = function () {
        r.open()
    }
    var renewal=new Date(document.getElementById("renewal").value);
    var today=new Date();
    if(renewal>today)
    {
        alert("Your subscription is still valid!");
        document.getElementById("paybtn").disabled = true;
    }
</script>

@endsection