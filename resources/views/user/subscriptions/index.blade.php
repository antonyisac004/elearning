@php
    use Carbon\Carbon;
@endphp
@extends('user.layout.app')

@section('content')
<!-- Page Content -->
<div class="content">
    <nav class="breadcrumb bg-white push">
        <a class="breadcrumb-item" href="{{ route('home') }}">Dashboard</a>
        <span class="breadcrumb-item active">My Subscription</span>
    </nav>

    <!-- Dynamic Table Full -->
    <div class="block">
        <div class="block-header block-header-default">
            <h3 class="block-title">My Current Subscribed Package : <span class="badge badge-success">{{$userpackage->name}}</span> Subscription Package Expiry Date : <span class="badge badge-info">{{Carbon::parse($user->renewal)->format('d-M-Y')}}</span></h3>
        </div> 
        
       
    </div>
    <!-- END Dynamic Table Full -->

              
                <!-- Pricing Tables -->
                <div class="content">
                    <div class="row py-30">
                        
                      
                        <div  class="col-md-6 col-xl-6 mx-auto">
                            <!-- Startup Plan -->
                            <div class="block block-link-pop block-rounded block-bordered text-center"  >
                               

                                <div class="block-header bg-primary">
                                    <h3 class="block-title text-white">{{ @$userpackage->name }} </h3>
                                </div>
                                <div class="block-content bg-primary">
                                    <div class="h1 font-w700 mb-10 text-white">${{ @$userpackage->price }}</div>
                                    <div class="h5 text-muted text-white-op">per month</div>
                                </div>

                                
                                <div class="block-content">
                                    @foreach ($userpackage->packageFeatures as $feature)
                                    <p><strong>
                                        @if(@$feature->enabled==1)
                                        <i class="fa fa-check-circle fa-lg"
                                         aria-hidden="true" style="color:green;"></i>
                                        @else
                                        <i class="fa fa-times-circle fa-lg" 
                                        aria-hidden="true" style="color:red;"></i>
                                        @endif
                                    {{@$feature->feature}}</strong> </p>
                                    @endforeach
                                </div>

                               
                               

                                
                            </div>
                            <!-- END Startup Plan -->
                        </div>
                       
                        
                    </div>
                </div>
                <!-- END Pricing Tables -->
                
</div>               
<!-- END Page Content -->

@endsection
@section('js_after')
@endsection