@php
    use Carbon\Carbon;
@endphp
@extends('user.layout.app')

@section('content')
<!-- Page Content -->
<div class="content">
    <nav class="breadcrumb bg-white push">
        <a class="breadcrumb-item" href="{{ route('home') }}">Dashboard</a>
        <span class="breadcrumb-item active">My Classes</span>
    </nav>

    <!-- Dynamic Table Full -->
    <div class="block">
        <div class="block-header block-header-default">
            <h3 class="block-title">View Recorded Classes
                {{-- | Subscribed Package : <span class="badge badge-success">{{$package}}</span> / Next Renewal : <span class="badge badge-info">{{Carbon::parse($user->renewal)->format('d-M-Y')}}</span> --}}
            </h3>
        </div>
        <div class="block-content block-content-full">
         <!-- Courses List -->
                    <table id="recorded-videos" class="table table-bordered table-striped table-vcenter">
                        <thead>
                            <tr>
                                <th style="text-transform: none !important;">Sl.No</th>
                                <th style="text-transform: none !important;">Meeting Title</th>
                                <th style="text-transform: none !important;">Scheduled Date / Time</th>
                                <th style="text-transform: none !important;">Play</th>
            
                            </tr>
                        </thead>
                        <tbody>
                            @if($videos->count())
                            @foreach ($videos as $data)
                            <tr>
                                <td class="text-center"> {{ $loop->iteration }} </td>
                                <td class="font-w600"> {{ @$data->title }}</td>
                                <td>
                                    {{ Carbon::parse(@$data->schedule)->format('d M Y h:i:s A') }}
                                </td>
                                <td>
                                    <a href="{{ route('user.recorded-course.show',['id'=>$data->id]) }}"><button type="button" class="btn btn-sm btn-secondary" data-toggle="tooltip"
                                    title="Play" style="background:transparent;border:0px;"><i class="fa fa-play" style="color:green;"></i></button></a>
                                </td>
                            </tr>
                            @endforeach
                            @else
                            <tr class="no-data">
                                <td colspan="7" class="text-center">No data found</td>
                            </tr>
                            @endif
                        </tbody>
                        
                    </table>
        </div>            
        <!-- END Courses -->
    </div>
    <!-- END Dynamic Table Full -->
</div>               
<!-- END Page Content -->

@endsection
@section('js_after')
<script>
    function openBrowser(url) {
        var curr_browser = navigator.appName;
        if (curr_browser == "Microsoft Internet Explorer") {
            window.opener = self;
        }
        window.open(url, 'null', 'width=900,height=750,toolbar=no,scrollbars=no,location=no,titlebar=no,resizable =no');
        window.moveTo(0, 0);
        window.resizeTo(screen.width, screen.height - 100);
    }
    $(function() {
        $('#recorded-videos').DataTable({
            "dom": '<"pull-left"f><"pull-right"l>tip',
            "paging": false,
            language : {
                sLengthMenu: "Show _MENU_"
            },
            "bInfo" : false
        });
        
    });
</script>
@endsection