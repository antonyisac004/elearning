@extends('user.layout.app')

@section('content')
<!-- Page Content -->
<div class="content">
    <nav class="breadcrumb bg-white push">
        <a class="breadcrumb-item" href="{{ route('home') }}">Dashboard</a>
        <span class="breadcrumb-item active">My Referral Link</span>
    </nav>

    <!-- Dynamic Table Full -->
    <div class="block">
        <div class="block-header block-header-default">
            <h3 class="block-title">Share/Copy Referral Link </h3>
        </div>
        <div class="block-content block-content-full">

            @if (session('error'))
                <div class="alert alert-danger alert-dismissable" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <p class="mb-0">{{ session('error') }}</p>
                </div>
            @endif
            @if (session('success'))
                <div class="alert alert-success alert-dismissable" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <p class="mb-0">{{ session('success') }}</p>
                </div>
            @endif
            <div>
                <div class="form-group">
                    <label for="link">Share/Copy Referral Link</label>
                    <textarea name="link" id="link" class="form-control" cols="30" rows="5" readonly>{{$link}}</textarea>
                </div>
                <div align="right"><button type="button" class="btn btn-alt-primary" onclick="copy()">Copy to Clipboard</button>&nbsp;<button type="button" class="btn btn-alt-primary" onclick="referMailer()">Share Referral Link via email</button></div>
            </div>  
        </div>
    </div>
    <!-- END Dynamic Table Full -->
</div>               
<!-- END Page Content -->

@endsection
@section('js_after')
<script>
    function copy() {
        let textarea = document.getElementById("link");
        textarea.select();
        document.execCommand("copy");
        alert("Link copied to clipboard!");
    }
    function referMailer() {
        let email=prompt("Please enter email id : ");
        var mailformat = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        if (mailformat.test(email) == false)
        {
            alert('Invalid Email Address');
            return (false);
        }
        $.ajax({
            url: "{{ route('user.referral-mail') }}",
            type:"get",
            data:{
                email:email,
            },
            success:function(response){
                console.log(response);
                if(response) {
                    // $("#addroom").reset();
                    alert("Mail forwarded successfully!");
                }
            },
        });
    }
</script>
@endsection