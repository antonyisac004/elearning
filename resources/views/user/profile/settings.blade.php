@extends('user.layout.app')

@section('content')
<!-- Page Content -->
<div class="content">
    <nav class="breadcrumb bg-white push">
        <a class="breadcrumb-item" href="{{ route('home') }}">Dashboard</a>
        <span class="breadcrumb-item active">Settings</span>
    </nav>

    <!-- Dynamic Table Full -->
    <div class="block">
        <div class="block-header block-header-default">
            <h3 class="block-title">Change your password </h3>
        </div>
        <div class="block-content block-content-full">
            @if (session('error'))
                <div class="alert alert-danger alert-dismissable" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <p class="mb-0">{{ session('error') }}</p>
                </div>
            @endif
            @if (session('success'))
                <div class="alert alert-success alert-dismissable" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <p class="mb-0">{{ session('success') }}</p>
                </div>
            @endif
            <div>
                <form action="{{ route('user.password.reset') }}" method="post">@csrf
                    <div class="col-md-12 @error('password') is-invalid @enderror">
                        <div class="form-material">
                            <input type="password" name="password" id="password" class="form-control" placeholder="......." required>
                            <label for="password">Password * 
                            <a  data-toggle="tooltip" title="Your password must be at least 10 characters in length,
                                                    must contain at least one lowercase letter,
                                                    must contain at least one uppercase letter,
                                                    must contain at least one digit,
                                                    must contain a special character"
                                                    >  <i class="fa fa-info-circle" aria-hidden="true"></i></a>
                            </label>
                            
                                                    
                        </div>
                        @error('password')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="col-md-12 @error('password_confirmation') is-invalid @enderror">
                        <div class="form-material">
                            <input type="password" name="password_confirmation" id="password_confirmation" placeholder="......" class="form-control" required>
                            <label for="password_confirmation">Confirm Password *</label>
                        </div>
                        @error('password_confirmation')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div><p> </p>
                    <button type="submit" class="btn btn-alt-primary">Change Password</button>
                </form>
            </div>  
        </div>
    </div>
    <!-- END Dynamic Table Full -->
</div>               
<!-- END Page Content -->

@endsection
@section('js_after')

@endsection