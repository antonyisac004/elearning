@extends('user.layout.app')

@section('content')
<div class="bg-image bg-image-bottom" style="background-image: url('<?php echo url('/'); ?>/assets/admin/media/photos/photo13@2x.jpg');">
    <div class="bg-primary-dark-op py-30">
        <div class="content content-full text-center">
            <!-- Avatar -->
            <div class="mb-15">
                <a class="img-link" href="">
                    @if ($data->photo=="")
                        <img class="img-avatar img-avatar96 img-avatar-thumb" src="<?php echo url('/'); ?>/assets/admin/media/avatars/avatar15.jpg" alt="">
                    @else
                        <img class="img-avatar img-avatar96 img-avatar-thumb" src="{{ asset('storage/'.$data->photo) }}" width="100%">
                      
                    @endif
                </a>
            </div>
            <!-- END Avatar -->

            <!-- Personal -->
            <h1 class="h3 text-white font-w700 mb-10">{{ $data->first_name }} {{ $data->last_name }}</h1>
            <!-- END Personal -->

        </div>
    </div>
</div>
<!-- END User Info -->
<!-- Main Content -->
<div class="content">
<form action="{{ route('user.profile.update') }}" method="post" enctype="multipart/form-data">
    @csrf
    <div class="form-group row">
        <div class="col-md-12 @error('first_name') is-invalid @enderror">
            <div class="form-material">
                <input type="text" name="first_name" id="first_name" placeholder="First Name" class="form-control"
                 value="{{ @old('first_name',$data->first_name) }}" required>
                <label for="first_name">First Name *</label>
            </div>
            @error('first_name')
                <div class="invalid-feedback">{{ $message }}</div>
            @enderror
        </div>
    </div>

    <div class="form-group row">
        <div class="col-md-12 @error('last_name') is-invalid @enderror">
            <div class="form-material">
                <input type="text" name="last_name" id="last_name" placeholder="Last Name" class="form-control"
                 value="{{ @old('last_name',$data->last_name) }}" required>
                <label for="last_name">Last Name *</label>
            </div>
            @error('last_name')
                <div class="invalid-feedback">{{ $message }}</div>
            @enderror
        </div>
    </div>

    <div class="form-group row">
        <div class="col-md-12 @error('phone') is-invalid @enderror">
            <div class="form-material">
                <input type="number" name="phone" id="phone" placeholder="Phone Number" class="form-control"
                 value="{{ @old('phone',$data->phone) }}" required>
                <label for="phone">Phone *</label>
            </div>
            @error('phone')
                <div class="invalid-feedback">{{ $message }}</div>
            @enderror
        </div>
    </div>

    <div class="form-group row">
        <div class="col-md-12 @error('email') is-invalid @enderror">
            <div class="form-material">
                <input type="email" name="email" id="email" placeholder="Email ID" class="form-control"
                 value="{{ @old('email',$data->email) }}" required>
                <label for="email">Email Id *</label>
            </div>
            @error('email')
                <div class="invalid-feedback">{{ $message }}</div>
            @enderror
        </div>
    </div>

    <div class="form-group row">
        <div class="col-md-12 @error('photo') is-invalid @enderror">
            <div class="form-material">
                <input type="file" name="photo" id="photo" class="form-control">
                <label for="photo">Photo</label>
            </div>
            @error('photo')
                <div class="invalid-feedback">{{ $message }}</div>
            @enderror
        </div>
    </div>

    <div class="form-group row">
        <div class="col-md-9">
            <button type="submit" class="btn btn-alt-primary">Submit</button>
        </div>
    </div>
</form>



</div>
<!-- END Main Content -->

@endsection
@section('js_after')
@endsection