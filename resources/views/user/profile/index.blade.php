@extends('user.layout.app')

@section('content')
<div class="bg-image bg-image-bottom" style="background-image: url('<?php echo url('/'); ?>/assets/admin/media/photos/photo13@2x.jpg');">
    <div class="bg-primary-dark-op py-30">
        <div class="content content-full text-center">
            <!-- Avatar -->
            <div class="mb-15">
                <a class="img-link" href="{{ route('home') }}">
                    @if($user->photo=="")
                        <img class="img-avatar img-avatar96 img-avatar-thumb" src="<?php echo url('/'); ?>/assets/admin/media/avatars/avatar15.jpg" alt="">
                    @else
                        <img class="img-avatar img-avatar96 img-avatar-thumb" src="{{ asset('storage/'.$user->photo) }}" alt="">
                    @endif
                </a>
            </div>
            <!-- END Avatar -->

            <!-- Personal -->
            <h1 class="h3 text-white font-w700 mb-10">{{ $user->first_name }} {{ $user->last_name }}</h1>
            <a href="{{ route('user.profile.edit') }}" type="button" class="btn btn-rounded btn-hero btn-sm btn-alt-success mb-5"><i class="fa fa-edit mr-5"></i>Edit Profile</a>
            {{-- <button type="button" class="btn btn-rounded btn-hero btn-sm btn-alt-success mb-5">
                <i class="fa fa-plus mr-5"></i> Add Friend
            </button> --}}
            <!-- END Personal -->

        </div>
    </div>
</div>
<!-- END User Info -->
<!-- Main Content -->
<div class="content">
<div class="block-content block-content-full">
    @if (session('error'))
        <div class="alert alert-danger alert-dismissable" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
            </button>
            <p class="mb-0">{{ session('error') }}</p>
        </div>
    @endif
    @if (session('success'))
        <div class="alert alert-success alert-dismissable" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
            </button>
            <p class="mb-0">{{ session('success') }}</p>
        </div>
    @endif
<div>
<div class="row items-push">
<div class="col-md-6 col-xl-6">
<!-- Activity -->
<h2 class="content-heading">
<i class="si si-list mr-5"></i> Activity
</h2>
<div class="row items-push">
<div class="col-md-12 col-xl-12">
    <div class="block">
        <div class="block-content block-content-full">
            <ul class="list list-timeline list-timeline-modern pull-t">
                @foreach ($activities as $activity)
                <li>
                    <div class="list-timeline-time">{{ $activity->updated_at->diffForHumans() }}</div>
                    <i class="list-timeline-icon fa fa-list bg-info"></i>
                    <div class="list-timeline-content">
                        <p class="font-w600">{{ $activity->subject }}</p>
                    </div>
                </li>
                @endforeach
            </ul>
        </div>
        <div class="block-content">
            {{ $activities->links() }}
        </div>
    </div>
</div>
</div>
<!-- Activity -->
</div>
<div class="col-md-6 col-xl-6">
<!-- Activity -->
<h2 class="content-heading">
<i class="si si-user mr-5"></i> Profile
</h2>
<div class="row items-push">
<div class="col-md-12">
    <!-- Floating Labels -->
    <div class="block">
        <div class="block-content">
            <table id="profile" class="table table-striped table-bordered" style="width:100%">
                <tr>
                    <th>First Name</th>
                    <td>{{$user->first_name}}</td>
                </tr>
                <tr>
                    <th>Last Name</th>
                    <td>{{$user->last_name}}</td>
                </tr>
                <tr>
                    <th>Phone</th>
                    <td>{{$user->phone}}</td>
                </tr>
                <tr>
                    <th>Email ID</th>
                    <td>{{$user->email}}</td>
                </tr>
                <tr>
                    <th>Status</th>
                    <td>
                        @if ($user->status)
                            <span class="badge badge-success">Active</span>
                        @else
                            <span class="badge badge-danger">Inactive</span>
                        @endif
                    </td>
                </tr>
                <tr>
                    <th>Subscribed Package Expiry Date</th>
                    <td>{{Carbon::parse($user->renewal)->format('d M Y')}}</td>
                </tr>
            </table>
        </div>
    </div>
    <!-- END Floating Labels -->
</div>
</div>
<!-- Activity -->
</div>
</div>



</div>
<!-- END Main Content -->

@endsection
@section('js_after')
@endsection