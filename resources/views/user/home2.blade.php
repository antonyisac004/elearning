@extends('user.layout.app')
@section('content')
<!-- Page Content -->
<div class="content">
                    <!-- Hero -->
                    <div class="block block-rounded">
                        <div class="block-content block-content-full bg-pattern" style="background-image: url('assets/media/various/bg-pattern-inverse.png');">
                            <div class="py-20 text-center">
                                <h2 class="font-w700 text-black mb-10">
                                <i class="fa fa-video-camera mr-5"></i> Upcoming Live Class
                                </h2>
                                @if($live_video!=null)
                                <div class="row">
                                    @foreach ($live_video as $video)
                                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="padding:05px;">
                                            <div style="padding:05px;border:2px;border-style:solid;border-radius:05px;background:#E9EAF5;">
                                            <i class="fa fa-video-camera mr-5"></i>
                                            <h3 class="h5 text-muted mb-0">
                                            Title : {{ @$video->title }}
                                            </h3>
            
                                            <h3 class="h5 text-muted mb-0">
                                            About Meeting:{{ @$video->description }}
                                            </h3>
            
                                            <h3 class="h5 text-muted mb-0">
                                            Meeting ID: {{ @$video->meeting_id }} 
                                            </h3>
            
                                            <h3 class="h5 text-muted mb-0">
                                            Meeting Passcode: {{ @$video->passcode }}
                                            </h3>
            
                                            <h3 class="h5 text-muted mb-0">
                                            Scheduled on :  {{ Carbon::parse(@$video->schedule)->format('l jS \\ F Y h:i:s A') }}
                                            </h3>
                                            @php
                                                $api_key="y25orjQNRe-0HTpoGW0xKw";
                                                $api_secret="CYaFsZZaLwixtWTJzOljw5Y664tp4nFBnAfo";
                                                $meeting_number=$video->meeting_id;
                                                $role=0;
                                                date_default_timezone_set("UTC");
                                                $time = time() * 1000 - 30000;//time in milliseconds (or close enough)
                                                $data = base64_encode($api_key . $meeting_number . $time . $role);
                                                $hash = hash_hmac('sha256', $data, $api_secret, true);
                                                $_sig = $api_key . "." . $meeting_number . "." . $time . "." . $role . "." . base64_encode($hash);
                                                //return signature, url safe base64 encoded
                                                $signature= rtrim(strtr(base64_encode($_sig), '+/', '-_'), '=');
                                            @endphp
                                            {{-- <a href="http://localhost:8080/zoom/meeting.html?name={{Auth::user()->first_name}}&mn={{$live_video->meeting_id}}&email=&pwd=&role=0&lang=en-US&signature={{$signature}}&china=0&apiKey=y25orjQNRe-0HTpoGW0xKw">Zoom</a> --}}
                                                <h3 class="h5 text-muted mb-0">
                                                    <a href="http://localhost:8080/zoom/meeting.html?name={{Auth::user()->first_name}}&mn={{$video->meeting_id}}&email=&pwd=&role=0&lang=en-US&signature={{$signature}}&china=0&apiKey=y25orjQNRe-0HTpoGW0xKw">Click here to join meeting</a>
                                                </h3>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                                
                                @else

                                <h3 class="h5 text-muted mb-0">
                                  No Data found.
                                </h3>

                                @endif
                            </div>
                        </div>
                    </div>
                    <!-- END Hero -->

                    <!-- Overview -->
                   
                    <h2 class="font-w700 text-black mb-10">  
                    <i class="fa fa-video-camera mr-5"></i> Previous Classes
                    </h2>
                    @if($recorded_videos->count())
                    <!-- Courses List -->
         <div class="row items-push js-gallery" id="gallery">
            @if($recorded_videos->count())
            @foreach ($recorded_videos as $video)
            <div class="col-md-6 animated fadeIn">
                <div class="options-container">
                    <video width="100%" height="100%" controls controlsList="nodownload">
                        <source src="{{@$video->video_link}}" type="video/mp4">
                    </video>
                    <!-- @if ($video->thumb)
                        <img class="img-fluid options-item" src="{{ asset('storage/'.$video->thumb) }}" alt="" id="thumbnail">
                    @else
                        <img class="img-fluid options-item" src="<?php echo url('/'); ?>/assets/admin/media/photos/thumbnail-video.png" alt="" id="thumbnail">
                    @endif -->
                    <!-- <div class="options-overlay bg-white-op-90">
                        <div class="options-overlay-content">
                            <h3 class="h4 mb-5">{{Str::limit($video->title, $limit = 25, $end = ' ...')}}</h3>

                            <a target="_blank" class="btn btn-sm btn-rounded btn-alt-primary min-width-75" 
                            href="{{@$video->video_link}}">
                                <i class="fa fa-play" aria-hidden="true"></i> Play
                            </a>
                        </div>
                    </div> -->
                </div>
            </div>
            @endforeach
            @else
            <div class="no-data">
                <div  class="text-center">No data found</div>
            </div>
            @endif           
        </div>
        <!-- END Courses -->
                    @else
                    <h3 class="h5 text-muted mb-0">
                        No Data found.
                    </h3>

                    @endif
                    <!-- END Overview -->

</div>               
<!-- END Page Content -->

@endsection
@section('js_after')
<script>
    function openBrowser(url) {
        var curr_browser = navigator.appName;
        if (curr_browser == "Microsoft Internet Explorer") {
            window.opener = self;
        }
        window.open(url, 'null', 'width=900,height=750,toolbar=no,scrollbars=no,location=no,titlebar=no,resizable =no');
        window.moveTo(0, 0);
        window.resizeTo(screen.width, screen.height - 100);
    }
</script>
@endsection