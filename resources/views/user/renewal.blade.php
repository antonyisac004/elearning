@extends('user.layout.app')

@section('content')
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<!-- Page Content -->
<div class="content">


<h4><div id="pay_message"></div></h4>

    <!-- Hero -->
    <div class="block block-rounded">
        <div class="block-content block-content-full bg-pattern" style="background-image: url('assets/media/various/bg-pattern-inverse.png');">
            <div class="py-20 text-center">
                <h2 class="font-w700 text-black mb-10">
                    Your Subscription Expired
                </h2>
                <h3 class="h5 text-muted mb-0">
                    Please renew or upgrade your subscription to continue our services
                </h3><br>
            
                    <p class="mb-12">
                        <strong>Subscription : </strong>
                        {{-- <b> {{ @$user->package->name}}</b> --}}
                        <div align="center"><select name="subscription" id="subscription" class="form-control" style="width:50%;" onchange="getData(this.value,{{$user->discount}})" >
                           
                            @foreach ($packages as $package)
                                <option @if($package->id==$user->package_id)
										    selected
										@endif value="{{$package->id}}">
                                        {{ $package->name .'- $'. @$package->price }} per month
                                </option>
                            @endforeach
                        </select></div>
                        <input type="hidden" name="price" id="price" value="{{$user->package->price-(($user->package->price*$user->discount)/100)}}">
                        <p> </p>
                        <div class="row" width="50%">
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">Package price : $<span id="package_price">{{$user->package->price}}</span></div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12"><b>Referral discount(if any) : <span id="discount">{{$user->discount}}</span> %</b></div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="color:green;"><b>Amount Payable : $<span id="payable">{{$user->package->price-(($user->package->price*$user->discount)/100)}}</span></b></div>
                        </div>
                    </p>
                    <p class="mb-12">
                        <strong>Expired on:</strong>
                        <b>{{Carbon::parse($user->renewal)->format('d M Y')}}</b>
                    </p>
                    
                    <p>
                    <strong>Status:</strong> 
                    <button type="button" data-toggle="tooltip" 
                    data-placement="top" title="{{ @$user->package->name}} package expired.Please renew the package"
                         class="btn btn-sm  btn-alt-danger mr-5 mb-5">
                    Expired
                    </button>
                    </p>
                    
                    <div class="pay">
                    <button type="button" class="btn btn-sm btn-alt-primary mr-12" id="paybtn">
                        Renew Package
                    </button>
                    </div>
            </div>
        </div>
    </div>
    <!-- END Hero -->


</div>               
<!-- END Page Content -->

@endsection
@section('js_after')
<script>
    function padStart(str) {
        return ('0' + str).slice(-2)
    }

    function demoSuccessHandler(transaction) {
        // You can write success code here. If you want to store some data in database.
        $("#paymentDetail").removeAttr('style');
        $('#paymentID').text(transaction.razorpay_payment_id);
        var paymentDate = new Date();
        $('#paymentDate').text(
                padStart(paymentDate.getDate()) + '.' + padStart(paymentDate.getMonth() + 1) + '.' + paymentDate.getFullYear() + ' ' + padStart(paymentDate.getHours()) + ':' + padStart(paymentDate.getMinutes())
                );
        $.ajax({
            method: 'post',
            url: "{!!route('user.savePayment')!!}",
            data: {
                "_token": "{{ csrf_token() }}",
                "razorpay_payment_id": transaction.razorpay_payment_id,
                "amount": document.getElementById("price").value,
                "subscription":document.getElementById("subscription").value,
            },
            complete: function (r) {
                console.log('complete');
                console.log(r);
                
                document.getElementById("pay_message").innerHTML="Your Payment is successfull.<button class='btn btn success' onclick='refresh()'><b style='color:green'>Click to refresh  <i class='fa fa-refresh' aria-hidden='true'></i></b></button>";
                $(".pay").hide();
            }
        })
    }
    function refresh(){
        location.reload();
    }
</script>
<script>
    var amt=parseInt(document.getElementById("price").value) * 100;
    var options = {
        key: "{{ 'rzp_test_H9hYopAQ48evZv' }}",
        amount: amt,
        // amount: 10000,
        currency: 'USD',
        name: '',
        description: "{{$user->package->name}}",
        image: "https://static.wixstatic.com/media/b96419_ee02e4331ebc41ecbf8394f0aaee8b55~mv2.jpg/v1/fill/w_160,h_120,al_c,q_80,usm_0.66_1.00_0.01/new%20logo%202.webp",
        handler: demoSuccessHandler
    }
</script>
<script>
    window.r = new Razorpay(options);
    document.getElementById('paybtn').onclick = function () {
        amt=document.getElementById("price").value * 100;
        var options = {
            key: "{{ 'rzp_test_H9hYopAQ48evZv' }}",
            amount: amt,
            // amount: 10000,
            currency: 'USD',
            name: '',
            description: "{{$user->package->name}}",
            image: "https://static.wixstatic.com/media/b96419_ee02e4331ebc41ecbf8394f0aaee8b55~mv2.jpg/v1/fill/w_160,h_120,al_c,q_80,usm_0.66_1.00_0.01/new%20logo%202.webp",
            handler: demoSuccessHandler
        }
        window.r = new Razorpay(options);
        r.open()
    }

    function getData(id,discount){
        $.ajax({
          url: "{!!route('user.package.data')!!}",
          type:"get",
          data:{
                id:id,
            },
          success:function(response){
            console.log(response);
            if(response) {
                var obj =  response.data;
                document.getElementById("package_price").innerHTML=eval(JSON.stringify(obj.price));
                document.getElementById("payable").innerHTML=obj.price-((obj.price*discount)/100);
                document.getElementById("price").value=obj.price-((obj.price*discount)/100);
            }
          },
        });
    }

// History API Prevent back button after payment
if( window.history && window.history.pushState ){

history.pushState( "nohb", null, "" );
$(window).on( "popstate", function(event){
  if( !event.originalEvent.state ){
	history.pushState( "nohb", null, "" );
	return;
  }
});
}
</script>
@endsection