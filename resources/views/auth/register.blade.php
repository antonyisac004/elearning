<!DOCTYPE html>
<html lang="en">

<head>

	<!-- META ============================================= -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="keywords" content="" />
	<meta name="author" content="" />
	<meta name="robots" content="" />
	
	<!-- DESCRIPTION -->
	<meta name="description" content="Elearning : Membership| Subscription" />
	
	<!-- OG -->
	<meta property="og:title" content="Elearning : Membership| Subscription" />
	<meta property="og:description" content="Elearning : Membership| Subscription" />
	<meta property="og:image" content="" />
	<meta name="format-detection" content="telephone=no">
	
	<!-- FAVICONS ICON ============================================= -->
	<link rel="icon" href="assets/images/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" type="image/x-icon" href="assets/images/favicon.png" />
	
	<!-- PAGE TITLE HERE ============================================= -->
	<title>Elearning : Membership| Subscription </title>
	
	<!-- MOBILE SPECIFIC ============================================= -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<!--[if lt IE 9]>
	<script src="assets/js/html5shiv.min.js"></script>
	<script src="assets/js/respond.min.js"></script>
	<![endif]-->
	
	<!-- All PLUGINS CSS ============================================= -->
	<link rel="stylesheet" type="text/css" href="<?php echo url('/'); ?>/assets/web/css/assets.css">
	
	<!-- TYPOGRAPHY ============================================= -->
	<link rel="stylesheet" type="text/css" href="<?php echo url('/'); ?>/assets/web/css/typography.css">
	
	<!-- SHORTCODES ============================================= -->
	<link rel="stylesheet" type="text/css" href="<?php echo url('/'); ?>/assets/web/css/shortcodes/shortcodes.css">
	
	<!-- STYLESHEETS ============================================= -->
	<link rel="stylesheet" type="text/css" href="<?php echo url('/'); ?>/assets/web/css/style.css">
	<link class="skin" rel="stylesheet" type="text/css" href="<?php echo url('/'); ?>/assets/web/css/color/color-1.css">
	
</head>
<body id="bg">
<div class="page-wraper">
<div id="loading-icon-bx"></div>

	<!-- Header Top ==== -->
    <header class="header rs-nav">
		<div class="top-bar">
			<div class="container">
				<div class="row d-flex justify-content-between">
					<div class="topbar-left">
						<ul>
							<li><a href="javascript:;"><i class="fa fa-question-circle"></i>Ask a Question</a></li>
							<li><a href="javascript:;"><i class="fa fa-envelope-o"></i>Support@website.com</a></li>
						</ul>
					</div>
					<div class="topbar-right">
						<ul>
							<!-- <li>
								<select class="header-lang-bx">
									<option data-icon="flag flag-uk">English UK</option>
									<option data-icon="flag flag-us">English US</option>
								</select>
							</li> -->
							<li><a href="{{ route('user.login') }}">Login</a></li>
							<li><a href="{{ route('membership.index') }}">Membership</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="sticky-header navbar-expand-lg">
            <div class="menu-bar clearfix">
                <div class="container clearfix">
					<!-- Header Logo ==== -->
					<div class="menu-logo">
						<a href="<?php echo url('/'); ?>"><img src="<?php echo url('/'); ?>/assets/web/images/logo.png" alt=""></a>
					</div>
					<!-- Mobile Nav Button ==== -->
                    <button class="navbar-toggler collapsed menuicon justify-content-end" type="button" data-toggle="collapse" data-target="#menuDropdown" aria-controls="menuDropdown" aria-expanded="false" aria-label="Toggle navigation">
						<span></span>
						<span></span>
						<span></span>
					</button>
					<!-- Author Nav ==== -->
                    <div class="secondary-menu">
                        <div class="secondary-inner">
                            <ul>
								<li><a href="javascript:;" class="btn-link"><i class="fa fa-facebook"></i></a></li>
								<li><a href="javascript:;" class="btn-link"><i class="fa fa-twitter"></i></a></li>
								<li><a href="javascript:;" class="btn-link"><i class="fa fa-whatsapp"></i></a></li>
								<!-- Search Button ==== -->
								<!-- <li class="search-btn"><button id="quik-search-btn" type="button" class="btn-link"><i class="fa fa-search"></i></button></li> -->
							</ul>
						</div>
                    </div>
					<!-- Search Box ==== -->
                    <div class="nav-search-bar">
                        <form action="#">
                            <input name="search" value="" type="text" class="form-control" placeholder="Type to search">
                            <span><i class="ti-search"></i></span>
                        </form>
						<span id="search-remove"><i class="ti-close"></i></span>
                    </div>
					<!-- Navigation Menu ==== -->
                    <div class="menu-links navbar-collapse collapse justify-content-start" id="menuDropdown">
						<div class="menu-logo">
							<a href="<?php echo url('/'); ?>"><img src="<?php echo url('/'); ?>/assets/web/images/logo.png" alt=""></a>
						</div>
                        <ul class="nav navbar-nav">	
							<li><a href="<?php echo url('/'); ?>">Home </a></li>
							<li><a href="javascript:;">About</a>
							</li>
							<!-- <li class="add-mega-menu"><a href="javascript:;">Our Courses <i class="fa fa-chevron-down"></i></a>
								<ul class="sub-menu add-menu">
									<li class="add-menu-left">
										<h5 class="menu-adv-title">Our Courses</h5>
										<ul>
											<li><a href="courses.html">Courses </a></li>
											<li><a href="courses-details.html">Courses Details</a></li>
											<li><a href="profile.html">Instructor Profile</a></li>
											<li><a href="event.html">Upcoming Event</a></li>
										</ul>
									</li>
									<li class="add-menu-right">
										<img src="<?php echo url('/'); ?>/assets/web/images/adv/adv.jpg" alt=""/>
									</li>
								</ul>
							</li> -->
							<!-- <li><a href="javascript:;">Blog <i class="fa fa-chevron-down"></i></a>
								<ul class="sub-menu">
									<li><a href="blog-classic-grid.html">Blog Classic</a></li>
									<li><a href="blog-classic-sidebar.html">Blog Classic Sidebar</a></li>
									<li><a href="blog-list-sidebar.html">Blog List Sidebar</a></li>
									<li><a href="blog-standard-sidebar.html">Blog Standard Sidebar</a></li>
									<li><a href="blog-details.html">Blog Details</a></li>
								</ul>
							</li> -->
							<li class="active nav-dashboard"><a href="{{ route('membership.index') }}">Membership</a></li>
							<li class="nav-dashboard"><a href="javascript:;">Contact</a></li>
						</ul>
						<div class="nav-social-link">
							<a href="javascript:;"><i class="fa fa-facebook"></i></a>
							<a href="javascript:;"><i class="fa fa-google-plus"></i></a>
							<a href="javascript:;"><i class="fa fa-linkedin"></i></a>
						</div>
                    </div>
					<!-- Navigation Menu END ==== -->
                </div>
            </div>
        </div>
    </header>
    <!-- header END ==== -->
    <!-- Content -->
    <div class="page-content bg-white">
        <!-- inner page banner -->
        <!-- <div class="page-banner ovbl-dark" style="background-image:url(assets/images/banner/banner2.jpg);">
            <div class="container">
                <div class="page-banner-entry">
                    <h1 class="text-white">Membership</h1>
				 </div>
            </div>
        </div> -->
		<!-- Breadcrumb row -->
		<div class="breadcrumb-row">
			<div class="container">
				<ul class="list-inline">
					<li><a href="<?php echo url('/'); ?>">Home</a></li>
					<li>Buy Package</li>
				</ul>
			</div>
		</div>
		<!-- Breadcrumb row END -->
        <!-- inner page banner END -->
		<div class="content-block">
            <!-- About Us -->
			<div class="section-area section-sp2">
                <div class="container">
					<div class="row">
						<div class="col-md-12 heading-bx text-center">
							<!-- Sign up Form -->
							<div class="profile-head">
								<h3><i class="fa fa-shopping-cart" aria-hidden="true"></i> Buy Package</h3>
							</div>
							<form class="edit-profile js-validation-signup" action="{{ route('register.store') }}" method="post">
								@csrf
								@method('POST')
								<div class="">
									<div class="form-group row">
										<div class="col-12 col-sm-8 col-md-8 col-lg-9 ml-auto">
											<h3>Package Information</h3>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-12 col-sm-4 col-md-4 col-lg-3 col-form-label">Change your Package</label>
										<div class="col-12 col-sm-8 col-md-8 col-lg-7">
										<select name="package_id" class="form-control " required>
											@foreach ($packages as $package)
											<option value="{{ $package->id }}" @if($package->id==$package_data->id)
											selected
											@endif>
											{{ $package->name .'- $'. @$package->price }} per month
											</option>
											@endforeach
										</select>  
										<div class="invalid-feedback">{{ $errors->first('package_id') }}</div>
										</div>
									</div>
									<div class="form-group row">
										<div class="col-12 col-sm-8 col-md-8 col-lg-9 ml-auto">
											<h3>Personal Information</h3>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-12 col-sm-4 col-md-4 col-lg-3 col-form-label">First Name *</label>
										<div class="col-12 col-sm-8 col-md-8 col-lg-7">
											<input type="text" class="form-control @error('first_name') is-invalid @enderror" id="first_name" name="first_name" 
                                                        value="{{ old('first_name') }}" placeholder="First name" 
														   autofocus>
                                                   
                                            @error('first_name')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
										</div>
									</div>
									<div class="form-group row">
										<label class="col-12 col-sm-4 col-md-4 col-lg-3 col-form-label">Last Name *</label>
										<div class="col-12 col-sm-8 col-md-8 col-lg-7">
											<input type="text" class="form-control @error('last_name') is-invalid @enderror" id="last_name" name="last_name"
                                                         value="{{ old('last_name') }}" placeholder="Last name"  >
                                            @error('last_name')
                                            	<div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
										</div>
									</div>
									<div class="form-group row">
										<label class="col-12 col-sm-4 col-md-4 col-lg-3 col-form-label">Email *</label>
										<div class="col-12 col-sm-8 col-md-8 col-lg-7">
											<input type="email" class="form-control @error('email') is-invalid @enderror" id="email" name="email"
                                                         value="{{ old('email') }}" placeholder="Email" >
                                            @error('email')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
										</div>
									</div>
									<div class="form-group row">
										<label class="col-12 col-sm-4 col-md-4 col-lg-3 col-form-label">Phone *</label>
										<div class="col-12 col-sm-8 col-md-8 col-lg-7">
											<input type="number" class="form-control @error('phone') is-invalid @enderror" id="phone" name="phone" 
                                                        value="{{ old('phone') }}" placeholder="Phone" >
                                            @error('phone')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
										</div>
									</div>
									<div class="form-group row">
										<label class="col-12 col-sm-4 col-md-4 col-lg-3 col-form-label">Password *
										<a  data-toggle="tooltip" title="Your password must be at least 10 characters in length,
                                                    must contain at least one lowercase letter,
                                                    must contain at least one uppercase letter,
                                                    must contain at least one digit,
                                                    must contain a special character"
                                        ><i class="fa fa-info-circle" aria-hidden="true"></i></a>
										</label>
										<div class="col-12 col-sm-8 col-md-8 col-lg-7">
											<input type="password" class="form-control @error('password') is-invalid @enderror" id="password" name="password" 
                                                placeholder="......."
                                                data-toggle="password"
                                                  >
                                            @error('password')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
										</div>
									</div>
									<div class="form-group row">
										<label class="col-12 col-sm-4 col-md-4 col-lg-3 col-form-label">Password Confirmation *</label>
										<div class="col-12 col-sm-8 col-md-8 col-lg-7">
											<input type="password" class="form-control @error('password_confirmation') is-invalid @enderror" id="password_confirmation"
                                                    name="password_confirmation"  placeholder="........"
                                                    onmouseover="this.type='text'"
                                                    onmouseout="this.type='password'"
                                                      >
                                            @error('password_confirmation')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
										</div>
									</div>
								</div>
								<div class="row" style="float:right;">
									<div class="col-12 col-sm-4 col-md-4 col-lg-3">
									</div>
									<div class="col-12 col-sm-8 col-md-8 col-lg-7">
									<button type="submit" class="btn"><i class="fa fa-plus mr-10"></i> Proceed to Payment</button>
									</div>
								</div>
												
							</form>
							<!-- END Sign up Form -->
						</div>
					</div>
				</div>
            </div> 
		</div>
    </div>
    <!-- Content END-->
	<!-- Footer ==== -->
    <footer>
        <div class="footer-top">
			<div class="pt-exebar">
				<div class="container">
					<div class="d-flex align-items-stretch">
						<div class="pt-logo mr-auto">
							<a href="<?php echo url('/'); ?>"><img src="<?php echo url('/'); ?>/assets/web/images/logo-white.png" alt=""/></a>
						</div>
						<div class="pt-social-link">
							<ul class="list-inline m-a0">
								<li><a href="#" class="btn-link"><i class="fa fa-facebook"></i></a></li>
								<li><a href="#" class="btn-link"><i class="fa fa-twitter"></i></a></li>
								<li><a href="#" class="btn-link"><i class="fa fa-linkedin"></i></a></li>
								<li><a href="#" class="btn-link"><i class="fa fa-google-plus"></i></a></li>
							</ul>
						</div>
						<div class="pt-btn-join">
							<a href="{{ route('membership.index') }}" class="btn ">Join Now</a>
						</div>
					</div>
				</div>
			</div>
            <div class="container">
                <div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 text-center"> <a>Elearning</a></div>
					<!-- <div class="col-lg-4 col-md-12 col-sm-12 footer-col-4">
                        <div class="widget">
                            <h5 class="footer-title">Sign Up For A Newsletter</h5>
							<p class="text-capitalize m-b20">Weekly Breaking news analysis and cutting edge advices on job searching.</p>
                            <div class="subscribe-form m-b20">
								<form class="subscription-form" action="http://educhamp.themetrades.com/demo/assets/script/mailchamp.php" method="post">
									<div class="ajax-message"></div>
									<div class="input-group">
										<input name="email" required="required"  class="form-control" placeholder="Your Email Address" type="email">
										<span class="input-group-btn">
											<button name="submit" value="Submit" type="submit" class="btn"><i class="fa fa-arrow-right"></i></button>
										</span> 
									</div>
								</form>
							</div>
                        </div>
                    </div>
					<div class="col-12 col-lg-5 col-md-7 col-sm-12">
						<div class="row">
							<div class="col-4 col-lg-4 col-md-4 col-sm-4">
								<div class="widget footer_widget">
									<h5 class="footer-title">Company</h5>
									<ul>
										<li><a href="index.html">Home</a></li>
										<li><a href="about-1.html">About</a></li>
										<li><a href="faq-1.html">FAQs</a></li>
										<li><a href="contact-1.html">Contact</a></li>
									</ul>
								</div>
							</div>
							<div class="col-4 col-lg-4 col-md-4 col-sm-4">
								<div class="widget footer_widget">
									<h5 class="footer-title">Get In Touch</h5>
									<ul>
										<li><a href="http://educhamp.themetrades.com/admin/index.html">Dashboard</a></li>
										<li><a href="blog-classic-grid.html">Blog</a></li>
										<li><a href="portfolio.html">Portfolio</a></li>
										<li><a href="event.html">Event</a></li>
									</ul>
								</div>
							</div>
							<div class="col-4 col-lg-4 col-md-4 col-sm-4">
								<div class="widget footer_widget">
									<h5 class="footer-title">Courses</h5>
									<ul>
										<li><a href="courses.html">Courses</a></li>
										<li><a href="courses-details.html">Details</a></li>
										<li><a href="membership.html">Membership</a></li>
										<li><a href="profile.html">Profile</a></li>
									</ul>
								</div>
							</div>
						</div>
                    </div>
					<div class="col-12 col-lg-3 col-md-5 col-sm-12 footer-col-4">
                        <div class="widget widget_gallery gallery-grid-4">
                            <h5 class="footer-title">Our Gallery</h5>
                            <ul class="magnific-image">
								<li><a href="assets/images/gallery/pic1.jpg" class="magnific-anchor"><img src="assets/images/gallery/pic1.jpg" alt=""></a></li>
								<li><a href="assets/images/gallery/pic2.jpg" class="magnific-anchor"><img src="assets/images/gallery/pic2.jpg" alt=""></a></li>
								<li><a href="assets/images/gallery/pic3.jpg" class="magnific-anchor"><img src="assets/images/gallery/pic3.jpg" alt=""></a></li>
								<li><a href="assets/images/gallery/pic4.jpg" class="magnific-anchor"><img src="assets/images/gallery/pic4.jpg" alt=""></a></li>
								<li><a href="assets/images/gallery/pic5.jpg" class="magnific-anchor"><img src="assets/images/gallery/pic5.jpg" alt=""></a></li>
								<li><a href="assets/images/gallery/pic6.jpg" class="magnific-anchor"><img src="assets/images/gallery/pic6.jpg" alt=""></a></li>
								<li><a href="assets/images/gallery/pic7.jpg" class="magnific-anchor"><img src="assets/images/gallery/pic7.jpg" alt=""></a></li>
								<li><a href="assets/images/gallery/pic8.jpg" class="magnific-anchor"><img src="assets/images/gallery/pic8.jpg" alt=""></a></li>
							</ul>
                        </div>
                    </div> -->
                </div>
            </div>
        </div>
        <!-- <div class="footer-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 text-center"> <a>Elearning</a></div>
                </div>
            </div>
        </div> -->
    </footer>
    <!-- Footer END ==== -->
    <button class="back-to-top fa fa-chevron-up" ></button>
</div>
<!-- External JavaScripts -->
<script src="<?php echo url('/'); ?>/assets/web/js/jquery.min.js"></script>
<script src="<?php echo url('/'); ?>/assets/web/vendors/bootstrap/js/popper.min.js"></script>
<script src="<?php echo url('/'); ?>/assets/web/vendors/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo url('/'); ?>/assets/web/vendors/bootstrap-select/bootstrap-select.min.js"></script>
<script src="<?php echo url('/'); ?>/assets/web/vendors/bootstrap-touchspin/jquery.bootstrap-touchspin.js"></script>
<script src="<?php echo url('/'); ?>/assets/web/vendors/magnific-popup/magnific-popup.js"></script>
<script src="<?php echo url('/'); ?>/assets/web/vendors/counter/waypoints-min.js"></script>
<script src="<?php echo url('/'); ?>/assets/web/vendors/counter/counterup.min.js"></script>
<script src="<?php echo url('/'); ?>/assets/web/vendors/imagesloaded/imagesloaded.js"></script>
<script src="<?php echo url('/'); ?>/assets/web/vendors/masonry/masonry.js"></script>
<script src="<?php echo url('/'); ?>/assets/web/vendors/masonry/filter.js"></script>
<script src="<?php echo url('/'); ?>/assets/web/vendors/owl-carousel/owl.carousel.js"></script>
<script src="<?php echo url('/'); ?>/assets/web/js/functions.js"></script>
<script src="<?php echo url('/'); ?>/assets/web/js/contact.js"></script>
<!-- <script src='assets/vendors/switcher/switcher.js'></script> -->
<script src="https://unpkg.com/bootstrap-show-password@1.2.1/dist/bootstrap-show-password.min.js"></script>
<!-- Page JS Plugins -->
<script src="<?php echo url('/'); ?>/assets/admin/js/plugins/jquery-validation/jquery.validate.min.js"></script>
<script>
        $(function() {
        $('.js-validation-signup').validate({
				errorClass: 'invalid-feedback animated fadeInDown',
                errorElement: 'div',
                errorPlacement: (error, e) => {
                    $(e).parents('.form-group > div').append(error);
                },
                highlight: e => {
                    $(e).closest('.form-group').removeClass('is-invalid').addClass('is-invalid');
                },
                success: e => {
                    $(e).closest('.form-group').removeClass('is-invalid');
                    $(e).remove();
                },
                rules: {
                    first_name:{
                        required: true, 
                    },
                    last_name:{
                        required: true, 
                    }
                    email: {
                        required: true,
                        email: true
                    },
                    phone: {
                        required: true,
                        minlength: 10
                    },
                    password: {
                        required: true,
                        minlength: 8
                    },
                    password_confirmation: {
                        required: true,
                        minlength: 8
                    }
                },
                messages: {
                    first_name: {
                        required: 'Please enter first name',
                    },
                    last_name: {
                        required: 'Please enter last name',
                    },
                    email: {
                        required: 'Please enter an email address',
                        email: 'Please provide a valid email address'
                    },
                    phone: {
                        required: 'Please enter phone',
                        minlength: 'Your phone number must be at least 10 numbers long'
                    },
                    password: {
                        required: 'Please provide a password',
                        minlength: 'Your password must be at least 8 characters long'
                    }
                    password_confirmation: {
                        required: 'Please confirm password provided above',
                        minlength: 'Your password must be at least 8 characters long'
                    }
                }
            });
        });
</script>
</body>

</html>
