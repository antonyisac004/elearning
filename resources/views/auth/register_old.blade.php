@extends('layouts.app')

@section('content')

                <!-- Page Content -->
                <div class="bg-body-dark bg-pattern" style="background-image: url('<?php echo url('/'); ?>/assets/admin/media/various/bg-pattern-inverse.png');">
                    <div class="row mx-0 justify-content-center">
                        <div class="hero-static col-lg-6 col-xl-12">
                            <div class="content content-full overflow-hidden">
                                <!-- Header -->
                                <div class="py-30 text-center">
                                    <a class="link-effect font-w700" href="{{ url('/home') }}">
        
                                    <span class="font-size-xl text-primary-dark">{{ config('app.name') }}</span>
                                    </a>
                                    <h1 class="h4 font-w700 mt-30 mb-10">Buy Your Package </h1>
                                    
                                    
                                </div>
                                <!-- END Header -->

                                <!-- Sign up Form -->
                               
                                <form class="js-validation-signup" action="{{ route('register.store') }}" method="post">
                                @csrf
                                @method('POST')
                        
                                    <div class="block block-themed block-rounded block-shadow">
                                        <div class="block-header bg-gd-emerald">
                                            <h3 class="block-title">Package Information</h3>
                                            <div class="block-options">
                                                <button type="button" class="btn-block-option">
                                                <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                                                </button>
                                            </div>
                                        </div>
                                        <div class="block-content">

                                            <div class="form-group row ">
                                                <div class="col-12" >
                                                    <label for="package_id">Change Your Package </label>
                                                    <select name="package_id" class="form-control " required>

                                                        @foreach ($packages as $package)
                                                        <option value="{{ $package->id }}" @if($package->id==$package_data->id)
                                                        selected
                                                        @endif>
                                                        {{ $package->name .'- $'. @$package->price }} per month
                                                        </option>
                                                        @endforeach
                                                    </select>   

                                                    <div class="invalid-feedback">{{ $errors->first('package_id') }}</div>

                                                </div>
                                            </div>
                                           
                                    </div>
                                    <div class="block block-themed block-rounded block-shadow">

                                        <div class="block-header bg-gd-emerald">
                                            <h3 class="block-title">Personal Information</h3>
                                            <div class="block-options">
                                                <button type="button" class="btn-block-option">
                                                <i class="fa fa-user" aria-hidden="true"></i>
                                                </button>
                                            </div>
                                        </div>

                                        <div class="block-content">

                                        <div class="form-group row">
                                                <div class="col-6">
                                                    <label for="first_name">First name*</label>
                                                    <input type="text" class="form-control @error('first_name') is-invalid @enderror" id="first_name" name="first_name" 
                                                        value="{{ old('first_name') }}" placeholder="First name" required  autofocus>
                                                   
                                                    @error('first_name')
                                                        <div class="invalid-feedback">{{ $message }}</div>
                                                    @enderror
                                                </div>

                                                <div class="col-6">
                                                    <label for="last_name">Last name*</label>
                                                    <input type="text" class="form-control @error('last_name') is-invalid @enderror" id="last_name" name="last_name"
                                                         value="{{ old('last_name') }}" placeholder="Last name" required  >
                                                    @error('last_name')
                                                        <div class="invalid-feedback">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            
                                            <div class="form-group row">
                                                <div class="col-6">
                                                    <label for="email">Email*</label>
                                                    <input type="email" class="form-control @error('email') is-invalid @enderror" id="email" name="email"
                                                         value="{{ old('email') }}" placeholder="Email" required  >
                                                    @error('email')
                                                        <div class="invalid-feedback">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                                <div class="col-6">
                                                    <label for="phone">Phone*</label>
                                                    <input type="number" class="form-control @error('phone') is-invalid @enderror" id="phone" name="phone" 
                                                        value="{{ old('phone') }}" placeholder="Phone" required  >
                                                    @error('phone')
                                                        <div class="invalid-feedback">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                
                                            </div>
                                            
                                            <div class="form-group row">
                                                <div class="col-6">
                                                    <label for="password">Password   </label>  <a  data-toggle="tooltip" title="Your password must be at least 10 characters in length,
                                                    must contain at least one lowercase letter,
                                                    must contain at least one uppercase letter,
                                                    must contain at least one digit,
                                                    must contain a special character"
                                                    >  <i class="fa fa-info-circle" aria-hidden="true"></i></a>
                                                    
                                                   
                                                    <input type="password" class="form-control @error('password') is-invalid @enderror" id="password" name="password" 
                                                    placeholder="......."
                                                    data-toggle="password"
                                                    required  >
                                                    @error('password')
                                                     <div class="invalid-feedback">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                                <div class="col-6">
                                                    <label for="password_confirmation ">Password Confirmation*</label>
                                                    <input type="password" class="form-control @error('password_confirmation') is-invalid @enderror" id="password_confirmation"
                                                    name="password_confirmation"  placeholder="........"
                                                    onmouseover="this.type='text'"
                                                    onmouseout="this.type='password'"
                                                    required  >
                                                    @error('password_confirmation')
                                                     <div class="invalid-feedback">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>

                                            <div class="form-group row mb-0">
                                                
                                                <div class="col-sm-12 text-sm-right push">
                                                    <button type="submit" class="btn btn-alt-success">
                                                        <i class="fa fa-plus mr-10"></i> Proceed to Payment
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- <div class="block-content bg-body-light">
                                            <div class="form-group text-center">
                                                <a class="link-effect text-muted mr-10 mb-5 d-inline-block" href="#">
                                                    <i class="fa fa-user text-muted mr-5"></i> Sign In
                                                </a>
                                            </div>
                                        </div> -->

                                        </div>

                                    </div>
                                    
                                </form>
                                <!-- END Sign up Form -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END Page Content -->

@endsection
@section('js_after')
<script src="https://unpkg.com/bootstrap-show-password@1.2.1/dist/bootstrap-show-password.min.js"></script>
        <script>
        $(function() {
        $('.js-validation-signup').validate({
                errorClass: 'invalid-feedback animated fadeInDown',
                errorElement: 'div',
                errorPlacement: (error, e) => {
                    $(e).parents('.form-group > div').append(error);
                },
                highlight: e => {
                    $(e).closest('.form-group').removeClass('is-invalid').addClass('is-invalid');
                },
                success: e => {
                    $(e).closest('.form-group').removeClass('is-invalid');
                    $(e).remove();
                },
                rules: {
                    first_name:{
                        required: true, 
                    },
                    last_name:{
                        required: true, 
                    }
                    email: {
                        required: true,
                        email: true
                    },
                    phone: {
                        required: true,
                        minlength: 10
                    },
                    password: {
                        required: true,
                        minlength: 8
                    },
                    password_confirmation: {
                        required: true,
                        minlength: 8
                    }
                },
                messages: {
                    first_name: {
                        required: 'Please enter first name',
                    },
                    last_name: {
                        required: 'Please enter last name',
                    },
                    email: {
                        required: 'Please enter an email address',
                        email: 'Please provide a valid email address'
                    },
                    phone: {
                        required: 'Please enter phone',
                        minlength: 'Your phone number must be at least 10 numbers long'
                    },
                    password: {
                        required: 'Please provide a password',
                        minlength: 'Your password must be at least 8 characters long'
                    }
                    password_confirmation: {
                        required: 'Please confirm password provided above',
                        minlength: 'Your password must be at least 8 characters long'
                    }
                }
            });
        });
    </script>
@endsection    
    