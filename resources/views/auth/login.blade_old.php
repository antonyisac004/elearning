@extends('layouts.app')

@section('content')

                <!-- Page Content -->
                <div class="bg-body-dark bg-pattern" style="background-image: url('<?php echo url('/'); ?>/assets/admin/media/various/bg-pattern-inverse.png');">
                    <div class="row mx-0 justify-content-center">
                        <div class="hero-static col-lg-6 col-xl-4">
                            <div class="content content-full overflow-hidden">
                                <!-- Header -->
                                <div class="py-30 text-center">
                                    <a class="link-effect font-w700" href="#">
        
                                    <span class="font-size-xl text-primary-dark">{{ config('app.name') }}</span>
                                    </a>
                                    <h1 class="h4 font-w700 mt-30 mb-10">User Signin</h1>
                                    
                                    
                                </div>
                                <!-- END Header -->

                                <!-- Sign In Form -->
                               
                                <form class="js-validation-signin" action="{{ route('user.login') }}" method="post">
                                    @csrf
                                    <div class="block block-themed block-rounded block-shadow">

                                        <div class="block-header bg-gd-dusk">
                                            <h3 class="block-title">{{ __('administration.sign in') }}</h3>
                                            <div class="block-options">
                                                <button type="button" class="btn-block-option">
                                                    <i class="si si-wrench"></i>
                                                </button>
                                            </div>
                                        </div>
                                        
                                        <div class="block-content">
                                            <div class="form-group row">
                                                <div class="col-12">
                                                    <label for="email">{{ __('administration.email label') }}</label>
                                                    <input type="email" class="form-control @if ($errors->has('email')) is-invalid  @endif" placeholder="admin@gmail.com" id="email" name="email" value="{{ old('email') }}" autofocus>
                                                    <div class="invalid-feedback">{{ $errors->first('email') }}</div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-12">
                                                    <label for="password">{{ __('administration.password label') }}</label>
                                                    <input type="password" class="form-control @if ($errors->has('password')) is-invalid  @endif" placeholder="........" id="password" name="password">
                                                    <div class="invalid-feedback">{{ $errors->first('password') }}</div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-12">
                                                    {!! NoCaptcha::renderJs() !!}
                                                    {!! app('captcha')->display() !!}
                                                    @if ($errors->has('g-recaptcha-response'))
                                                    <div class="invalid-feedback">{{ $errors->first('g-recaptcha-response') }}</div>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group row mb-0">
                                                <div class="col-sm-6 d-sm-flex align-items-center push">
                                                    <div class="custom-control custom-checkbox mr-auto ml-0 mb-0">
                                                        <input type="checkbox" class="custom-control-input" id="login-remember-me" name="login-remember-me" {{ old('login-remember-me') ? 'checked' : '' }} >
                                                        <label class="custom-control-label" for="login-remember-me">{{ __('administration.remember label') }}</label>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 text-sm-right push">
                                                    <button type="submit" class="btn btn-alt-primary">
                                                        <i class="si si-login mr-10"></i> Sign In
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="block-content bg-body-light">
                                            <div class="form-group text-center">
            
                                                <a class="link-effect text-muted mr-10 mb-5 d-inline-block" href="{{ route('password.request') }}">
                                                    <i class="fa fa-warning mr-5"></i> {{ __('administration.forgot password text') }}
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <!-- END Sign In Form -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END Page Content -->

@endsection
@section('js_after')

    <script>
        $('.js-validation-signin').validate({
                errorClass: 'invalid-feedback animated fadeInDown',
                errorElement: 'div',
                errorPlacement: (error, e) => {
                    $(e).parents('.form-group > div').append(error);
                },
                highlight: e => {
                    $(e).closest('.form-group').removeClass('is-invalid').addClass('is-invalid');
                },
                success: e => {
                    $(e).closest('.form-group').removeClass('is-invalid');
                    $(e).remove();
                },
                rules: {
                    email: {
                        required: true,
                        email: true
                    },
                    password: {
                        required: true,
                        minlength: 8
                    }
                },
                messages: {
                    email: {
                        required: 'Please enter an email address',
                        email: 'Please provide a valid email address'
                    },
                    password: {
                        required: 'Please provide a password',
                        minlength: 'Your password must be at least 8 characters long'
                    }
                }
            });
    </script>
@endsection    
    