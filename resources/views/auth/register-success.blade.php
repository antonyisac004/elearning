@extends('layouts.app')

@section('content')
<section class="header4 cid-rP36ZFFR42 mbr-fullscreen" id="extContent23-15">
    <div class="container">
        <div class="row justify-content-md-center">
            <div class="media-content col-lg-7 col-md-8">
                <div class="mbr-figure">
                    <img src="{{ asset('assets/images/mbr-17.png') }}" alt="Green Society" title="Green Society">
                </div>
                <h1 class="mbr-section-title align-center mbr-black pb-3 mbr-bold mbr-fonts-style display-1">
                    Registration
                    Successful
                </h1>

                <div class="mbr-text align-center mbr-white">
                    <p class="mbr-text mbr-black mbr-fonts-style display-7">
                    You are registered but the site administrator must review your account, you will not be able to login until your account has been approved.</p>
                </div>

            </div>

        </div>
    </div>
</section>
@endsection