@extends('layouts.app')

@section('content')

<div class="bg-body-dark bg-pattern" style="background-image: url('assets/media/various/bg-pattern-inverse.png');">
                    <div class="row mx-0 justify-content-center">
                        <div class="hero-static col-lg-6 col-xl-4">
                            <div class="content content-full overflow-hidden">
                                <!-- Header -->
                                <div class="py-30 text-center">
                                    <a class="link-effect font-w700" href="#">
                                        <span class="font-size-xl text-primary-dark">{{ config('app.name') }}</span>
                                    </a>
                                    <h1 class="h4 font-w700 mt-30 mb-10">Reset Your Password</h1>
                                    <h2 class="h5 font-w400 text-muted mb-0">Please add your new password</h2>
                                </div>
                                <!-- END Header -->

                                <!-- Sign up Form -->
                                
                                <form class="js-validation-signup" action="{{ route('password.update') }}" method="post">
                                    @csrf
                                    <input type="hidden" name="token" value="{{ $token }}">
                                    <div class="block block-themed block-rounded block-shadow">
                                        <div class="block-header bg-gd-primary">
                                            <h3 class="block-title">Reset Password</h3>
                                            <div class="block-options">
                                                <button type="button" class="btn-block-option">
                                                    <i class="si si-wrench"></i>
                                                </button>
                                            </div>
                                        </div>
                                        <div class="block-content">

                                            <div class="form-group row">
                                                <div class="col-12">
                                                    <label for="email">Email Address</label>
                                                    <input type="email" class="form-control 
                                                     @if ($errors->has('email')) is-invalid  @endif" id="email" name="email" placeholder="admin@gmail.com" 
                                                     value="{{ $email ?? old('email') }}" autocomplete="email" autofocus>
                                                    <div class="invalid-feedback">{{ $errors->first('email') }}</div>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <div class="col-12">
                                                    <label for="password">Password</label>
                                                    <input type="password" class="form-control 
                                                     @if ($errors->has('password')) is-invalid  @endif" id="password" name="password" 
                                                     placeholder="......." >
                                                    <div class="invalid-feedback">{{ $errors->first('password') }}</div>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <div class="col-12">
                                                    <label for="password_confirmation">Password Confirmation</label>
                                                    <input type="password" class="form-control 
                                                     @if ($errors->has('password_confirmation')) is-invalid  @endif" id="password_confirmation" name="password_confirmation" 
                                                     placeholder="......." >
                                                    <div class="invalid-feedback">{{ $errors->first('password_confirmation') }}</div>
                                                </div>
                                            </div>

                                            

                                            <div class="form-group text-center">
                                                <button type="submit" class="btn btn-alt-primary">
                                                    <i class="fa fa-asterisk mr-10"></i> Reset Password
                                                </button>
                                            </div>
                                        </div>
                                        <div class="block-content bg-body-light">
                                            <div class="form-group text-center">
                                                <a class="link-effect text-muted mr-10 mb-5 d-inline-block" href="{{ route('user.login') }}">
                                                    <i class="fa fa-user text-muted mr-5"></i> Sign In
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <!-- END Reminder Form -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END Page Content -->

@endsection 