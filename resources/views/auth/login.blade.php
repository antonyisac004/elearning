<!DOCTYPE html>
<html lang="en">


<head>

	<!-- META ============================================= -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="keywords" content="" />
	<meta name="author" content="" />
	<meta name="robots" content="" />
	
	<!-- DESCRIPTION -->
	<meta name="description" content="{{ config('app.name') }}" />
	
	<!-- OG -->
	<meta property="og:title" content="{{ config('app.name') }}" />
	<meta property="og:description" content="{{ config('app.name') }}" />
	<meta property="og:image" content="" />
	<meta name="format-detection" content="telephone=no">
	
	<!-- FAVICONS ICON ============================================= -->
	<link rel="icon" href="<?php echo url('/'); ?>/assets/web/images/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo url('/'); ?>/assets/web/images/favicon.png" />
	
	<!-- PAGE TITLE HERE ============================================= -->
	<title>{{ config('app.name') }} </title>
	
	<!-- MOBILE SPECIFIC ============================================= -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<!--[if lt IE 9]>
	<script src="assets/js/html5shiv.min.js"></script>
	<script src="assets/js/respond.min.js"></script>
	<![endif]-->
	
	<!-- All PLUGINS CSS ============================================= -->
	<link rel="stylesheet" type="text/css" href="<?php echo url('/'); ?>/assets/web/css/assets.css">
	
	<!-- TYPOGRAPHY ============================================= -->
	<link rel="stylesheet" type="text/css" href="<?php echo url('/'); ?>/assets/web/css/typography.css">
	
	<!-- SHORTCODES ============================================= -->
	<link rel="stylesheet" type="text/css" href="<?php echo url('/'); ?>/assets/web/css/shortcodes/shortcodes.css">
	
	<!-- STYLESHEETS ============================================= -->
	<link rel="stylesheet" type="text/css" href="<?php echo url('/'); ?>/assets/web/css/style.css">
	<link class="skin" rel="stylesheet" type="text/css" href="<?php echo url('/'); ?>/assets/web/css/color/color-1.css">
	
</head>
<body id="bg">
<div class="page-wraper">
	<div id="loading-icon-bx"></div>
	<div class="account-form">
		<div class="account-head" style="background-image:url(<?php echo url('/'); ?>/assets/web/images/background/bg2.jpg);">
			<a href="<?php echo url('/'); ?>"><img src="<?php echo url('/'); ?>/assets/web/images/logo-white.png" alt=""></a>
		</div>
		<div class="account-form-inner">
			<div class="account-container">
				<div class="heading-bx left">
					<h2 class="title-head">Login to your <span>Account</span></h2>
					<!-- <p>Don't have an account? <a href="register.html">Create one here</a></p> -->
				</div>	
				<form class="contact-bx js-validation-signin" action="{{ route('user.login') }}" method="post">
					@csrf
					<div class="row placeani">
						<div class="col-lg-12">
							<div class="form-group">
								<div class="input-group">
									<!-- <label for="email">Your {{ __('administration.email label') }}</label> -->
									<input type="email" 
									class="form-control @if ($errors->has('email')) is-invalid  @endif"
									id="email" name="email" value="{{ old('email') }}" placeholder="Your {{ __('administration.email label') }}" autofocus>
									<div class="invalid-feedback">{{ $errors->first('email') }}</div>
								</div>
							</div>
						</div>
						<div class="col-lg-12">
							<div class="form-group">
								<div class="input-group"> 
									<!-- <label for="oassword">Your {{ __('administration.password label') }}</label> -->
									<input type="password" class="form-control @if ($errors->has('password')) is-invalid  @endif"
									placeholder="Your {{ __('administration.password label') }}"
									id="password" name="password">
									<div class="invalid-feedback">{{ $errors->first('password') }}</div>
								</div>
							</div>
						</div>
						<div class="col-lg-12">
							<div class="form-group">
								{!! NoCaptcha::renderJs() !!}
                                {!! app('captcha')->display() !!}
                                @if ($errors->has('g-recaptcha-response'))
                                    <div class="invalid-feedback">{{ $errors->first('g-recaptcha-response') }}</div>
                                @endif
							</div>
						</div>
						<div class="col-lg-12">
							<div class="form-group form-forget">
								<div class="custom-control custom-checkbox">
									<input type="checkbox" class="custom-control-input" id="customControlAutosizing" name="customControlAutosizing"  {{ old('customControlAutosizing') ? 'checked' : '' }}>
									<label class="custom-control-label" for="customControlAutosizing">{{ __('administration.remember label') }}</label>
								</div>
								<a href="{{ route('password.request') }}" class="ml-auto">{{ __('administration.forgot password text') }}</a>
							</div>
						</div>
						<div class="col-lg-12 m-b30">
							<button name="submit" type="submit" value="Submit" class="btn button-md">Login</button>
						</div>
						<!-- <div class="col-lg-12">
							<h6>Login with Social media</h6>
							<div class="d-flex">
								<a class="btn flex-fill m-r5 facebook" href="#"><i class="fa fa-facebook"></i>Facebook</a>
								<a class="btn flex-fill m-l5 google-plus" href="#"><i class="fa fa-google-plus"></i>Google Plus</a>
							</div>
						</div> -->
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<!-- External JavaScripts -->
<script src="<?php echo url('/'); ?>/assets/web/js/jquery.min.js"></script>
<script src="<?php echo url('/'); ?>/assets/web/vendors/bootstrap/js/popper.min.js"></script>
<script src="<?php echo url('/'); ?>/assets/web/vendors/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo url('/'); ?>/assets/web/vendors/bootstrap-select/bootstrap-select.min.js"></script>
<script src="<?php echo url('/'); ?>/assets/web/vendors/bootstrap-touchspin/jquery.bootstrap-touchspin.js"></script>
<script src="<?php echo url('/'); ?>/assets/web/vendors/magnific-popup/magnific-popup.js"></script>
<script src="<?php echo url('/'); ?>/assets/web/vendors/counter/waypoints-min.js"></script>
<script src="<?php echo url('/'); ?>/assets/web/vendors/counter/counterup.min.js"></script>
<script src="<?php echo url('/'); ?>/assets/web/vendors/imagesloaded/imagesloaded.js"></script>
<script src="<?php echo url('/'); ?>/assets/web/vendors/masonry/masonry.js"></script>
<script src="<?php echo url('/'); ?>/assets/web/vendors/masonry/filter.js"></script>
<script src="<?php echo url('/'); ?>/assets/web/vendors/owl-carousel/owl.carousel.js"></script>
<script src="<?php echo url('/'); ?>/assets/web/js/functions.js"></script>
<script src="<?php echo url('/'); ?>/assets/web/js/contact.js"></script>
<!-- <script src='<?php echo url('/'); ?>/assets/web/vendors/switcher/switcher.js'></script> -->
<!-- Page JS Plugins -->
<script src="<?php echo url('/'); ?>/assets/admin/js/plugins/jquery-validation/jquery.validate.min.js"></script>
<script>
        $('.js-validation-signin').validate({
                errorClass: 'invalid-feedback animated fadeInDown',
                errorElement: 'div',
                errorPlacement: (error, e) => {
                    $(e).parents('.form-group > div').append(error);
                },
                highlight: e => {
                    $(e).closest('.form-group').removeClass('is-invalid').addClass('is-invalid');
                },
                success: e => {
                    $(e).closest('.form-group').removeClass('is-invalid');
                    $(e).remove();
                },
                rules: {
                    email: {
                        required: true,
                        email: true
                    },
                    password: {
                        required: true,
                        minlength: 8
                    },
                },
                messages: {
                    email: {
                        required: 'Please enter your email address',
                        email: 'Please provide a valid email address'
                    },
                    password: {
                        required: 'Please enter your password',
                        minlength: 'Your password must be at least 8 characters long'
                    },
                }
            });
    </script>
</body>

</html>
