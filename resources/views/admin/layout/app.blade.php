<!doctype html>
<html lang="en" class="no-focus">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

        <title>{{ config('app.name') }}</title>

        <meta name="description" content="{{ config('app.name') }}">
        <meta name="author" content="Antony Isac">
        <meta name="robots" content="noindex, nofollow">

        <!-- Open Graph Meta -->
        <meta property="og:title" content="{{ config('app.name') }}">
        <meta property="og:site_name" content="{{ config('app.name') }}">
        <meta property="og:description" content="{{ config('app.name') }}">
        <meta property="og:type" content="website">
        <meta property="og:url" content="">
        <meta property="og:image" content="">

        <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <link rel="shortcut icon" href="<?php echo url('/'); ?>/assets/admin/media/favicons/favicon.png">
        <link rel="icon" type="image/png" sizes="192x192" 
        href="<?php echo url('/'); ?>/assets/admin/media/favicons/favicon-192x192.png">
        <link rel="apple-touch-icon" sizes="180x180" 
        href="<?php echo url('/'); ?>/assets/admin/media/favicons/apple-touch-icon-180x180.png">
        <!-- END Icons -->

        <!-- Stylesheets -->

        <!-- Datatables Plugins CSS -->
        <link rel="stylesheet" 
        href="<?php echo url('/'); ?>/assets/admin/js/plugins/datatables/dataTables.bootstrap4.css">

        <!-- Fonts and Codebase framework -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Muli:300,400,400i,600,700">
        <link rel="stylesheet" id="css-main" href="<?php echo url('/'); ?>/assets/admin/css/codebase.min.css">

        @yield('css_after')
    </head>
    <body>

        
        <div id="page-container" 
            class="sidebar-o sidebar-inverse enable-page-overlay side-scroll page-header-fixed main-content-narrow">
           
           
            <nav id="sidebar">
                <!-- Sidebar Content -->
                <div class="sidebar-content">
                    <!-- Side Header -->
                    <div class="content-header content-header-fullrow px-15">
                        <!-- Mini Mode -->
                        <div class="content-header-section sidebar-mini-visible-b">
                            <!-- Logo -->
                            <span class="content-header-item font-w700 font-size-xl float-left animated fadeIn">
                                <span class="text-dual-primary-dark">i</span><span class="text-primary">z</span>
                            </span>
                            <!-- END Logo -->
                        </div>
                        <!-- END Mini Mode -->

                        <!-- Normal Mode -->
                        <div class="content-header-section text-center align-parent sidebar-mini-hidden">
                            <!-- Close Sidebar, Visible only on mobile screens -->
                            <!-- Layout API, functionality initialized in Template._uiApiLayout() -->
                            <button type="button" class="btn btn-circle btn-dual-secondary d-lg-none align-v-r"
                             data-toggle="layout" data-action="sidebar_close">
                                <i class="fa fa-times text-danger"></i>
                            </button>
                            <!-- END Close Sidebar -->

                            <!-- Logo -->
                            <div class="content-header-item">
                                <a class="link-effect font-w700" href="{{ route('admin.home') }}">
                                    <span class="font-size-xl text-dual-primary-primary">{{ config('app.name') }}</span>
                                </a>
                            </div>
                            <!-- END Logo -->
                        </div>
                        <!-- END Normal Mode -->
                    </div>
                    <!-- END Side Header -->

                    <!-- Side User -->
                    <div class="content-side content-side-full content-side-user px-10 align-parent">
                        <!-- Visible only in mini mode -->
                        <div class="sidebar-mini-visible-b align-v animated fadeIn">
                            <img class="img-avatar img-avatar32" 
                            src="<?php echo url('/'); ?>/assets/admin/media/avatars/avatar15.jpg" alt="">
                        </div>
                        <!-- END Visible only in mini mode -->

                        <!-- Visible only in normal mode -->
                        <div class="sidebar-mini-hidden-b text-center">
                            <a class="img-link" href="{{ route('admin.profile') }}">
                                <img class="img-avatar" 
                                src="<?php echo url('/'); ?>/assets/admin/media/avatars/avatar15.jpg" alt="">
                            </a>
                            <ul class="list-inline mt-10">
                                <li class="list-inline-item">
                                    <a class="link-effect text-dual-primary-dark font-size-xs font-w600 text-uppercase"
                                     href="{{ route('admin.profile') }}">{{ Auth::user()->name }}</a>
                                </li>
                            </ul>
                        </div>
                        <!-- END Visible only in normal mode -->
                    </div>
                    <!-- END Side User -->

                    <!-- Side Navigation -->
                    <div class="content-side content-side-full">
                        <ul class="nav-main">
               
                            <li>
                                <a class="{{ (request()->is('admin/home')) ? 'active' : '' }}"
                                     href="{{ route('admin.home') }}"><i class="fa fa-dashboard"></i><span 
                                     class="sidebar-mini-hide">Dashboard</span></a>
                            </li>

                            <li class="{{   (request()->is('admin/reports*')) ? 'open' : '' }}">
                                <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i 
                                class="fa fa-bar-chart"></i><span class="sidebar-mini-hide">Reports</span></a>
                                <ul>
                                    <li  class="{{ (request()->is('admin/reports*'))? 'open' : '' }}">
                                        <a class="nav-submenu" 
                                        href="{{ route('admin.reports.index') }}">Payment Reports</a>
                                    </li>
                                </ul>
                            </li>

                            <li class="{{   (request()->is('admin/category*')) ? 'open' : '' }}">
                                <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i 
                                class="fa fa-code-fork"></i><span class="sidebar-mini-hide">Categories</span></a>
                                <ul>
                                    <li>
                                        <a class="{{ (request()->is('admin/category/create')) ? 'active' : '' }}" 
                                            href="{{ route('admin.category.create') }}">Add</a>
                                    </li>
                                    <li>
                                        <a class="{{ (request()->is('admin/category/index')) ? 'active' : '' }}" 
                                            href="{{ route('admin.category.index') }}">View</a>
                                    </li>
                                </ul>
                            </li>

                            <li class="{{   (request()->is('admin/users*')) ? 'open' : '' }}">
                                <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i 
                                class="fa fa-users"></i><span class="sidebar-mini-hide">Users </span></a>
                                <ul>
                                    <li  class="{{ (request()->is('admin/users*'))? 'open' : '' }}">
                                        <a class="nav-submenu" href="{{ route('admin.users.index') }}">Users List</a>
                                    </li>
                                </ul>
                            </li>

                            <li class="{{   (request()->is('admin/packages*')) || 
                                            (request()->is('admin/package-features*')) ? 'open' : '' }}">
                                <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i 
                                class="fa fa-credit-card"></i><span class="sidebar-mini-hide">Subscriptions </span></a>
                                <ul>
                                    <li  class="{{ (request()->is('admin/packages*'))? 'open' : '' }}">
                                        <a class="nav-submenu" data-toggle="nav-submenu" href="#">Packages</a>
                                        <ul>
                                            <li>
                                                <a class="{{ (request()->is('admin/packages/create')) ? 'active' : '' }}" 
                                                    href="{{ route('admin.packages.create') }}">Add</a>
                                            </li>
                                            <li>
                                                <a  class="{{ (request()->is('admin/packages/index')) ? 'active' : '' }}" 
                                                    href="{{ route('admin.packages.index') }}">View</a>
                                            </li>
                    
                                        </ul>
                                    </li>
                                    <li class="{{ (request()->is('admin/package-features*')) ? 'open' : '' }}">
                                        <a class="nav-submenu" data-toggle="nav-submenu" href="#">
                                        <span class="sidebar-mini-hide">Package Features</span></a>
                                            <ul>
                                                <li>
                                                <a 
                                                class="{{ (request()->is('admin/package-features/create')) ? 'active' : '' }}" 
                                                href="{{ route('admin.package-features.create') }}">Add</a>
                                                </li>
                                                <li>
                                                <a 
                                                class="{{ (request()->is('admin/package-features/index')) ? 'active' : '' }}" 
                                                href="{{ route('admin.package-features.index') }}">View</a>
                                                </li>
                                                        
                                            </ul>
                                    </li>
                                </ul>
                            </li>
                            <li class="{{   (request()->is('admin/recorded-videos*')) ||
                                (request()->is('admin/live-videos*')) ? 'open' : '' }}">
                                <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i 
                                class="fa fa-certificate"></i><span class="sidebar-mini-hide">Courses </span></a>
                                <ul>
                                    <li  class="{{ (request()->is('admin/live-videos*'))  ? 'open' : '' }}">
                                        <a class="nav-submenu" data-toggle="nav-submenu" href="#">Live Courses</a>
                                        <ul>
                                            <li>
                                                <a class="{{ (request()->is('admin/live-videos/create')) ? 'active' : '' }}" 
                                                    href="{{ route('admin.live-videos.create') }}">Add</a>
                                            </li>
                                            <li>
                                                <a  class="{{ (request()->is('admin/live-videos/index')) ? 'active' : '' }}" 
                                                    href="{{ route('admin.live-videos.index') }}">View</a>
                                            </li>
                    
                                        </ul>
                                    </li>
                                    <li  class="{{ (request()->is('admin/recorded-videos*'))? 'open' : '' }}">
                                        <a class="nav-submenu" data-toggle="nav-submenu" href="#">Recorded Courses</a>
                                        <ul>
                                            <li>
                                                <a class="{{ (request()->is('admin/recorded-videos/create')) ? 'active' : '' }}" 
                                                    href="{{ route('admin.recorded-videos.create') }}">Add</a>
                                            </li> 
                                            <li>
                                                <a  class="{{ (request()->is('admin/recorded-videos/index')) ? 'active' : '' }}" 
                                                    href="{{ route('admin.recorded-videos.index') }}">View</a>
                                            </li>
                    
                                        </ul>
                                    </li>
                                </ul>
                                <li class="{{ (request()->is('admin/admin-activity*') 
                                            || request()->is('admin/user-activity*')) ? 'open' : '' }}">
                                    <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i 
                                    class="fa fa-history"></i><span class="sidebar-mini-hide">Activities </span></a>
                                    <ul>
                                        <li  class="{{ (request()->is('admin/admin-activity/index*'))? 'active' : '' }}">
                                            <a class="nav-submenu" href="{{ route('admin.admin-activity.index') }}">Admin</a>
                                        </li>
                                        <li  class="{{ (request()->is('admin/user-activity/index*'))? 'active' : '' }}">
                                            <a class="nav-submenu" href="{{ route('admin.user-activity.index') }}">User</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                           
                            
                        </ul>
                    </div>
                    <!-- END Side Navigation -->
                </div>
                <!-- Sidebar Content -->
            </nav>
            <!-- END Sidebar -->

            <!-- Header -->
            <header id="page-header">
                <!-- Header Content -->
                <div class="content-header">
                    <!-- Left Section -->
                    <div class="content-header-section">
                        <!-- Toggle Sidebar -->
                        <!-- Layout API, functionality initialized in Template._uiApiLayout() -->
                        <button type="button" class="btn btn-circle btn-dual-secondary" data-toggle="layout" data-action="sidebar_toggle">
                            <i class="fa fa-navicon"></i>
                        </button>
                        <!-- END Toggle Sidebar -->

                       
                    </div>
                    <!-- END Left Section -->

                    <!-- Right Section -->
                    <div class="content-header-section">
                        <!-- User Dropdown -->
                        <div class="btn-group" role="group">
                            <button type="button" class="btn btn-rounded btn-dual-secondary" id="page-header-user-dropdown" 
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fa fa-user d-sm-none"></i>
                                <span class="d-none d-sm-inline-block">{{ Auth::user()->name }}</span>
                                <i class="fa fa-angle-down ml-5"></i>
                            </button>
                            <div class="dropdown-menu dropdown-menu-right min-width-200" aria-labelledby="page-header-user-dropdown">
                                <h5 class="h6 text-center py-10 mb-5 border-b text-uppercase">{{ Auth::user()->name }}</h5>
                                <a class="dropdown-item" href="{{ route('admin.profile') }}">
                                    <i class="si si-user mr-5"></i> Profile
                                </a>
                               
                                <!-- Toggle Side Overlay -->
                                <!-- Layout API, functionality initialized in Template._uiApiLayout() -->
                                <a class="dropdown-item" href="{{ route('admin.settings.index') }}" >
                                    <i class="si si-wrench mr-5"></i> Settings
                                </a>
                                <!-- END Side Overlay -->

                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="{{ route('admin.logout') }}"
                                onclick="event.preventDefault(); document.getElementById('logout-form').submit();" >
                                    <i class="si si-logout mr-5"></i> Sign Out
                                </a>
                                <form id="logout-form" action="{{ route('admin.logout') }}" method="POST"
                                style="display: none;">
                                @csrf
                            </form>
                            </div>
                        </div>
                        <!-- END User Dropdown -->

                        <!-- Notifications -->
                        <!-- <div class="btn-group" role="group">
                            <button type="button" class="btn btn-rounded btn-dual-secondary" id="page-header-notifications"
                             data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fa fa-flag"></i>
                                <span class="badge badge-primary badge-pill">5</span>
                            </button>
                            <div class="dropdown-menu dropdown-menu-right min-width-300" aria-labelledby="page-header-notifications">
                                <h5 class="h6 text-center py-10 mb-0 border-b text-uppercase">Notifications</h5>
                                <ul class="list-unstyled my-20">
                                    <li>
                                        <a class="text-body-color-dark media mb-15" href="javascript:void(0)">
                                            <div class="ml-5 mr-15">
                                                <i class="fa fa-fw fa-check text-success"></i>
                                            </div>
                                            <div class="media-body pr-10">
                                                <p class="mb-0">You’ve upgraded to a VIP account successfully!</p>
                                                <div class="text-muted font-size-sm font-italic">15 min ago</div>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="text-body-color-dark media mb-15" href="javascript:void(0)">
                                            <div class="ml-5 mr-15">
                                                <i class="fa fa-fw fa-exclamation-triangle text-warning"></i>
                                            </div>
                                            <div class="media-body pr-10">
                                                <p class="mb-0">Please check your payment info since we can’t validate them!</p>
                                                <div class="text-muted font-size-sm font-italic">50 min ago</div>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="text-body-color-dark media mb-15" href="javascript:void(0)">
                                            <div class="ml-5 mr-15">
                                                <i class="fa fa-fw fa-times text-danger"></i>
                                            </div>
                                            <div class="media-body pr-10">
                                                <p class="mb-0">Web server stopped responding and it was automatically restarted!</p>
                                                <div class="text-muted font-size-sm font-italic">4 hours ago</div>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="text-body-color-dark media mb-15" href="javascript:void(0)">
                                            <div class="ml-5 mr-15">
                                                <i class="fa fa-fw fa-exclamation-triangle text-warning"></i>
                                            </div>
                                            <div class="media-body pr-10">
                                                <p class="mb-0">Please consider upgrading your plan. You are running out of space.</p>
                                                <div class="text-muted font-size-sm font-italic">16 hours ago</div>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="text-body-color-dark media mb-15" href="javascript:void(0)">
                                            <div class="ml-5 mr-15">
                                                <i class="fa fa-fw fa-plus text-primary"></i>
                                            </div>
                                            <div class="media-body pr-10">
                                                <p class="mb-0">New purchases! +$250</p>
                                                <div class="text-muted font-size-sm font-italic">1 day ago</div>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item text-center mb-0" href="javascript:void(0)">
                                    <i class="fa fa-flag mr-5"></i> View All
                                </a>
                            </div>
                        </div> -->
                        <!-- END Notifications -->

                        
                    </div>
                    <!-- END Right Section -->
                </div>
                <!-- END Header Content -->

                <!-- Header Loader -->
                <!-- Please check out the Activity page under Elements category 
                to see examples of showing/hiding it -->
                <div id="page-header-loader" class="overlay-header bg-primary">
                    <div class="content-header content-header-fullrow text-center">
                        <div class="content-header-item">
                            <i class="fa fa-sun-o fa-spin text-white"></i>
                        </div>
                    </div>
                </div>
                <!-- END Header Loader -->
            </header>
            <!-- END Header -->

            <!-- Main Container -->
            <main id="main-container">

                <!-- Page Content -->
                @yield('content')
                <!-- END Page Content -->

            </main>
            <!-- END Main Container -->

            <!-- Footer -->
             <footer id="page-footer" class="opacity-0">
                <div class="content py-20 font-size-xs clearfix">
                    
                    
                </div>
            </footer>
            <!-- END Footer -->

        </div>
        <!-- END Page Container -->

       
        <script src="<?php echo url('/'); ?>/assets/admin/js/codebase.core.min.js"></script>

       
        <script src="<?php echo url('/'); ?>/assets/admin/js/codebase.app.min.js"></script>

        <!-- Page JS Plugins -->
        <script src="<?php echo url('/'); ?>/assets/admin/js/plugins/chartjs/Chart.bundle.min.js"></script>

        <!-- Page JS Code -->
        <script src="<?php echo url('/'); ?>/assets/admin/js/pages/be_pages_dashboard.min.js"></script>

        <!-- Datatable JS Plugins -->
        <script src="<?php echo url('/'); ?>/assets/admin/js/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="<?php echo url('/'); ?>/assets/admin/js/plugins/datatables/dataTables.bootstrap4.min.js"></script>

        <!-- Datatable JS Code -->
        <script src="<?php echo url('/'); ?>/assets/admin/js/pages/be_tables_datatables.min.js"></script>
        @yield('js_after')
        <script>
        // document.addEventListener('contextmenu', event => event.preventDefault());
        document.onkeydown = function (e) {
        if (event.keyCode == 123) {
            return false;
        }
        if (e.ctrlKey && e.shiftKey && (e.keyCode == 'I'.charCodeAt(0) || e.keyCode == 'i'.charCodeAt(0))) {
            return false;
        }
        if (e.ctrlKey && e.shiftKey && (e.keyCode == 'C'.charCodeAt(0) || e.keyCode == 'c'.charCodeAt(0))) {
            return false;
        }
        if (e.ctrlKey && e.shiftKey && (e.keyCode == 'J'.charCodeAt(0) || e.keyCode == 'j'.charCodeAt(0))) {
            return false;
        }
        if (e.ctrlKey && (e.keyCode == 'U'.charCodeAt(0) || e.keyCode == 'u'.charCodeAt(0))) {
            return false;
        }
        if (e.ctrlKey && (e.keyCode == 'S'.charCodeAt(0) || e.keyCode == 's'.charCodeAt(0))) {
            return false;
        }
        }
        </script>
    </body>
</html>