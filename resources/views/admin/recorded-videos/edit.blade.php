@extends('admin.layout.app')
@section('css_after')
    <link rel="stylesheet" href="{{ asset('assets/admin/js/plugins/flatpickr/flatpickr.min.css')}}">
@endsection
@section('content')
<!-- Page Content -->
<div class="content">
    <nav class="breadcrumb bg-white push">
        <a class="breadcrumb-item" href="{{ route('admin.home') }}">Dashboard</a>
        <a class="breadcrumb-item" href="{{ route('admin.recorded-videos.index') }}">Recorded Videos</a>
        <span class="breadcrumb-item active">Edit</span>
    </nav>

    <div class="block">
        <div class="block-header block-header-default">
            <h3 class="block-title">Edit Video</h3>
        </div>
        <div class="block-content">
            @if (session('error'))
            <div class="alert alert-danger alert-dismissable" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <p class="mb-0">{{ session('error') }}</p>
            </div>
            @endif
            @if (session('success'))
            <div class="alert alert-success alert-dismissable" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <p class="mb-0">{{ session('success') }}</p>
            </div>
            @endif
            <form class="js-validation-packages" action="{{ route('admin.recorded-videos.update',$data->id) }}" method="post" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <h2 class="content-heading pt-1">Video Details</h2>
                <div class="row">
                    <div class="col-md-8 col-md-8 col-sm-12 col-xs-12">
                        <div class="form-group row">
                            <div class="col-md-12 @error('video') is-invalid @enderror">
                                <div class="form-material">
                                    <input type="url" class="form-control" id="video" name="video"
                                        placeholder="Please provide video link" value="{{ @old('video',$data->video_link) }}"
                                        required readonly>
                                    <label for="video">Video Link *</label>
                                </div>
                                @error('video')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-12 @error('title') is-invalid @enderror">
                                <div class="form-material">
                                    <input type="text" class="form-control" id="title" name="title"
                                        placeholder="Please provide video title" value="{{ @old('title',$data->title) }}"
                                        required readonly>
                                    <label for="title">Title *</label>
                                </div>
                                @error('title')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-12 @error('description') is-invalid @enderror">
                                <div class="form-material">
                                    <textarea class="form-control" id="description" name="description" cols="0" rows="10" placeholder="Please provide video description" required readonly>{{ @old('description',$data->description) }}</textarea>
                                    <label for="description">Description *</label>
                                </div>
                                @error('description')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12 @error('thumb') is-invalid @enderror">
                                <div class="form-material">
                                    <input type="hidden" name="thumb" id="thumb" class="form-control">
                                    {{-- <label for="thumb">Set Thumbnail </label> --}}
                                </div>
                                @error('description')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 col-md-4 col-sm-12 col-xs-12">
                        <div class="form-group row">
                            <div class="col-md-12 @error('package[]') is-invalid @enderror">
                                <div class="form-material">
                                    
                                    <table>
                                        
                                            <tr>
                                                <div class="form-group row">
                                                    <div class="col-md-12 @error('package[]') is-invalid @enderror">
                                                        <div class="form-material">
                          
                                                           
                                                        <label for="package[]">Select Packages *</label>
                                                        </div>
                                               
                                                        @foreach ($packages as $package)
                        
                                                        <input type="checkbox" name="package[]" id="package"
                                                        value="{{$package->id}}"
                                                       
                                                      @if($data->packages->contains($package->id)) checked=checked @endif  >
                                                        <label>{{$package->name}}</label><br>
                        
                                                        @endforeach
                        
                                                        @error('package[]')
                                                        <div class="invalid-feedback">{{ $message }}</div>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </tr>
                                        
                                    </table>
                                    
                                </div>
                                @error('package[]')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-12 @error('date') is-invalid @enderror">
                                <div class="form-material">
                                    <input type="text" class="form-control" id="date" name="date" data-allow-input="true" data-enable-time="true" value="{{ @old('date',date('Y-m-d\TH:i', strtotime($data->schedule))) }}" required readonly="true">
                                    <!-- {{-- <input type="datetime-local" class="form-control" id="date" name="date"
                                        placeholder="Please schedule the date & time" value="{{ @old('date',date('Y-m-d\TH:i', strtotime($data->schedule))) }}"
                                        required> --}} -->
                                    <label for="date">Schedule date & time *</label>
                                </div>
                                @error('date')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12 @error('category') is-invalid @enderror">
                                <div class="form-material">
                                    <select name="category" id="category" class="form-control" required >
                                        @foreach ($categories as $category)
                                            <option 
                                            @if(@$category->category==@$data->category->category)
                                            selected 
                                            @endif
                                             value="{{$category->id}}">{{@$category->category}}</option>
                                        @endforeach
                                    </select>
                                    <label for="category">Category *</label>
                                </div>
                                @error('date')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        {{-- <h2 class="content-heading">Video Status</h2>
                        <div class="form-group row">
                            <div class="col-md-6 @error('status') is-invalid @enderror p-0">
                                <label class="col-12">Status *</label>
                                <div class="col-12">
                                    <div class="custom-control custom-radio custom-control-inline mb-5">
                                        <input class="custom-control-input" type="radio" name="status" id="status1" value="1"
                                            @if (@old('status',$data->status)) checked @endif>
                                        <label class="custom-control-label" for="status1">Active</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline mb-5">
                                        <input class="custom-control-input" type="radio" name="status" id="status2" value="0"
                                            @if (!@old('status',$data->status)) checked @endif>
                                        <label class="custom-control-label" for="status2">Inactive</label>
                                    </div>
                                    @error('status')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div> --}}
                        <div class="form-group row">
                            <div class="col-md-9">
                                <button type="submit" class="btn btn-alt-primary">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>               
<!-- END Page Content -->

@endsection
@section('js_after')
<script src="{{ asset('assets/admin/js/plugins/flatpickr/flatpickr.min.js') }}"></script>
<!-- Page JS Code -->
<script src="{{ asset('assets/admin/js/pages/be_forms_plugins.min.js') }}"></script>  
<!-- Page JS Helpers (Flatpickr + BS Datepicker + BS Colorpicker + BS Maxlength + Select2 + Masked Input + Range Sliders + Tags Inputs plugins) -->
<script>jQuery(function(){ Codebase.helpers(['flatpickr', 'datepicker', 'colorpicker', 'maxlength', 'select2', 'masked-inputs', 'rangeslider', 'tags-inputs']); });</script>    
<script>
    
    $('.js-validation-packages').validate({
        errorClass: 'invalid-feedback animated fadeInDown',
                errorElement: 'div',
                errorPlacement: (error, e) => {
                    $(e).parents('.form-group > div').append(error);
                },
                highlight: e => {
                    $(e).closest('.form-group > div').removeClass('is-invalid').addClass('is-invalid');
                },
                success: e => {
                    $(e).closest('.form-group > div').removeClass('is-invalid');
                    $(e).remove();
                }
                messages: {
                price: {
                    pattern: "Please enter a valid number."
                }
        }
    });
</script> 
<script>
    // $(function () {
    //     $("#date").flatpickr({ 
    //         minDate: "today",
    //         allowInput: false
    //     });
    // });    
</script>
@endsection