@extends('admin.layout.app')
@section('css_after')
    <link rel="stylesheet" href="{{ asset('assets/admin/js/plugins/flatpickr/flatpickr.min.css')}}">
@endsection
@section('content')
<!-- Page Content -->
<div class="content">
    <nav class="breadcrumb bg-white push">
        <a class="breadcrumb-item" href="{{ route('admin.home') }}">Dashboard</a>
        <a class="breadcrumb-item" href="{{ route('admin.recorded-videos.index') }}">Videos</a>
        <span class="breadcrumb-item active">Add</span>
    </nav>

    <div class="block">
        <div class="block-header block-header-default">
            <h3 class="block-title">Add Recorded Video</h3>
        </div>
        <div class="block-content">
            @if (session('error'))
            <div class="alert alert-danger alert-dismissable" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <p class="mb-0">{{ session('error') }}</p>
            </div>
            @endif
            @if (session('success'))
            <div class="alert alert-success alert-dismissable" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <p class="mb-0">{{ session('success') }}</p>
            </div>
            @endif
            <form class="js-validation-packages" action="{{ route('admin.recorded-videos.store') }}" method="post" enctype="multipart/form-data">
                @csrf
                <h2 class="content-heading pt-1">Add Recorded Video</h2>
                <div class="row">
                    <div class="col-md-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group row">
                            <div class="col-md-12 @error('meeting_id') is-invalid @enderror">
                                <div class="form-material">
                                    <input type="number" class="form-control" id="meeting_id" name="meeting_id"
                                        placeholder="Please enter Meeting ID" value="{{ @old('meeting_id') }}"
                                        required>
                                    <label for="video">Meeting ID *</label>
                                </div>
                                @error('meeting_id')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12 @error('package[]') is-invalid @enderror">
                                <div class="form-material">
                                    <table>
                                        @foreach ($packages as $package)
                                            <tr>
                                                <td><label>{{$package->name}}</label></td><td><input type="checkbox" name="package[]" id="package[]" value="{{$package->id}}"></td>
                                            </tr>
                                        @endforeach
                                    </table>
                                    <label for="package[]">Select Packages *</label>
                                </div>
                                @error('package')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>   
                        <div class="form-group row">
                            <div class="col-md-9">
                                <button type="submit" class="btn btn-alt-primary">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>               
<!-- END Page Content -->

@endsection
@section('js_after')
<script src="{{ asset('assets/admin/js/plugins/flatpickr/flatpickr.min.js') }}"></script>
<!-- Page JS Code -->
<script src="{{ asset('assets/admin/js/pages/be_forms_plugins.min.js') }}"></script>  
<!-- Page JS Plugins -->
<script src="<?php echo url('/'); ?>/assets/admin/js/plugins/jquery-validation/jquery.validate.min.js"></script>
<!-- Page JS Helpers (Flatpickr + BS Datepicker + BS Colorpicker + BS Maxlength + Select2 + Masked Input + Range Sliders + Tags Inputs plugins) -->
<script>jQuery(function(){ Codebase.helpers(['flatpickr', 'datepicker', 'colorpicker', 'maxlength', 'select2', 'masked-inputs', 'rangeslider', 'tags-inputs']); });</script>
<script> 
    $(function () {
        $("#start_time").flatpickr({ 
            minDate: "today" 
        });
        $("#date").flatpickr({ 
            minDate: "today" 
        });
    });
    $('.js-validation-packages').validate({
        errorClass: 'invalid-feedback animated fadeInDown',
                errorElement: 'div',
                errorPlacement: (error, e) => {
                    $(e).parents('.form-group > div').append(error);
                },
                highlight: e => {
                    $(e).closest('.form-group > div').removeClass('is-invalid').addClass('is-invalid');
                },
                success: e => {
                    $(e).closest('.form-group > div').removeClass('is-invalid');
                    $(e).remove();
                },
                rules: {
                    
                    meeting_id: {
                        required: true,
                    },
                    "package[]": {
                        required: true,
                        minlength: 1
                    }, 
                },
                messages: {
                    meeting_id: {
                        required: 'Please enter meeting id',
                    },
                    package: {
                        required: 'Please choose package',
                        minlength: 'Please select atleast one package'
                    },
                }    
    });
</script> 
@endsection