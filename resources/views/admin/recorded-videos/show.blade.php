@extends('admin.layout.app')

@section('content')
<!-- Page Content -->
<div class="content">
    <nav class="breadcrumb bg-white push">
    <a class="breadcrumb-item" href="{{ route('admin.home') }}">Dashboard</a>
        <a class="breadcrumb-item" href="{{ route('admin.recorded-videos.index') }}">Recorded Videos</a>
        <span class="breadcrumb-item active">Show</span>
    </nav>

    <div class="row py-30">
        <div class="col-md-12 col-xl-12">
            <table class="container table">
                <tr>
                    <td>
                        <video class="container" media="all and (max-width: 480px)" autoplay controls controlsList="nodownload">
                            <source src="{{$video->video_link}}" type="video/mp4">
                        </video>
                    </td>
                </tr>
                <tr>
                    <td>
                        <h4 align="center">{{$video->title}}</h4>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p align="justify">{{$video->description}}</p>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <h5>Applied packages : </h5>
                        @foreach ($video->packages as $package)
                            <span class="badge badge-success">{{$package->name}}</span>
                        @endforeach
                    </td>
                </tr>
                
            </table>
        </div>
    </div>
</div>               
<!-- END Page Content -->

@endsection
@section('js_after')
<script>
$(window).bind('contextmenu', false);
    document.addEventListener('contextmenu', event => event.preventDefault());
    document.onkeydown = function (e) {
        if (event.keyCode == 123) {
            return false;
        }
        if (e.ctrlKey && e.shiftKey && (e.keyCode == 'I'.charCodeAt(0) || e.keyCode == 'i'.charCodeAt(0))) {
            return false;
        }
        if (e.ctrlKey && e.shiftKey && (e.keyCode == 'C'.charCodeAt(0) || e.keyCode == 'c'.charCodeAt(0))) {
            return false;
        }
        if (e.ctrlKey && e.shiftKey && (e.keyCode == 'J'.charCodeAt(0) || e.keyCode == 'j'.charCodeAt(0))) {
            return false;
        }
        if (e.ctrlKey && (e.keyCode == 'U'.charCodeAt(0) || e.keyCode == 'u'.charCodeAt(0))) {
            return false;
        }
        if (e.ctrlKey && (e.keyCode == 'S'.charCodeAt(0) || e.keyCode == 's'.charCodeAt(0))) {
            return false;
        }
    }
</script>
@endsection