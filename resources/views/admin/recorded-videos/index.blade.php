@extends('admin.layout.app')
@section('content')
<!-- Page Content -->
<div class="content">
    <nav class="breadcrumb bg-white push">
        <a class="breadcrumb-item" href="{{ route('admin.home') }}">Dashboard</a>
        <span class="breadcrumb-item active">Recorded Videos</span>
    </nav>

    <!-- Dynamic Table Full -->
    <div class="block">
        <div class="block-header block-header-default">
            <h3 class="block-title">Recorded Videos</h3>
            {{-- <div align="right">
                <select name="package" id="package" class="form-control" onchange="fetchVideo(this.value)">
                    <option value="">All</option>
                    @foreach ($packages as $package)
                        <option value="{{$package->id}}">{{$package->name}}</option>
                    @endforeach
                </select>
            </div> --}}
        </div>
        <div class="block-content block-content-full">

            @if (session('error'))
                <div class="alert alert-danger alert-dismissable" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <p class="mb-0">{{ session('error') }}</p>
                </div>
            @endif
            @if (session('success'))
                <div class="alert alert-success alert-dismissable" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <p class="mb-0">{{ session('success') }}</p>
                </div>
            @endif

            <table id="recorded-videos" class="table table-bordered table-striped table-vcenter">
                <thead>
                    <tr>
                        <th style="text-transform: none !important;">Sl.No</th>
                        <th style="text-transform: none !important;">Meeting Title</th>
                        <th style="text-transform: none !important;">Packages</th>
                        <th style="text-transform: none !important;">Scheduled Date / Time</th>
                        <th style="text-transform: none !important;">Play</th>
                        <th style="text-transform: none !important;">Actions</th>
    
                    </tr>
                </thead>
                <tbody>
                    @if($videos->count())
                    @foreach ($videos as $data)
                    <tr>
                        <td class="text-center"> {{ $loop->iteration }} </td>
                        <td class="font-w600"> {{ @$data->title }}</td>
                        <td class="d-none d-sm-table-cell">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                @foreach ($data->packages as $package)
                                    <span class="badge badge-success">{{ $package->name }}</span>
                                @endforeach
                            </div>
                        </td>
                        <td>
                            {{ Carbon::parse(@$data->schedule)->format('d M Y h:i:s A') }}
                        </td>
                        <td>
                            <a href="{{ route('admin.recorded-videos.show',$data->id) }}"><button type="button" class="btn btn-sm btn-secondary" data-toggle="tooltip"
                            title="Play" style="background:transparent;border:0px;"><i class="fa fa-play" style="color:green;"></i></button></a>
                        </td>
                        <td>
                            <div class="btn-group">
                                <a href="{{ route('admin.recorded-videos.edit',$data->id) }}"
                                        class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Edit"><i
                                            class="fa fa-edit"></i></a>
                                
                                <button type="button" class="btn btn-sm btn-secondary" data-toggle="tooltip"
                                    title="Delete"
                                    onclick="event.preventDefault(); if(confirm('Are you sure to delete this video?')){ document.getElementById('delete-recorded-videos-{{ $data->id }}').submit();}">
                                <i class="fa fa-times"></i> 
                                </button>
                                <form id="delete-recorded-videos-{{ $data->id }}"
                                        action="{{ route('admin.recorded-videos.destroy', $data->id) }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                </form>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                    @else
                    <tr class="no-data">
                        <td colspan="7" class="text-center">No data found</td>
                    </tr>
                    @endif
                </tbody>
                <tfoot>
                    <tr>
                    <th colspan="6">
                        <div style="float:right;">{{ $videos->links() }}</div>
                    </th>
                    </tr>
                </tfoot>
            </table>

            <!-- Courses List -->
            {{-- <div class="row items-push js-gallery" id="gallery">
                @if($videos->count())
                @foreach ($videos as $video)
                <div class="col-md-6 animated fadeIn"  align="center">
                    <div class="options-container">
                        <video width="320" height="240" controls controlsList="nodownload">
                            <source src="{{$video->video_link}}" type="video/mp4">
                        </video> --}}
                        {{-- @if ($video->thumb)
                            <img class="img-fluid options-item" src="{{ asset('storage/'.$video->thumb) }}" alt="" id="thumbnail">
                        @else
                            <img class="img-fluid options-item" src="<?php echo url('/'); ?>/assets/admin/media/photos/thumbnail-video.png" alt="" id="thumbnail">
                        @endif --}}
                        {{-- <div>
                            <div>
                                <h3 class="h4 mb-5">{{Str::limit($video->title, $limit = 25, $end = ' ...')}}</h3>
                                
                                <h4 class="h6 text-white-op mb-15">
                                @foreach ($video->package_recorded_video as $package)
                                        @foreach ($package->getPackage as $data)
                                            <span class="badge badge-success">{{$data->name}}</span>
                                        @endforeach
                                    @endforeach
                                </h4> --}}

                                {{-- <a class="btn btn-sm btn-rounded btn-alt-primary min-width-75" 
                                href="{{ route('admin.recorded-videos.show',$video->id) }}">
                                    <i class="fa fa-play" aria-hidden="true"></i> Play
                                </a>

                                <a  class="btn btn-sm btn-rounded btn-alt-primary min-width-75" 
                                href="{{ route('admin.recorded-videos.edit',$video->id) }}"">
                                <i class="fa fa-pencil" aria-hidden="true"></i> Edit
                                </a>

                                <a class="btn btn-sm btn-rounded btn-alt-danger min-width-75" data-toggle="tooltip"
                                title="Delete"
                                onclick="event.preventDefault();if(confirm('Are you sure to delete this video?')){ document.getElementById('delete-recorded-videos-{{ $video->id }}').submit();}">
                                    <i class="fa fa-times"></i>  Delete
                                </a>

                                <form id="delete-recorded-videos-{{ $video->id }}"
                                    action="{{ route('admin.recorded-videos.destroy', $video->id) }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                </form> --}}

                            {{-- </div>
                        </div>
                    </div>
                </div>
                @endforeach
                @else
                <div class="no-data">
                    <div  class="text-center">No data found</div>
                </div>
                @endif           
            </div> --}}
            <!-- END Courses -->

        
        </div>
    </div>
    <!-- END Dynamic Table Full -->
</div>               
<!-- END Page Content -->

@endsection
@section('js_after')
<script>

    var acc = document.getElementsByClassName("accordion");
    var i;

    for (i = 0; i < acc.length; i++) {
        acc[i].addEventListener("click", function() {
            this.classList.toggle("active");
            var panel = this.nextElementSibling;
            if (panel.style.maxHeight) {
            panel.style.maxHeight = null;
            } else {
            panel.style.maxHeight = panel.scrollHeight + "px";
            } 
        });
    }

    function fetchVideo(id){
        if(id==""){
            location.reload();
        }
        event.preventDefault();
        $.ajax({
          url: "/admin/fetchVideo",
          type:"get",
          data:{
            id:id,
          },
          success:function(response){
            console.log(response);
            if(response) {
                var obj =  response.data;
                document.getElementById("gallery").innerHTML="";
                var bdy="";
                for (var i = 0; i < obj.length; i++) {
                    var id=eval(JSON.stringify(obj[i].id));
                    bdy+='<div class="col-md-6 animated fadeIn" align="center">';
                    bdy+='<div class="options-container">';
                    // bdy+='<img class="img-fluid options-item" src="<?php echo url('/'); ?>/assets/admin/media/photos/thumbnail-video.png" alt="" id="thumbnail">';
                    bdy+='<video width="320" height="240" controls controlsList="nodownload">';
                    bdy+='<source src="' + eval(JSON.stringify(obj[i].video_link)) + '" type="video/mp4">';
                    bdy+='</video>';
                    bdy+='<div>'; 
                    bdy+='<div>';
                    bdy+='<h3 class="h4 mb-5"><a data-toggle="tooltip" title="' + eval(JSON.stringify(obj[i].title)) + '" style="cursor:pointer">' + (eval(JSON.stringify(obj[i].title)).substring(0, 20)).concat(' ...') + '</a></h3>';
                    // bdy+='<a target="_blank" class="btn btn-sm btn-rounded btn-alt-primary min-width-75" href="'+ eval(JSON.stringify(obj[i].video_link)) +'">';
                    // bdy+='<i class="fa fa-play" aria-hidden="true"></i> Play</a>';
                    // bdy+="<button onclick='editVideo(\"" + id + "\")' class='btn btn-sm btn-rounded btn-alt-primary min-width-75'>Edit</button>";
                    // bdy+="<button onclick='deleteVideo(\"" + id + "\")' class='btn btn-sm btn-rounded btn-alt-danger min-width-75'>Delete</button>";
                    bdy+='</div>';
                    bdy+='</div>';     
                    bdy+='</div>';
                    bdy+='</div>';
                    document.getElementById("gallery").innerHTML=bdy;
                }
            }
          },
         });
    }

    function deleteVideo(id){
        if(confirm('Are you sure to delete this video?')){
            $.ajax({
                url: "/admin/deleteVideo",
                type:"get",
                data:{
                    id:id,
                },
                success:function(response){
                    console.log(response);
                    if(response) {
                        alert(response.success);
                        location.reload();
                    }
                },
            });
        }
    }
    function editVideo(id){
        window.location.href = 'recorded-videos/' + id + '/edit';
    }
</script>
<script>
    function openBrowser(url) {
        var curr_browser = navigator.appName;
        if (curr_browser == "Microsoft Internet Explorer") {
            window.opener = self;
        }
        window.open(url, 'null', 'width=900,height=750,toolbar=no,scrollbars=no,location=no,titlebar=no,resizable =no');
        window.moveTo(0, 0);
        window.resizeTo(screen.width, screen.height - 100);
    }
    $(function() {
        $('#recorded-videos').DataTable({
            "dom": '<"pull-left"f><"pull-right"l>tip',
            "paging": false,
            language : {
                sLengthMenu: "Show _MENU_"
            },
            "bInfo" : false
        });
        
    });
</script>
@endsection