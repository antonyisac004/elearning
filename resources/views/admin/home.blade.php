@extends('admin.layout.app')

@section('content')
<!-- Page Content -->
<div class="content">                    
                    <div class="row gutters-tiny invisible" data-toggle="appear">
                        <!-- Row #4 -->
                        <div class="col-md-4">
                            <div class="block block-transparent bg-primary">
                                <div class="block-content block-content-full">
                                    <div class="py-20 text-center">
                                        <div class="mb-20">
                                            <i class="fa fa-bar-chart fa-4x text-primary-light"></i>
                                        </div>
                                        <div class="font-size-h4 font-w600 text-white">Reports</div>
                                        <div class="text-white-op">Contains Payment Reports</div>
                                        <div class="pt-20">
                                            <a class="btn btn-rounded btn-alt-primary" href="{{ route('admin.reports.index') }}">
                                                <i class="fa fa-eye mr-5"></i> View Reports
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="block block-transparent bg-info">
                                <div class="block-content block-content-full">
                                    <div class="py-20 text-center">
                                        <div class="mb-20">
                                            <i class="fa fa-users fa-4x text-info-light"></i>
                                        </div>
                                        <div class="font-size-h4 font-w600 text-white">Users</div>
                                        <div class="text-white-op">Contains Users Lists</div>
                                        <div class="pt-20">
                                            <a class="btn btn-rounded btn-alt-info" href="{{ route('admin.users.index') }}">
                                                <i class="fa fa-eye mr-5"></i> View Users
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="block block-transparent bg-success">
                                <div class="block-content block-content-full">
                                    <div class="py-20 text-center">
                                        <div class="mb-20">
                                            <i class="fa fa-gift fa-4x text-success-light"></i>
                                        </div>
                                        <div class="font-size-h4 font-w600 text-white">Subscription Packages</div>
                                        <div class=" text-white-op">Contains Packages Lists</div>
                                        <div class="pt-20">
                                            <a class="btn btn-rounded btn-alt-success" href="{{ route('admin.packages.index') }}">
                                                <i class="fa fa-eye mr-5"></i> View Packages
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="block block-transparent bg-success">
                                <div class="block-content block-content-full">
                                    <div class="py-20 text-center">
                                        <div class="mb-20">
                                            <i class="fa fa-key fa-4x text-success-light"></i>
                                        </div>
                                        <div class="font-size-h4 font-w600 text-white">Package Features</div>
                                        <div class=" text-white-op">Contains Features Lists</div>
                                        <div class="pt-20">
                                            <a class="btn btn-rounded btn-alt-success" href="{{ route('admin.package-features.index') }}">
                                                <i class="fa fa-eye mr-5"></i> View Features
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="block block-transparent bg-primary">
                                <div class="block-content block-content-full">
                                    <div class="py-20 text-center">
                                        <div class="mb-20">
                                            <i class="fa fa-television fa-4x text-primary-light"></i>
                                        </div>
                                        <div class="font-size-h4 font-w600 text-white">Live Courses</div>
                                        <div class="text-white-op">Contains Live Courses</div>
                                        <div class="pt-20">
                                            <a class="btn btn-rounded btn-alt-primary" href="{{ route('admin.live-videos.index') }}">
                                                <i class="fa fa-eye mr-5"></i> View LiveCourses
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="block block-transparent bg-info">
                                <div class="block-content block-content-full">
                                    <div class="py-20 text-center">
                                        <div class="mb-20">
                                            <i class="fa fa-video-camera fa-4x text-info-light"></i>
                                        </div>
                                        <div class="font-size-h4 font-w600 text-white">Recorded Courses</div>
                                        <div class="text-white-op">Contains Recorded Courses</div>
                                        <div class="pt-20">
                                            <a class="btn btn-rounded btn-alt-info" href="{{ route('admin.recorded-videos.index') }}">
                                                <i class="fa fa-eye mr-5"></i> View RecordedCourses
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        
                        <!-- END Row #4 -->
                    </div>
</div>               
<!-- END Page Content -->

@endsection
@section('js_after')
@endsection