@extends('admin.layout.app')

@section('content')
<!-- Page Content -->
<div class="content">
    <nav class="breadcrumb bg-white push">
        <a class="breadcrumb-item" href="{{ route('admin.home') }}">Dashboard</a>
        <a class="breadcrumb-item" href="{{ route('admin.package-features.index') }}">Package Features</a>
        <span class="breadcrumb-item active">Add</span>
    </nav>

    <div class="block">
        <div class="block-header block-header-default">
            <h3 class="block-title">Add some features for your packages</h3>
        </div>
        <div class="block-content">
            @if (session('error'))
            <div class="alert alert-danger alert-dismissable" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <p class="mb-0">{{ session('error') }}</p>
            </div>
            @endif
            @if (session('success'))
            <div class="alert alert-success alert-dismissable" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <p class="mb-0">{{ session('success') }}</p>
            </div>
            @endif
            <form class="js-validation-packages" action="{{ route('admin.package-features.store') }}" method="post">
                @csrf
                <h2 class="content-heading pt-1">Package Features</h2>
                <div class="form-group row">
                    <div class="col-md-12 @error('feature') is-invalid @enderror">
                        <div class="form-material">
                            <input type="text" class="form-control" id="feature" name="feature"
                                placeholder="Add the feature" value="{{ @old('feature') }}">
                            <label for="feature">Feature *</label>
                        </div>
                        @error('feature')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-12 @error('package') is-invalid @enderror">
                        <div class="form-material">
                            <select name="package" class="form-control" id="package" required>
                                <option value="">Select a Package</option>
                                @foreach ($packages as $package)
                                    <option value="{{$package->id}}">{{$package->name}}</option>
                                @endforeach
                            </select>
                            <label for="package">Package *</label>
                        </div>
                        @error('package')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-12 @error('enabled') is-invalid @enderror">
                        <div class="form-material">
                            <select name="enabled" class="form-control" id="enabled" required>
                                <option value="">Enable / Disable the feature *</option>
                                <option value="1">Enable</option>
                                <option value="0">Disable</option>
                            </select>
                            <label for="enabled">Enabled / Disabled with package</label>
                        </div>
                        @error('enabled')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
               
                <h2 class="content-heading">Feature Status </h2>
                <div class="form-group row">
                    <div class="col-md-6 @error('status') is-invalid @enderror p-0">
                        <label class="col-12">Status *</label>
                        <div class="col-12">
                            <div class="custom-control custom-radio custom-control-inline mb-5">
                                <input class="custom-control-input" type="radio" name="status" id="status1" value="1" checked>
                                <label class="custom-control-label" for="status1">Active</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline mb-5">
                                <input class="custom-control-input" type="radio" name="status" id="status2" value="0">
                                <label class="custom-control-label" for="status2">Inactive</label>
                            </div>
                            @error('status')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-9">
                        <button type="submit" class="btn btn-alt-primary">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>               
<!-- END Page Content -->

@endsection
@section('js_after')
<!-- Page JS Plugins -->
<script src="<?php echo url('/'); ?>/assets/admin/js/plugins/jquery-validation/jquery.validate.min.js"></script>
<script>
    $('.js-validation-packages').validate({
        errorClass: 'invalid-feedback animated fadeInDown',
                errorElement: 'div',
                errorPlacement: (error, e) => {
                    $(e).parents('.form-group > div').append(error);
                },
                highlight: e => {
                    $(e).closest('.form-group > div').removeClass('is-invalid').addClass('is-invalid');
                },
                success: e => {
                    $(e).closest('.form-group > div').removeClass('is-invalid');
                    $(e).remove();
                },
                rules: {
                    feature: {
                        required: true,
                    },
                    package: {
                        required: true,
                    },
                    enabled: {
                        required: true,
                    },
                },
                messages: {
                    feature: {
                        required: 'Please enter feature name',
                    },
                    package: {
                        required: 'Please choose package',
                    },
                    enabled: {
                        required: 'Please choose enable or disable package',
                    },
                }    
    });
</script>     
@endsection