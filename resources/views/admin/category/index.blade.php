@extends('admin.layout.app')

@section('content')
<!-- Page Content -->
<div class="content">
    <nav class="breadcrumb bg-white push">
        <a class="breadcrumb-item" href="{{ route('admin.home') }}">Dashboard</a>
        <span class="breadcrumb-item active">Categories</span>
    </nav>

    <!-- Dynamic Table Full -->
    <div class="block">
        <div class="block-header block-header-default">
            <h3 class="block-title">View Categories</h3>
        </div>
        <div class="block-content block-content-full">

        @if (session('error'))
            <div class="alert alert-danger alert-dismissable" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <p class="mb-0">{{ session('error') }}</p>
            </div>
        @endif
        @if (session('success'))
            <div class="alert alert-success alert-dismissable" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <p class="mb-0">{{ session('success') }}</p>
            </div>
        @endif
       
        <table id="category" class="table table-bordered table-striped table-vcenter ">
            <thead>
                <tr>
                    <th style="text-transform: none !important;">Sl.No</th>
                    <th style="text-transform: none !important;">Category</th>
                    <th style="text-transform: none !important;">Note</th>
                    <th style="text-transform: none !important;">Status</th>
                    <th style="text-transform: none !important;">Actions</th>
                </tr>
            </thead>
            <tbody>
                @if($category->count())
                @foreach ($category as $data)
                <tr>
                    <td class="text-center"> {{ $loop->iteration }} </td>
                    <td class="font-w600"> {{ @$data->category }}</td>
                    <td class="d-none d-sm-table-cell">{{ @$data->note }}</td>
                    <td>
                        @if ($data->status)
                            <span class="badge badge-success">Active</span>
                        @else
                            <span class="badge badge-danger">Inactive</span>
                        @endif
                    </td>
                    <td>
                        <div class="btn-group">
                            <a href="{{ route('admin.category.edit',$data->id) }}"
                                        class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Edit"><i
                                         class="fa fa-edit"></i></a>
                            <button type="button" class="btn btn-sm btn-secondary" data-toggle="tooltip"
                                        title="Delete"
                                        onclick="event.preventDefault(); if(confirm('Are you sure to delete this category?')){ document.getElementById('delete-category-{{ $data->id }}').submit();}">
                                        <i class="fa fa-times"></i>
                            </button>
                            <form id="delete-category-{{ $data->id }}"
                                        action="{{ route('admin.category.destroy', $data->id) }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                            </form>
                        </div>
                    </td>
                </tr>
                @endforeach
                @else
                <tr class="no-data">
                    <td colspan="5" class="text-center">No data found</td>
                </tr>
                @endif
            </tbody>
            <tfoot>
                <tr>
                <th colspan="5">
                    <div style="float:right;">{{ $category->links() }}</div>
                </th>
                </tr>
            </tfoot> 
        </table>   
        </div>
    </div>
    <!-- END Dynamic Table Full -->
    
</div>               
<!-- END Page Content -->

@endsection
@section('js_after')
<script>
    $(function() {
        $('#category').DataTable({
            "dom": '<"pull-left"f><"pull-right"l>tip',
            "paging": false,
            language : {
                sLengthMenu: "Show _MENU_"
            },
            "bInfo" : false
        });
        
    });
</script>
@endsection