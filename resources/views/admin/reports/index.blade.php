@extends('admin.layout.app')

@section('content')
<div class="content">
    <h2 class="content-heading">Reports</h2>
    <nav class="breadcrumb bg-white push">
        <a class="breadcrumb-item" href="{{ route('admin.home') }}">Dashboard</a>
        <span class="breadcrumb-item active">Reports</span>
    </nav>

    <div class="block">
        <div class="block-header block-header-default">
            <h3 class="block-title">Payment Report</h3>
        </div>
        <div class="block-content">
            @if (session('error'))
            <div class="alert alert-danger alert-dismissable" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <p class="mb-0">{{ session('error') }}</p>
            </div>
            @endif
            @if (session('success'))
            <div class="alert alert-success alert-dismissable" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <p class="mb-0">{{ session('success') }}</p>
            </div>
            @endif
            <form class="js-validation-reports" action="{{ route('admin.reports.store') }}" method="post" onsubmit="return validateForm()">
                @csrf
                <div class="form-group row">
                    <div class="col-md-3 @error('from') is-invalid @enderror">
                        <div class="form-material">
                            <input type="date" class="form-control" id="from" name="from"
                                placeholder="Please select from date" value="{{ @old('from',$from) }}" max="<?php echo date("Y-m-d"); ?>" >
                            <label for="date_range">From</label>
                        </div>
                        @error('date_range')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="col-md-3 @error('to') is-invalid @enderror">
                        <div class="form-material">
                            <input type="date" class="form-control" id="to" name="to"
                                placeholder="Please select to date" value="{{ @old('to',$to) }}" max="<?php echo date("Y-m-d"); ?>" >
                            <label for="date_range">to</label>
                        </div>
                        @error('date_range')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="col-md-3">
                        <div class="form-material">
                            <button type="submit" class="btn btn-alt-primary">Search</button>
                        </div>
                    </div>
                </div>
               
            </form>
            <div style="font-weight:bold;font-size:20px;" align="center">
                <p>{{$message}}</p>
            </div>
            <table id="payment-reports" class="table table-bordered table-striped table-vcenter  display nowrap" style="width:100%">

            <thead>
                <tr>
                    <th style="text-transform: none !important;">Sl.No</th>
                    <th style="text-transform: none !important;">User</th>
                    <th style="text-transform: none !important;">Amount</th>
                    <th style="text-transform: none !important;">Date</th>
                </tr>
            </thead>
            <tbody>
                @if($payment->count())
                    @foreach ($payment as $data)
                        <tr>
                            <td> {{ $loop->iteration }} </td>
                            <td> {{ @$data->user->first_name }} {{ @$data->user->last_name }}</td>
                            <td >{{ @$data->amount }}</td>
                            <td >{{ Carbon\Carbon::parse(@$data->created_at)->format('d-M-Y') }}</td>
                        </tr>
                    @endforeach
                @else
                <tr class="no-data">
                    <td colspan="7" class="text-center">No data found</td>
                </tr>
                @endif
            </tbody>
            <tfoot>
                <tr>
                <th colspan="6">
                    <div style="float:right;">{{ $payment->links() }}</div>
                </th>
                </tr>
            </tfoot>
            </table>


        </div>
    </div>
</div>
@endsection
@section('css_after')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.5/css/buttons.dataTables.min.css" />
@endsection
@section('js_after')
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.print.min.js"></script>
<!-- Page JS Plugins -->
<script src="<?php echo url('/'); ?>/assets/admin/js/plugins/jquery-validation/jquery.validate.min.js"></script>


<script>
    function validateForm()
    {
        var from=document.getElementById("from").value;
        var to=document.getElementById("to").value;
        if(from=="" || to=="")
        {
            alert("Kindly fill out from & to dates!");
        }
    }
    $('#payment-reports').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'csv', 
            'excel', 
            //'pdf', 
            //'print'
        ]
    } );
</script>
<script>
    $('.js-validation-reports').validate({
        errorClass: 'invalid-feedback animated fadeInDown',
                errorElement: 'div',
                errorPlacement: (error, e) => {
                    $(e).parents('.form-group > div').append(error);
                },
                highlight: e => {
                    $(e).closest('.form-group > div').removeClass('is-invalid').addClass('is-invalid');
                },
                success: e => {
                    $(e).closest('.form-group > div').removeClass('is-invalid');
                    $(e).remove();
                },
                rules: {
                    from: {
                        required: true,
                    },
                    to: {
                        required: true,
                    },
                },
                messages: {
                    from: {
                        required: 'Please choose from date!',
                    },
                    to: {
                        required: 'Please choose date valid till!',
                    },
                }    
    });
</script>
@endsection