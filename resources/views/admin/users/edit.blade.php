@extends('admin.layout.app')

@section('content')
<!-- Page Content -->
<div class="content">
    <nav class="breadcrumb bg-white push">
        <a class="breadcrumb-item" href="{{ route('admin.home') }}">Dashboard</a>
        <a class="breadcrumb-item" href="{{ route('admin.users.index') }}">Users</a>
        <span class="breadcrumb-item active">Edit</span>
    </nav>

    <div class="block">
        <div class="block-header block-header-default">
            <h3 class="block-title">Edit User</h3>
        </div>
        <div class="block-content">
            @if (session('error'))
            <div class="alert alert-danger alert-dismissable" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <p class="mb-0">{{ session('error') }}</p>
            </div>
            @endif
            @if (session('success'))
            <div class="alert alert-success alert-dismissable" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <p class="mb-0">{{ session('success') }}</p>
            </div>
            @endif
            <form class="js-validation-packages" action="{{ route('admin.users.update',$data->id) }}" method="post">
                @csrf
                @method('PUT')
                <h2 class="content-heading pt-1">User Details</h2>
                <div class="form-group row">
                    <div class="col-md-12 @error('first_name') is-invalid @enderror">
                        <div class="form-material">
                            <input type="text" class="form-control" id="first_name" name="first_name"
                                placeholder="Your first name" value="{{ @old('first_name',$data->first_name) }}"
                                required>
                            <label for="first_name">First Name *</label>
                        </div>
                        @error('first_name')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-12 @error('last_name') is-invalid @enderror">
                        <div class="form-material">
                            <input type="text" class="form-control" id="last_name" name="last_name"
                                placeholder="Your last name" value="{{ @old('last_name',$data->last_name) }}"
                                required>
                            <label for="last_name">Last Name *</label>
                        </div>
                        @error('last_name')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-12 @error('phone') is-invalid @enderror">
                        <div class="form-material">
                            <input type="number" class="form-control" id="phone" name="phone"
                                placeholder="Please provide your phone number" value="{{ @old('phone',$data->phone) }}"
                                required>
                            <label for="number">Phone *</label>
                        </div>
                        @error('phone')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-12 @error('email') is-invalid @enderror">
                        <div class="form-material">
                            <input readonly type="email" class="form-control" id="email" name="email"
                                placeholder="Please provide your email id" value="{{ @old('email',$data->email) }}"
                                required>
                            <label for="email">Email *</label>
                        </div>
                        @error('email')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
               
                <h2 class="content-heading">User Status</h2>
                <div class="form-group row">
                    <div class="col-md-6 @error('status') is-invalid @enderror p-0">
                        <label class="col-12">Status *</label>
                        <div class="col-12">
                            <div class="custom-control custom-radio custom-control-inline mb-5">
                                <input class="custom-control-input" type="radio" name="status" id="status1" value="1"
                                    @if (@old('status',$data->status)) checked @endif>
                                <label class="custom-control-label" for="status1">Active</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline mb-5">
                                <input class="custom-control-input" type="radio" name="status" id="status2" value="0"
                                    @if (!@old('status',$data->status)) checked @endif>
                                <label class="custom-control-label" for="status2">Inactive</label>
                            </div>
                            @error('status')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-9">
                        <button type="submit" class="btn btn-alt-primary">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>               
<!-- END Page Content -->

@endsection
@section('js_after')
<script>
    $('.js-validation-packages').validate({
        errorClass: 'invalid-feedback animated fadeInDown',
                errorElement: 'div',
                errorPlacement: (error, e) => {
                    $(e).parents('.form-group > div').append(error);
                },
                highlight: e => {
                    $(e).closest('.form-group > div').removeClass('is-invalid').addClass('is-invalid');
                },
                success: e => {
                    $(e).closest('.form-group > div').removeClass('is-invalid');
                    $(e).remove();
                }
                messages: {
                price: {
                    pattern: "Please enter a valid number."
                }
        }
    });
</script>    
@endsection