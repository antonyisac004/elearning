@extends('admin.layout.app')

@section('content')
<!-- Page Content -->
<div class="content">
    <nav class="breadcrumb bg-white push">
        <a class="breadcrumb-item" href="{{ route('admin.home') }}">Dashboard</a>
        <span class="breadcrumb-item active">Users</span>
    </nav>

    <!-- Dynamic Table Full -->
    <div class="block">
        <div class="block-header block-header-default">
            <h3 class="block-title">View Users</h3>
        </div>
        <div class="block-content block-content-full">

            @if (session('error'))
                <div class="alert alert-danger alert-dismissable" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <p class="mb-0">{{ session('error') }}</p>
                </div>
            @endif
            @if (session('success'))
                <div class="alert alert-success alert-dismissable" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <p class="mb-0">{{ session('success') }}</p>
                </div>
            @endif
        
            <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                <thead>
                    <tr>
                        <th style="text-transform: none !important;">Sl.No</th>
                        <th style="text-transform: none !important;">First Name</th>
                        <th style="text-transform: none !important;">Last Name</th>
                        <th style="text-transform: none !important;">Phone</th>
                        <th style="text-transform: none !important;">Email</th>
                        <th style="text-transform: none !important;">Status</th>
                        <th style="text-transform: none !important;">Subscriptions</th>
                        <th style="text-transform: none !important;">Expiry Date</th>
                        <th style="text-transform: none !important;">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @if($users->count())
                    @foreach ($users as $data)
                    <tr>
                        <td> {{ $loop->iteration }} </td>
                        <td> {{ @$data->first_name }}</td>
                        <td> {{ @$data->last_name }}</td>
                        <td>{{ @$data->phone }}</td>
                        <td>{{ @$data->email }}</td>
                        
                        <td>
                            @if ($data->status)
                            <span class="badge badge-success">Active</span>
                            @else
                            <span class="badge badge-danger">Inactive</span>
                            @endif
                        </td>
                        <td>
                        @php
                            $now = Carbon\Carbon::now();
                            $renewal_date = Carbon\Carbon::parse(@$data->renewal);
                        @endphp

                        @if($renewal_date >= $now)
                            
                            <button type="button" data-toggle="tooltip" data-placement="top" title="{{ @$data->package->name}} package is currently active" 
                            class="btn btn-sm  btn-alt-success mr-5 mb-5">
                            {{ @$data->package->name}} <i class="fa fa-check"></i>
                                    </button>
                        @else
                            
                            <button type="button" data-toggle="tooltip" data-placement="top" title="{{ @$data->package->name}} package expired.Please renew the package"
                            class="btn btn-sm  btn-alt-danger mr-5 mb-5">
                            {{ @$data->package->name}} <i class="fa fa-times"></i>
                            </button>
                        @endif
                    
                        
                                </td>
                        <td>{{ Carbon\Carbon::parse(@$data->renewal)->format('d-M-Y') }}</td>

                        <td>
                            <div class="btn-group">
                                <a href="{{ route('admin.users.show',$data->id) }}"
                                            class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Show"><i
                                                class="fa fa-eye"></i></a>
                                <a href="{{ route('admin.users.edit',$data->id) }}"
                                            class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Edit"><i
                                            class="fa fa-edit"></i></a>
                                            <button type="button" class="btn btn-sm btn-secondary" data-toggle="tooltip"
                                            title="Delete"
                                            onclick="event.preventDefault(); if(confirm('Are you sure to delete this user?')){ document.getElementById('delete-user-{{ $data->id }}').submit();}">
                                            <i class="fa fa-times"></i>
                                            </button>
                                <form id="delete-user-{{ $data->id }}"
                                            action="{{ route('admin.users.destroy', $data->id) }}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                </form>
                                
                            </div>
                        </td>
                        
                    </tr>
                    @endforeach
                    @else
                    <tr class="no-data">
                        <td colspan="9" class="text-center">No data found</td>
                    </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
    <!-- END Dynamic Table Full -->
</div>               
<!-- END Page Content -->

@endsection
@section('js_after')
@endsection