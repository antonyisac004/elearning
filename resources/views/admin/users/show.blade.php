@php
    use Carbon\Carbon;
@endphp
@extends('admin.layout.app')

@section('content')
<!-- Page Content -->
<div class="content">
    <nav class="breadcrumb bg-white push">
        <a class="breadcrumb-item" href="{{ route('admin.home') }}">Dashboard</a>
        <a class="breadcrumb-item" href="{{ route('admin.users.index') }}">Users</a>
        <span class="breadcrumb-item active">Show</span>
    </nav>

    <div class="block">
        <div class="block-header block-header-default">
            <h3 class="block-title">View User</h3>
        </div>
        <div class="block-content">
            @if (session('error'))
            <div class="alert alert-danger alert-dismissable" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <p class="mb-0">{{ session('error') }}</p>
            </div>
            @endif
            @if (session('success'))
            <div class="alert alert-success alert-dismissable" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <p class="mb-0">{{ session('success') }}</p>
            </div>
            @endif
            <table id="profile" class="table" style="width:100%">
                <tr>
                    <td align="center">
                        <div class="mb-15">
                            <a class="img-link" href="#">
                                @if($user->photo=="")
                                    <img class="img-avatar img-avatar96 img-avatar-thumb" src="<?php echo url('/'); ?>/assets/admin/media/avatars/avatar15.jpg" alt="">
                                @else
                                    <img class="img-avatar img-avatar96 img-avatar-thumb" src="{{ asset('storage/'.$user->photo) }}" alt="">
                                @endif
                            </a>
                        </div>
                        @if (Carbon::parse($user->renewal)>Carbon::now())
                            <button type="button" data-toggle="tooltip" data-placement="top" title="{{ @$data->package->name}} package is currently active" 
                            class="btn btn-sm  btn-alt-success mr-5 mb-5">
                            {{ $user->package->name}} <i class="fa fa-check"></i>
                                    </button>
                        @else
                            <button type="button" data-toggle="tooltip" data-placement="top" title="{{ @$data->package->name}} package expired.Please renew the package"
                            class="btn btn-sm  btn-alt-danger mr-5 mb-5">
                        {{ $user->package->name}} <i class="fa fa-times"></i>
                        </button>
                        @endif
                        <p> </p>
                    </td>
                </tr>
            </table>
            <table id="profile" class="table table-striped table-bordered" style="width:100%">
                <tr>
                    <th>First Name</th>
                    <td>{{$user->first_name}}</td>
                </tr>
                <tr>
                    <th>Last Name</th>
                    <td>{{$user->last_name}}</td>
                </tr>
                <tr>
                    <th>Phone</th>
                    <td>{{$user->phone}}</td>
                </tr>
                <tr>
                    <th>Email ID</th>
                    <td>{{$user->email}}</td>
                </tr>
                <tr>
                    <th>Status</th>
                    <td>
                        @if ($user->status)
                            <span class="badge badge-success">Active</span>
                        @else
                            <span class="badge badge-danger">Inactive</span>
                        @endif
                    </td>
                </tr>
                <tr>
                    <th>Next Renewal</th>
                    @if (Carbon::parse($user->renewal)>Carbon::now())
                        <td>{{Carbon::parse($user->renewal)->format('d M Y')}}</td>
                    @else
                        <td style="color:red;">{{Carbon::parse($user->renewal)->format('d M Y')}}</td>
                    @endif
                </tr>
                <tr>
                    <th>Referral Discount</th>
                    <td>{{$user->discount}} %</td>
                </tr>
                <tr>
                    <th>Action</th>
                    <td><a href="{{ route('admin.users.show',$user->id) }}"
                        class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Show"><i
                            class="fa fa-eye"></i></a>
                        <a href="{{ route('admin.users.edit',$user->id) }}"
                        class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Edit"><i
                         class="fa fa-edit"></i></a>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>               
<!-- END Page Content -->

@endsection
@section('js_after')
<script>
    $('.js-validation-packages').validate({
        errorClass: 'invalid-feedback animated fadeInDown',
                errorElement: 'div',
                errorPlacement: (error, e) => {
                    $(e).parents('.form-group > div').append(error);
                },
                highlight: e => {
                    $(e).closest('.form-group > div').removeClass('is-invalid').addClass('is-invalid');
                },
                success: e => {
                    $(e).closest('.form-group > div').removeClass('is-invalid');
                    $(e).remove();
                }
                messages: {
                price: {
                    pattern: "Please enter a valid number."
                }
        }
    });
</script>    
@endsection