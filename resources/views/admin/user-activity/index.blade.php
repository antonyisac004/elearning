@extends('admin.layout.app')

@section('content')
<!-- Page Content -->
<div class="content">
    <nav class="breadcrumb bg-white push">
        <a class="breadcrumb-item" href="{{ route('admin.home') }}">Dashboard</a>
        <span class="breadcrumb-item active">User Activities</span>
    </nav>

    <!-- Dynamic Table Full -->
    <div class="block">
        <div class="block-header block-header-default">
            <h3 class="block-title">View User Activities</h3>
            <div align="right">
                <button class="btn btn-info" onclick="massDelete()">Delete All</button>
            </div>
        </div>
        <div class="block-content block-content-full">

        @if (session('error'))
            <div class="alert alert-danger alert-dismissable" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <p class="mb-0">{{ session('error') }}</p>
            </div>
        @endif
        @if (session('success'))
            <div class="alert alert-success alert-dismissable" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <p class="mb-0">{{ session('success') }}</p>
            </div>
        @endif
       
        <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
            <thead>
                <tr>
                    <th style="text-transform: none !important;">Sl.No</th>
                    <th style="text-transform: none !important;">Name</th>
                    <th style="text-transform: none !important;">Description</th>
                    <th style="text-transform: none !important;">Delete</th>
                    <th style="text-transform: none !important;">Last Modified</th>
                </tr>
            </thead>
            
            <tbody>
                @if($datas->count())
                @foreach ($datas as $data)
                <tr>
                    <td class="text-center"> {{ $loop->iteration }} </td>
                    <td class="font-w600"> {{ @$data->administrator->first_name }} {{ @$data->administrator->last_name }}</td>
                    <td class="d-none d-sm-table-cell">{{ @$data->subject }}</td>
                    <td align="center">
                        <div class="btn">
                            <button type="button" class="btn btn-sm btn-secondary" data-toggle="tooltip"
                                        title="Delete"
                                        onclick="event.preventDefault(); if(confirm('Are you sure to delete this activity?')){ document.getElementById('delete-user-activity-{{ $data->id }}').submit();}">
                                        <i class="fa fa-times"></i>
                            </button>
                            <form id="delete-user-activity-{{ $data->id }}"
                                        action="{{ route('admin.user-activity.destroy', $data->id) }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                            </form>
                        </div>
                    </td>
                    <td>{{ $data->updated_at->diffForHumans() }}</td>
                </tr>
                @endforeach
                @else
                <tr class="no-data">
                    <td colspan="7" class="text-center">No data found</td>
                </tr>
                @endif
            </tbody>
        </table>
        </div>
    </div>
    <!-- END Dynamic Table Full -->
</div>               
<!-- END Page Content -->

@endsection
@section('js_after')
<script>
    function massDelete() {
        var r = confirm("Are you sure to delete all the activities!");
        if(r==true)
        {
            $.ajax({
                url: "{{ route('admin.user-activity.massDelete') }}",
                type:"get",
                success:function(response){
                    console.log(response);
                    if(response) {
                        alert("Activities deleted successfully")
                        location.reload();
                    }
                },
            });
        }
    }
</script>
@endsection