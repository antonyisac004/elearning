@extends('admin.layout.app')

@section('content')
<!-- Page Content -->
<div class="content">
    <nav class="breadcrumb bg-white push">
    <a class="breadcrumb-item" href="{{ route('admin.home') }}">Dashboard</a>
        <a class="breadcrumb-item" href="{{ route('admin.recorded-videos.index') }}">Recorded Videos</a>
        <span class="breadcrumb-item active">Show</span>
    </nav>

    <div class="row py-30">
        <div class="col-md-12 col-xl-12">
            <table class="container table">
                <tr>
                    <td>
                        <video class="container" width="100%" height="100%" autoplay controls controlsList="nodownload">
                            <source src="{{$video->video_link}}" type="video/mp4">
                        </video>
                    </td>
                </tr>
                <tr>
                    <td>
                        <h4 align="center">{{$video->title}}</h4>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p align="justify">{{$video->description}}</p>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <h5>Asigned to : </h5>
                        @foreach ($video->package as $package)
                            <span class="badge badge-success">{{$package->getPackage->name}}</span>
                        @endforeach
                    </td>
                </tr>
                
            </table>
        </div>
    </div>
</div>               
<!-- END Page Content -->

@endsection
@section('js_after')
<script>
    document.addEventListener('contextmenu', event => event.preventDefault());
    document.onkeydown = function (e) {
        if (event.keyCode == 123) {
            return false;
        }
        if (e.ctrlKey && e.shiftKey && (e.keyCode == 'I'.charCodeAt(0) || e.keyCode == 'i'.charCodeAt(0))) {
            return false;
        }
        if (e.ctrlKey && e.shiftKey && (e.keyCode == 'C'.charCodeAt(0) || e.keyCode == 'c'.charCodeAt(0))) {
            return false;
        }
        if (e.ctrlKey && e.shiftKey && (e.keyCode == 'J'.charCodeAt(0) || e.keyCode == 'j'.charCodeAt(0))) {
            return false;
        }
        if (e.ctrlKey && (e.keyCode == 'U'.charCodeAt(0) || e.keyCode == 'u'.charCodeAt(0))) {
            return false;
        }
        if (e.ctrlKey && (e.keyCode == 'S'.charCodeAt(0) || e.keyCode == 's'.charCodeAt(0))) {
            return false;
        }
    }
</script>
@endsection