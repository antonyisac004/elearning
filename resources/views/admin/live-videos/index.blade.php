@extends('admin.layout.app')

@section('content')
<!-- Page Content -->
<div class="content">
    <nav class="breadcrumb bg-white push">
        <a class="breadcrumb-item" href="{{ route('admin.home') }}">Dashboard</a>
        <span class="breadcrumb-item active">Live Videos</span>
    </nav>

    <!-- Dynamic Table Full -->
    <div class="block">
        <div class="block-header block-header-default">
            <h3 class="block-title">View Live Videos</h3>
        </div>
        <div class="block-content block-content-full">

        @if (session('error'))
            <div class="alert alert-danger alert-dismissable" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <p class="mb-0">{{ session('error') }}</p>
            </div>
        @endif
        @if (session('success'))
            <div class="alert alert-success alert-dismissable" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <p class="mb-0">{{ session('success') }}</p>
            </div>
        @endif
        <h4 align="center">Upcoming Meetings</h4>
        <table id="live-videos" class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                    <th style="text-transform: none !important;">Sl.No</th>
                    <th style="text-transform: none !important;">Meeting Title</th>
                    <th style="text-transform: none !important;">Meeting Id</th>
                    <th style="text-transform: none !important;">Packages</th>
                    <th style="text-transform: none !important;">Scheduled Date / Time</th>
                    <th style="text-transform: none !important;">Category</th>
                    <th style="text-transform: none !important;">Actions</th>

                </tr>
            </thead>
            <tbody>
                @if($videos->count())
                @foreach ($videos as $data)
                <tr>
                    <td class="text-center"> {{ $loop->iteration }} </td>
                    <td class="font-w600"> {{ @$data->title }}</td>
                    <td class="font-w600"> {{ @$data->meeting_id }}</td>
                    <td class="d-none d-sm-table-cell">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            @foreach ($data->packages as $package)
                                <span class="badge badge-success">{{ $package->name }}</span>
                            @endforeach
                        </div>
                    </td>
                    <td>
                        {{ Carbon::parse(@$data->schedule)->format('d M Y h:i:s A') }}
                    </td>
                    <td>
                        {{ @$data->category->category}}
                    </td>
                    <td>
                        <div class="btn-group">
                            @if($data->schedule >= Carbon::now()->subMinutes(30))
                                <a target="_blank" href="{{$data->start_link}}"
                                        class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Start Meeting"><i
                                            class="fa fa-play"></i></a>
                                <a href="{{ route('admin.live-videos.edit',$data->id) }}"
                                        class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Edit"><i
                                         class="fa fa-edit"></i></a>
                            @endif
                            
                            <button type="button" class="btn btn-sm btn-secondary" data-toggle="tooltip"
                                title="Delete"
                                onclick="event.preventDefault(); if(confirm('Are you sure to delete this video?')){ document.getElementById('delete-live-video-{{ $data->id }}').submit();}">
                            <i class="fa fa-times"></i> 
                            </button>
                            <form id="delete-live-video-{{ $data->id }}"
                                    action="{{ route('admin.live-videos.destroy', $data->id) }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                            </form>
                            <button type="button" value="{{$data->meeting_id}}" id="{{$data->id}}" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Transfer to Recorded Videos" onclick="getRec(this)">
                                <i class="fa fa-external-link"></i>
                            </button>
                        </div>
                    </td>
                </tr>
                @endforeach
                @else
                <tr class="no-data">
                    <td colspan="7" class="text-center">No data found</td>
                </tr>
                @endif
            </tbody>
            <tfoot>
                <tr>
                <th colspan="7">
                    <div style="float:right;">{{ $videos->appends(['inactives' => $inactives->currentPage()])->links() }}</div>
                </th>
                </tr>
            </tfoot>
        </table>
        <hr>
        <h4 align="center">Finished Meetings</h4>
        <table id="live-videos-inactive" class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                    <th style="text-transform: none !important;">Sl.No</th>
                    <th style="text-transform: none !important;">Meeting Title</th>
                    <th style="text-transform: none !important;">Meeting Id</th>
                    <th style="text-transform: none !important;">Packages</th>
                    <th style="text-transform: none !important;">Scheduled Date / Time</th>
                    <th style="text-transform: none !important;">Category</th>
                    <th style="text-transform: none !important;">Actions</th>

                </tr>
            </thead>
            <tbody>
                @if($inactives->count())
                @foreach ($inactives as $data)
                <tr>
                    <td class="text-center"> {{ $loop->iteration }} </td>
                    <td class="font-w600"> {{ @$data->title }}</td>
                    <td class="font-w600"> {{ @$data->meeting_id }}</td>
                    <td class="d-none d-sm-table-cell">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            @foreach ($data->packages as $package)
                                <span class="badge badge-success">{{ $package->name }}</span>
                            @endforeach
                        </div>
                    </td>
                    <td>
                        {{ Carbon::parse(@$data->schedule)->format('d M Y h:i:s A') }}
                    </td>
                    <td>
                        {{ @$data->category->category}}
                    </td>
                    <td>
                        <div class="btn-group">
                            @if($data->schedule >= Carbon::now())
                                <a target="_blank" href="{{$data->start_link}}"
                                        class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Start Meeting"><i
                                            class="fa fa-play"></i></a>
                                <a href="{{ route('admin.live-videos.edit',$data->id) }}"
                                        class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Edit"><i
                                         class="fa fa-edit"></i></a>
                            @endif
                            
                            <button type="button" class="btn btn-sm btn-secondary" data-toggle="tooltip"
                                title="Delete"
                                onclick="event.preventDefault(); if(confirm('Are you sure to delete this video?')){ document.getElementById('delete-live-video-{{ $data->id }}').submit();}">
                            <i class="fa fa-times"></i> 
                            </button>
                            <form id="delete-live-video-{{ $data->id }}"
                                    action="{{ route('admin.live-videos.destroy', $data->id) }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                            </form>
                            <button type="button" value="{{$data->meeting_id}}" id="{{$data->id}}" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Transfer to Recorded Videos" onclick="getRec(this)">
                                <i class="fa fa-external-link"></i>
                            </button>
                        </div>
                    </td>
                </tr>
                @endforeach
                @else
                <tr class="no-data">
                    <td colspan="7" class="text-center">No data found</td>
                </tr>
                @endif
            </tbody>
            <tfoot>
                <tr>
                <th colspan="7">
                    <div style="float:right;">{{ $inactives->appends(['videos' => $videos->currentPage()])->links() }}</div>
                </th>
                </tr>
            </tfoot>
        </table>
        </div>
    </div>
    <!-- END Dynamic Table Full -->
</div>               
<!-- END Page Content -->

@endsection
@section('js_after')
<script>
    $(function() {
        $('#live-videos').DataTable({
            "dom": '<"pull-left"f><"pull-right"l>tip',
            "paging": false,
            language : {
                sLengthMenu: "Show _MENU_"
            },
            "bInfo" : false
        });
        
    });
    $(function() {
        $('#live-videos-inactive').DataTable({
            "dom": '<"pull-left"f><"pull-right"l>tip',
            "paging": false,
            language : {
                sLengthMenu: "Show _MENU_"
            },
            "bInfo" : false
        });
        
    });
</script>
<script>
    function getRec(button)
    {
        $.ajax({
          url: "{{ route('admin.recordings') }}",
          type:"get",
          data:{
                id:button.value,
                video:button.id,
            },
          success:function(response){
            console.log(response);
            
            if(response) {
                alert(response.success);
            }
          },
        });
    }
</script>
@endsection