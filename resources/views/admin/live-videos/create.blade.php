@extends('admin.layout.app')
@section('css_after')
    <link rel="stylesheet" href="{{ asset('assets/admin/js/plugins/flatpickr/flatpickr.min.css')}}">
@endsection
@section('content')
<!-- Page Content -->
<div class="content">
    <nav class="breadcrumb bg-white push">
        <a class="breadcrumb-item" href="{{ route('admin.home') }}">Dashboard</a>
        <a class="breadcrumb-item" href="{{ route('admin.live-videos.index') }}">Videos</a>
        <span class="breadcrumb-item active">Add</span>
    </nav>

    <div class="block">
        <div class="block-header block-header-default">
            <h3 class="block-title">Add Video</h3>
            <div align="right">
                <button id="scheduleZoomButton" type="button" class="btn btn-alt-primary" data-toggle="modal" data-target="#zoomModal">Schedule with Zoom</button>
            </div>
        </div>
        <div class="block-content">
            @if (session('error'))
            <div class="alert alert-danger alert-dismissable" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <p class="mb-0">{{ session('error') }}</p>
            </div>
            @endif
            @if (session('success'))
            <div class="alert alert-success alert-dismissable" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <p class="mb-0">{{ session('success') }}</p>
            </div>
            @endif
            <form class="js-validation-packages" id="liveVideo" action="{{ route('admin.live-videos.store') }}" method="post">
                @csrf
                <h2 class="content-heading pt-1">Schedule Live</h2>
                <div class="row">
                    <div class="col-md-8 col-md-8 col-sm-12 col-xs-12">
                        <div class="form-group row">
                            <div class="col-md-12 @error('video') is-invalid @enderror">
                                <div class="form-material">
                                    <input type="url" class="form-control" id="video" name="video"
                                        placeholder="Please Enter Live Link" value="{{ @old('video') }}">
                                    <label for="video">Live Link *</label>
                                </div>
                                @error('video')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12 @error('start') is-invalid @enderror">
                                <div class="form-material">
                                    <input type="url" class="form-control" id="start" name="start"
                                        placeholder="Please Enter start Link" value="{{ @old('start') }}">
                                    <label for="start">Meeting Start Link *</label>
                                </div>
                                @error('start')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12 @error('title') is-invalid @enderror">
                                <div class="form-material">
                                    <input type="text" class="form-control" id="title" name="title"
                                        placeholder="Please provide  meeting title" value="{{ @old('title') }}">
                                    <label for="title">Title *</label>
                                </div>
                                @error('title')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12 @error('meeting_id') is-invalid @enderror">
                                <div class="form-material">
                                    <input type="text" class="form-control" id="meeting_id" name="meeting_id"
                                        placeholder="Please provide meeting id" value="{{ @old('meeting_id') }}"
                                        >
                                    <label for="meeting_id">Meeting Id *</label>
                                </div>
                                @error('meeting_id')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <input type="hidden" cid="passcode" name="passcode"value="{{ @old('passcode') }}" >
                        <div class="form-group row">
                            <div class="col-md-12 @error('passcode') is-invalid @enderror">
                                <div class="form-material">
                                    <input type="password" class="form-control" id="passcode" name="passcode"
                                        placeholder="Please provide passcode" value="{{ @old('passcode') }}"
                                        maxlength="10" required>
                                        <label for="password">Passcode *
                                            <a  data-toggle="tooltip" title="maximum length of passcode must be 10,
                                                                    passcode can contains only [a to z, A to Z, 0 to 9, @ -_*! ]">
                                                                      <i class="fa fa-info-circle" aria-hidden="true"></i></a>
                                        </label>
                                </div>
                                @error('passcode')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-12 @error('description') is-invalid @enderror">
                                <div class="form-material">
                                    <textarea class="form-control" id="description" name="description"
                                     cols="30" rows="8" placeholder="Please provide video description" >{{ @old('description') }}</textarea>
                                    <label for="description">Description *</label>
                                </div>
                                @error('description')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 col-md-4 col-sm-12 col-xs-12">
                        <div class="form-group row">
                            <div class="col-md-12 @error('package[]') is-invalid @enderror">
                                <div class="form-material">
                                    <table>
                                        @foreach ($packages as $package)
                                            <tr>
                                                <td><label>{{$package->name}}</label></td><td><input type="checkbox" name="package[]" id="package[]" value="{{$package->id}}"></td>
                                            </tr>
                                        @endforeach
                                    </table>
                                    <label for="package[]">Select Packages *</label>
                                </div>
                                @error('package')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-12 @error('date') is-invalid @enderror">
                                <div class="form-material">
                                    <input value="{{ @old('date') }}" autocomplete="off" read-only="true" type="text" class="js-flatpickr form-control" id="date" 
                                    name="date" data-allow-input="true" data-enable-time="true" placeholder="Select Date & Time" >
                                    <label for="date">Schedule date & time *</label>
                                </div>
                                @error('date')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12 @error('category') is-invalid @enderror">
                                <div class="form-material">
                                    <select name="category" id="category" class="form-control" required>
                                        <option value="">Select Category</option>
                                        @foreach ($categories as $category)
                                            <option value="{{$category->id}}">{{$category->category}}</option>
                                        @endforeach
                                    </select>
                                    <label for="category">Category *</label>
                                </div>
                                @error('date')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                    
                        {{-- <h2 class="content-heading">Video Status</h2>
                        <div class="form-group row">
                            <div class="col-md-6 @error('status') is-invalid @enderror p-0">
                                <label class="col-12">Status *</label>
                                <div class="col-12">
                                    <div class="custom-control custom-radio custom-control-inline mb-5">
                                        <input class="custom-control-input" type="radio" name="status" id="status1" value="1" checked>
                                        <label class="custom-control-label" for="status1">Active</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline mb-5">
                                        <input class="custom-control-input" type="radio" name="status" id="status2" value="0"
                                            >
                                        <label class="custom-control-label" for="status2">Inactive</label>
                                    </div>
                                    @error('status')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div> --}}
                        <div class="form-group row">
                            <div class="col-md-9">
                                <button type="submit" class="btn btn-alt-primary">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>               
<!-- END Page Content -->
@endsection
@section('js_after')
<!-- Page JS Plugins -->
<script src="<?php echo url('/'); ?>/assets/admin/js/plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="{{ asset('assets/admin/js/plugins/flatpickr/flatpickr.min.js') }}"></script>
<!-- Page JS Helpers (Flatpickr) -->
<script>jQuery(function(){ Codebase.helpers(['flatpickr']); });</script>
<script>
$(function () {
    $("#start_time").flatpickr({ 
        minDate: "today" 
    });
    $("#date").flatpickr({ 
        minDate: "today" 
    });
});   
    $('.js-validation-packages').validate({
        errorClass: 'invalid-feedback animated fadeInDown',
                errorElement: 'div',
                errorPlacement: (error, e) => {
                    $(e).parents('.form-group > div').append(error);
                },
                highlight: e => {
                    $(e).closest('.form-group > div').removeClass('is-invalid').addClass('is-invalid');
                },
                success: e => {
                    $(e).closest('.form-group > div').removeClass('is-invalid');
                    $(e).remove();
                },
                rules: {
                    video: {
                        required: true,
                    },
                    start: {
                        required: true,
                    },
                    title: {
                        required: true,
                    },
                    meeting_id: {
                        required: true,
                    },
                    description: {
                        required: true,
                    },
                    "package[]": {
                        required: true,
                        minlength: 1
                    }, 
                    date: {
                        required: true,
                    },
                },
                messages: {
                    video: {
                        required: 'Please enter live link',
                    },
                    start: {
                        required: 'Please enter start link',
                    },
                    title: {
                        required: 'Please enter title',
                    },
                    meeting_id: {
                        required: 'Please enter meeting id',
                    },
                    description: {
                        required: 'Please enter description',
                    },
                    package: {
                        required: 'Please choose package',
                        minlength: 'Please select atleast one package'
                    },
                    date: {
                        required: 'Please choose date and time',
                    },
                }    
    });
</script>    
<script>
    function scheduleZoom()
    {
        event.preventDefault();
        let topic = $("#topic").val();
        let start_time = $("#start_time").val();
        let agenda = $("#agenda").val();
        let duration = $("#duration").val();
        let password= $("#password").val();
        if(topic!="" && start_time!="" && agenda!="" && duration!=""){
            $.ajax({
                url: "/api/create",
                type:"post",
                data:{
                    topic:topic,
                    start_time:start_time,
                    agenda:agenda,
                    duration:duration,
                    password:password,
                },
                success:function(response){
                    console.log(response);
                    if(response){
                        var obj =  response.data;
                        // if(response.success==true)
                        // {
                            alert("Meeting Scheduled successfully!");
                            $("#zoomModal").modal('hide');
                            $("#scheduleZoomButton").hide();
                            $("#video").val(obj.join_url);
                            $("#start").val(obj.start_url);
                            $("#title").val(obj.topic);
                            $("#meeting_id").val(obj.id);
                            $("#description").val(obj.agenda);
                            // var d=obj.start_time;
                            // var z=d.substring(0, 19);
                            $("#date").val($("#start_time").val());
                            $("#passcode").val(obj.password);
                        // }else{
                        //     alert(Some error occured!);
                        // }
                    }
                },
            });
        }else{
            alert("Kindly fill all the fields!");
        }
    }
</script>

 <!-- The Modal -->
 <div class="modal" id="zoomModal">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Schedule with Zoom</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          <form id="zoom" onsubmit="scheduleZoom()">
            <div class="form-group row">
                <div class="col-md-12 @error('topic') is-invalid @enderror">
                    <div class="form-material">
                        <input type="text" class="form-control" name="topic" id="topic" placeholder="Title / Topic" value="{{ @old('passcode') }}"
                            required>
                        <label for="passcode">Title / Topic</label>
                    </div>
                    @error('topic')
                        <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
                <div class="col-md-12 @error('start_time') is-invalid @enderror">
                    <div class="form-material">
                        <input read-only="true" autocomplete="off" type="text" class="js-flatpickr form-control" id="start_time" name="start_time" data-allow-input="true" data-enable-time="true" placeholder="Select Date & Time" required>
                        <label for="date">Schedule date & time</label>
                    </div>
                    @error('start_time')
                        <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
                <div class="col-md-12 @error('agenda') is-invalid @enderror">
                    <div class="form-material">
                        <input type="text" class="form-control" name="agenda" id="agenda" placeholder="Agenda / Description" value="{{ @old('passcode') }}"
                            autocomplete="off" required>
                        <label for="agenda">Agenda / Description</label>
                    </div>
                    @error('topic')
                        <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
                <div class="col-md-12 @error('password') is-invalid @enderror">
                    <div class="form-material">
                        <input type="password" class="form-control" name="password" id="password" placeholder="Passcode" value="{{ @old('password') }}" maxlength="10" 
                        autocomplete="off" required>
                        <label for="password">Passcode *
                            <a  data-toggle="tooltip" title="maximum length of passcode must be 10,
                                                    passcode can contains only [a to z, A to Z, 0 to 9, @ -_*! ]">
                                                      <i class="fa fa-info-circle" aria-hidden="true"></i></a>
                        </label>
                    </div>
                    @error('password')
                        <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
                <div class="col-md-12 @error('duration') is-invalid @enderror">
                    <div class="form-material">
                        <input type="number" class="form-control" name="duration" id="duration" placeholder="Duration of meeting" value="{{ @old('duration') }}"
                            autocomplete="off" required>
                        <label for="duration">Duration(in Minutes)</label>
                    </div>
                    @error('duration')
                        <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div><p> </p>
                
            </div>
            <div class="form-group row">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-alt-primary">Scehdule</button>
                </div>
            </div>
          </form>
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-alt-primary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
</div>  
@endsection