@extends('admin.layout.app')

@section('content')
<!-- Page Content -->
<div class="content">
    <nav class="breadcrumb bg-white push">
        <a class="breadcrumb-item" href="{{ route('admin.home') }}">Dashboard</a>
        <a class="breadcrumb-item" href="{{ route('admin.packages.index') }}">Packages</a>
        <span class="breadcrumb-item active">Add</span>
    </nav>

    <div class="block">
        <div class="block-header block-header-default">
            <h3 class="block-title">Add Package</h3>
        </div>
        <div class="block-content">
            @if (session('error'))
            <div class="alert alert-danger alert-dismissable" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <p class="mb-0">{{ session('error') }}</p>
            </div>
            @endif
            @if (session('success'))
            <div class="alert alert-success alert-dismissable" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <p class="mb-0">{{ session('success') }}</p>
            </div>
            @endif
            <form class="js-validation-packages" action="{{ route('admin.packages.store') }}" method="post">
                @csrf
                <h2 class="content-heading pt-1">Package Details</h2>
                <div class="form-group row">
                    <div class="col-md-12 @error('name') is-invalid @enderror">
                        <div class="form-material">
                            <input type="text" class="form-control" id="name" name="name"
                                placeholder="Please provide name of the package" value="{{ @old('name') }}">
                            <label for="name">Package Name *</label>
                        </div>
                        @error('name')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-12 @error('price') is-invalid @enderror">
                        <div class="form-material">
                            <input type="number" class="form-control" id="price" name="price"
                                placeholder="Please provide price of the package" value="{{ @old('price') }}">
                            <label for="price">Price * </label>
                        </div>
                        @error('price')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-12 @error('duration') is-invalid @enderror">
                        <div class="form-material">
                            <input type="number" class="form-control" id="duration" name="duration"
                                placeholder="Please provide duration of the package" min=1 value="{{ @old('duration') }}">
                            <label for="duration">Package Duration (in months) *</label>
                        </div>
                        @error('duration')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
               
                <h2 class="content-heading">Package Status </h2>
                <div class="form-group row">
                    <div class="col-md-6 @error('status') is-invalid @enderror p-0">
                        <label class="col-12">Status *</label>
                        <div class="col-12">
                            <div class="custom-control custom-radio custom-control-inline mb-5">
                                <input class="custom-control-input" type="radio" name="status" id="status1" value="1" checked>
                                <label class="custom-control-label" for="status1">Active</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline mb-5">
                                <input class="custom-control-input" type="radio" name="status" id="status2" value="0">
                                <label class="custom-control-label" for="status2">Inactive</label>
                            </div>
                            @error('status')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-9">
                        <button type="submit" class="btn btn-alt-primary">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>               
<!-- END Page Content -->

@endsection
@section('js_after')
<!-- Page JS Plugins -->
<script src="<?php echo url('/'); ?>/assets/admin/js/plugins/jquery-validation/jquery.validate.min.js"></script>
<script>
    $('.js-validation-packages').validate({
        errorClass: 'invalid-feedback animated fadeInDown',
                errorElement: 'div',
                errorPlacement: (error, e) => {
                    $(e).parents('.form-group > div').append(error);
                },
                highlight: e => {
                    $(e).closest('.form-group > div').removeClass('is-invalid').addClass('is-invalid');
                },
                success: e => {
                    $(e).closest('.form-group > div').removeClass('is-invalid');
                    $(e).remove();
                },
                rules: {
                    name: {
                        required: true,
                    },
                    price: {
                        required: true,
                    },
                    duration: {
                        required: true,
                    },
                },
                messages: {
                    name: {
                        required: 'Please enter package name',
                    },
                    price: {
                        required: 'Please enter package price',
                    },
                    duration: {
                        required: 'Please enter package duration',
                    },
                }    
    });
</script>    
@endsection