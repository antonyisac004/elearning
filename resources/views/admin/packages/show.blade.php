@extends('admin.layout.app')

@section('content')
<!-- Page Content -->
<div class="content">
    <nav class="breadcrumb bg-white push">
    <a class="breadcrumb-item" href="{{ route('admin.home') }}">Dashboard</a>
        <a class="breadcrumb-item" href="{{ route('admin.packages.index') }}">Packages</a>
        <span class="breadcrumb-item active">View</span>
    </nav>

    <div class="row py-30">
        <div class="col-md-3 col-xl-3"></div>
        <div class="col-md-6 col-xl-6">
            <!-- Developer Plan -->
            <a class="block block-link-pop block-rounded block-bordered text-center" href="javascript:void(0)">
                <div class="block-header">
                    <h3 class="block-title font-w600">
                         {{ @$data->name }}
                    </h3>
                </div>
                <div class="block-content bg-body-light">
                <div class="h1 font-w700 text-primary mb-10">${{ @$data->price }}</div>
                <div class="h5 text-muted">per month</div>
                </div>
                <div class="block-content">
                    @foreach ($data->packageFeatures as $feature)
                        <p><strong>
                            @if(@$feature->enabled==1)
                            <i class="fa fa-check-circle fa-lg"
                                aria-hidden="true" style="color:green;"></i>
                            @else
                            <i class="fa fa-times-circle fa-lg" 
                            aria-hidden="true" style="color:red;"></i>
                            @endif
                            {{$feature->feature}}</strong></p>
                    @endforeach
                </div>
                <div class="block-content block-content-full">
                    <span class="btn btn-hero btn-sm btn-rounded btn-noborder btn-primary">
                        {{ @$data->name }}
                    </span>
                </div>
            </a>
            <!-- END Developer Plan -->
        </div>
        <div class="col-md-3 col-xl-3"></div>
    </div>
</div>               
<!-- END Page Content -->

@endsection
@section('js_after')
@endsection