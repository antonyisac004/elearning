<?php

return [
    //Auth
    'welcome' => 'Welcome to Your Dashboard',
    'wish'    => 'Have a nice day!',
    'sign in' => 'Please sign in',
    'email label' => 'Email',
    'password label' => 'Password',
    'remember label' => 'Remember Me',
    'forgot password text' => 'Forgot Your Password?',
];