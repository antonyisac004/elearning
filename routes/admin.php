<?php 

Route::get('/', 'Admin\HomeController@index');
Route::get('/home', 'Admin\HomeController@index')->name('home');

//Profile
Route::get('/profile', 'Admin\ProfileController@show')->name('profile');
Route::post('/profile', 'Admin\ProfileController@store')->name('profile');

//Settings
Route::get('/settings', 'Admin\SettingsController@index')->name('settings.index');
Route::post('/settings', 'Admin\SettingsController@store')->name('settings.store');

//Packages
Route::resource('packages', 'Admin\PackageController');
Route::resource('package-features', 'Admin\PackageFeatureController');

// Live Videos
Route::resource('live-videos', 'Admin\LiveVideoController');
Route::get('/fetchLiveVideo', 'Admin\LiveVideoController@fetchLiveVideo');
Route::get('/deleteLiveVideo', 'Admin\LiveVideoController@deleteLiveVideo');

// Recorded Videos
Route::resource('recorded-videos', 'Admin\RecordedVideoController');
Route::get('/fetchVideo', 'Admin\RecordedVideoController@fetchVideo');
Route::get('/deleteVideo', 'Admin\RecordedVideoController@deleteVideo');

// Users
Route::resource('users', 'Admin\UserController');

// Category
Route::resource('category', 'Admin\CategoryController');

//Reports
Route::get('/reports', 'Admin\ReportController@index')->name('reports.index');
Route::post('/reports', 'Admin\ReportController@store')->name('reports.store');

//Admin Activities
Route::resource('admin-activity', 'Admin\AdminActivityController');
Route::get('/adminMassDelete', 'Admin\AdminActivityController@massDelete')->name('admin-activity.massDelete');

//User Activities
Route::resource('user-activity', 'Admin\UserActivityController');
Route::get('/userMassDelete', 'Admin\UserActivityController@massDelete')->name('user-activity.massDelete');