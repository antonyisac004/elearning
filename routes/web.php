<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/********  Front End Routes *******/

//Home Routes
Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('homepage');
Route::get('/referral/{code}', 'User\ReferralController@refferal')->name('user.referral');
//Membership 
Route::get('/membership', 'HomeController@membership')->name('membership.index');
Route::get('/getRec', 'Zoom\MeetingController@getRec')->name('admin.recordings');

//Autentication
Auth::routes(['register' => false,]);
// Registration Routes
Route::get('/buy/{slug}', 'RegisterController@index')->name('register.index');
Route::post('/signup', 'RegisterController@store')->name('register.store');
// route for to show payment form using get method
Route::get('/confirm-payment/{slug}/{id}', 'RegisterController@pay')->name('register.pay');
// route for make payment request using post method
Route::post('dopayment', 'RegisterController@dopayment')->name('dopayment');


/*************  Admin Routes ********/

Route::group(['prefix' => 'admin'], function () {
    Route::get('/login', 'AdminAuth\LoginController@showLoginForm')->name('admin.login');
    Route::post('/login', 'AdminAuth\LoginController@login');
    Route::post('/logout', 'AdminAuth\LoginController@logout')->name('admin.logout');

    Route::post('/password/email', 'AdminAuth\ForgotPasswordController@sendResetLinkEmail')->name('admin.password.request');
    Route::post('/password/reset', 'AdminAuth\ResetPasswordController@reset')->name('admin.password.email');
    Route::get('/password/reset', 'AdminAuth\ForgotPasswordController@showLinkRequestForm')->name('admin.password.reset');
    Route::get('/password/reset/{token}', 'AdminAuth\ResetPasswordController@showResetForm');

});

/*************  Admin Routes Ends ********/

/*************  User Routes ********/

Route::group(['prefix' => 'user'], function () {

    Route::get('/login', 'Auth\LoginController@showLoginForm')->name('user.login');
    Route::post('/login', 'Auth\LoginController@login');

    Route::middleware(['auth'])->group(function () {

        Route::get('/', 'User\HomeController@index')->name('home');
        Route::get('/home', 'User\HomeController@index');
        Route::get('/subscription', 'User\SubscriptionController@index')->name('user.subscription');
        Route::get('/recorded-course', 'User\RecordedCourseController@index')->name('user.recorded-course');
        Route::get('/recorded-course/show', 'User\RecordedCourseController@show')->name('user.recorded-course.show');
        Route::get('/profile', 'User\ProfileController@index')->name('user.profile');
        Route::get('/profile/edit', 'User\ProfileController@edit')->name('user.profile.edit');
        Route::post('/profile/update', 'User\ProfileController@update')->name('user.profile.update');
        Route::get('/referral-link', 'User\ReferLinkController@index')->name('user.referral-link');
        Route::get('/referralMail', 'User\ReferLinkController@referralMail')->name('user.referral-mail');
        Route::get('/settings', 'User\SettingsController@index')->name('user.settings');
        Route::post('/profile/passwordReset', 'User\SettingsController@update')->name('user.password.reset');
        Route::post('/savePayment', 'User\RenewalController@savePayment')->name('user.savePayment');
        Route::get('/getData', 'User\RenewalController@getData')->name('user.package.data');
      
    });



});
