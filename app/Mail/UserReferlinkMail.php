<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Support\Facades\Auth;
use App\ReferLink;
use Illuminate\Queue\SerializesModels;

class UserReferlinkMail extends Mailable
{
    use Queueable, SerializesModels;

    public $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
        $user=Auth::User();
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $user=Auth::User();
        $link=$user->referrLink->link;
        return $this->subject('Use this referral link for signup to get the discount')
            ->view('emails.user_referral_mail',['link'=>$link]);
    }
}