<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RecordedVideo extends Model
{
    //
    protected $table="recorded_videos";
    use Uuids;
    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    protected $fillable = [
        'id','video_link','title','description','schedule','status','category_id'
    ];

    public function packages()
    {
        return $this->belongsToMany('App\Package');
    }
    public function category()
    {
        return $this->hasOne('App\Category','id','category_id');
    }
    public function package_recorded_video()
    {
        return $this->hasMany('App\PackageRecordedVideo');
    }
    
}
