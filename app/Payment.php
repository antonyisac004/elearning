<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'payment_id', 'user_id', 'created_at',
    ];

    public function user()
    {
        return $this->hasOne('App\User','id','user_id');
    }
}
