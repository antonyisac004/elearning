<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PackageRecordedVideo extends model
{
    public $timestamps = false;
    protected $table="package_recorded_video";
    public function getPackage()
    {
        return $this->hasMany('App\Package','id','package_id');
    }
}
