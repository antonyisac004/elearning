<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LiveVideo extends Model
{
     protected $table="live_videos";
     use Uuids;
     /**
      * Indicates if the IDs are auto-incrementing.
      *
      * @var bool
      */
     public $incrementing = false;

      /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'id','video_link','title', 'meeting_id', 'passcode','description','schedule','status'
  ];

  
  public function packages()
  {
        return $this->belongsToMany('App\Package');
  }

  public function category()
  {
    return $this->hasOne('App\Category','id','category_id');
  }


  public function live_video_package()
  {
      return $this->hasMany('App\LiveVideoPackage');
  }

   
}
