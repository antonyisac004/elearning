<?php

namespace App\Http\Controllers;
use App\Package;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {   
        return view('home',['packages' => Package::where("status",1)->orderBy("created_at", "asc")->get()]);
    }

    public function membership()
    {
        return view('membership',['packages' => Package::where("status",1)->orderBy("created_at", "asc")->get()]);
    }
}
