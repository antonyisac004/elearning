<?php

namespace App\Http\Controllers;

use Mail;
use Illuminate\Support\Facades\Hash;
use App\Package;
use App\User;
use Carbon\Carbon;
use Validator;
use App\Payment;
use App\ReferLink;
use App\Referral;
use Redirect,Response;
use \Crypt;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Mail\UserAccountActivation;

class RegisterController extends Controller
{

    public function index($slug)
    {   

        $package_data=Package::where('slug',$slug)->where('status',1)->firstorfail();
        $packages=Package::where('status',1)->orderby('price','asc')->get();
        if(session('ref')==""){
            $ref="";
        }else{
            $ref=session('ref');
        }
        return view('auth.register', ['packages' => $packages,'package_data'=>$package_data,'ref'=>$ref]);
    }

    public function store(Request $request)
    {
        $validated = $request->validate([
            'first_name' => 'required|alpha|max:255',
            'last_name' => 'required|alpha|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'phone' => 'required|numeric|digits:10',
            'password' => 'required|required_with:password_confirmation|string|confirmed|min:8|
                            regex:/[a-z]/|regex:/[A-Z]/|regex:/[0-9]/|regex:/[@$!%*#?&]/',
            'package_id' => 'required',
        ]);
        $user = new User;
        $user->id = (string) Str::uuid();
        $user->first_name = $validated['first_name'];
        $user->last_name = $validated['last_name'];
        $user->email = $validated['email'];
        $user->phone = $validated['phone'];
        $user->password = bcrypt($validated['password']);
        $user->package_id = $validated['package_id'];
        $user->discount = 0;
        // $pack=Package::where('id',$validated['package_id'])->firstorfail();
        $date=Carbon::now()->toDateString();
        $user->renewal = $date;
        $res = $user->save();
        $last_id=$user->id;
        if ($res){
            $link=new ReferLink();
            $link->user_id=$last_id;
            $randomString = Str::random(10);
            $link->link="http://elearning.test/referral/$randomString";
            $link->code=$randomString;
            $link->save();
        }
        if ($res) {
                $package_data=Package::where('id',$user->package_id)->where('status',1)->firstorfail();
                return redirect(route('register.pay',['slug'=>$package_data->slug,'id'=>Crypt::encrypt($last_id)]));
        }else{
                return redirect()->back()->with('error', 'Failed to complete new registration. Please try again.');
        }
                
        
    }

    public function pay($slug,$id) {
        $user_id=Crypt::decrypt($id);
        $user=User::where('id',$user_id)->firstorfail();
        $package_data=Package::where('slug',$slug)->where('status',1)->firstorfail();
        if (session('ref')==""){
            $price=$package_data->price;
        }else{
            $price=$package_data->price-(($package_data->price*10)/100);
        }
        return view('pay',['package_data'=>$package_data,'user'=>$user,'package_price'=>$price]);
    }

    public function dopayment(Request $request) {
        $user=User::where('id',$request->user_id)->firstorfail();
        $user->status = 1;
        $user->save();
        $pack=Package::where('id',$user->package_id)->firstorfail();
        $date=Carbon::now()->addMonths($pack->duration)->toDateString();
        $user->renewal = $date;
        $user->discount = 0;
        $user->update();
        if(session('ref')!=""){
            $ref_user=User::where('id',session('ref_user'))->firstorfail();
            $dsc=$ref_user->discount;
            $ref_user->discount=$dsc+10;
            $ref_user->update();
        }
        if(session('ref')!=""){
            $ref=new Referral();
            $ref_res=ReferLink::where('code',session('ref'))->firstorfail();
            $ref->referring_user=$ref_res->user_id;
            $ref->referring_disc=10;
            $ref->referred_user=$user->id;
            $ref->referred_disc=10;
            $ref->save();
            session(['ref'=>""]);
        }

       
        
        //Input items of form
        $input = $request->all();

        $payment = new Payment;
        $payment->payment_id=$request->razorpay_payment_id;
        $payment->user_id=$request->user_id;
        $payment->amount=$request->amount;
        $res = $payment->save();

        Mail::to($user->email)->send(new UserAccountActivation);

        // Please check browser console.
        print_r($input);
        exit;
    }

  

   
}
