<?php

namespace App\Http\Controllers\Admin;
use App\ActivityLog;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AdminActivityController extends Controller
{
    public function index()
    {
        return view('admin.admin-activity.index',['datas' => ActivityLog::latest("updated_at")->paginate(10)]);
    }
    public function destroy($id)
    {
        $res = ActivityLog::where('id',$id)->delete();
        if ($res) {
            return redirect()->back()->with('success', 'Successfully deleted the activity.');
        } else {
            return redirect()->back()->with('error', 'Failed to delete the activity. Please try again.');
        }
    }
    public function massDelete()
    {
        $res = ActivityLog::whereNotNull('id')->delete();
        if($res){
            return response()->json(['success'=>'Category Updated Successfully']);
        }
    }
}
