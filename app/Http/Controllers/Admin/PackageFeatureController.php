<?php

namespace App\Http\Controllers\Admin;

use App\PackageFeature;
use App\Package;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PackageFeatureController extends Controller
{
    public function index()
    {
        return view('admin.package-features.index',['package_features' => PackageFeature::orderby("created_at","asc")->paginate(10)]);
    }

    public function create()
    {
        $packages=Package::where('status', 1)->get();
        return view('admin.package-features.create',['packages'=>$packages]);
    }

    public function store(Request $request)
    {
        $user = auth()->guard('admin')->user();
        $validated = $request->validate([
            'feature' => 'required|string|min:4',
            'package' => 'required',
            'enabled' => 'required',
            'status' => 'required',
        ]);
        $feature = new PackageFeature();
        $feature->feature = $validated['feature'];
        $feature->package_id = $validated['package'];
        $feature->enabled = $validated['enabled'];
        $feature->status = $validated['status'];
        $res = $feature->save();
        if ($res) {
            return redirect()->back()->with('success', 'Successfully added new feature.');
        } else {
            return redirect()->back()->with('error', 'Failed to add new feature. Please try again.');
        }
    }

    public function edit(Request $request, $uuid)
    {
        $features = PackageFeature::where('id', $uuid)->firstorFail();
        $packages=Package::where('status', 1)->get();
        return view('admin.package-features.edit', ['data' => $features,'packages'=>$packages]);
    }

    public function update(Request $request, PackageFeature $packageFeature)
    {

        $validated = $request->validate([
            'feature' => 'required|string|min:4',
            'package' => 'required',
            'enabled' => 'required',
            'status' => 'required',
        ]);
        $packageFeature = PackageFeature::where('id', $packageFeature->id)->firstorFail();
        $packageFeature->feature = $validated['feature'];
        $packageFeature->package_id = $validated['package'];
        $packageFeature->enabled = $validated['enabled'];
        $packageFeature->status = $validated['status'];
        $res = $packageFeature->save();
        if ($res) {
            return redirect()->back()->with('success', 'Successfully updated the feature.');
        } else {
            return redirect()->back()->with('error', 'Failed to update the feature. Please try again.');
        }
    }

    public function destroy(PackageFeature $packageFeature)
    {
        $res = $packageFeature->delete();
        if ($res) {
            return redirect()->back()->with('success', 'Successfully deleted the package.');
        } else {
            return redirect()->back()->with('error', 'Failed to delete the package. Please try again.');
        }
    }
}
