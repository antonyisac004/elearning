<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\RecordedVideo;
use App\LiveVideo;
use App\Package;
use App\Category;
use App\PackageRecordedVideo;
use App\Http\Traits\ZoomJWT;
use Carbon\Carbon;
use Validator;
use Storage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RecordedVideoController extends Controller
{
    use ZoomJWT;

    const MEETING_TYPE_INSTANT = 1;
    const MEETING_TYPE_SCHEDULE = 2;
    const MEETING_TYPE_RECURRING = 3;
    const MEETING_TYPE_FIXED_RECURRING_FIXED = 8;

    public function index()
    {
        $videos=RecordedVideo::orderby("schedule", 'desc')->paginate(10);
        $packages=Package::where('status',1)->get();
        return view('admin.recorded-videos.index',['videos'=>$videos,'packages'=>$packages]);
    }

    public function create()
    {
        $packages=Package::where('status',1)->get();
        $category=Category::all();
        return view('admin.recorded-videos.create', ['packages'=>$packages,'categories'=>$category]);
    }

    public function store(Request $request)
    {
        $user = auth()->guard('admin')->user();
        $validated = $request->validate([
            'meeting_id' => 'required',
            'package' => 'required',
        ]);
        //dd($validated['meeting_id']);exit;

        $path = 'https://api.zoom.us/v2/meetings/'.$validated['meeting_id'].'/recordings';
        $response = $this->zoomRecGet($path);
        $data = json_decode($response->body(), true);
        //dd($data['recording_files'][0]['download_url']);exit;
        
        if(($response->getStatusCode()!=404) && ($response->getStatusCode()!=400))
        {
            $recs=$data["recording_files"];
            $video=$recs[0];
            $recorded_video=RecordedVideo::where('meeting_id',$validated['meeting_id'])->first();
            if($recorded_video!=null)
            {
                return redirect()->back()->with('error', 'Data already exists in our Recordings list.');
            }else{
                $link=$data['recording_files'][0]['download_url'];
                $video=new RecordedVideo();
                $video->title=$data["topic"];
                $video->description=$data["topic"];
                $video->schedule=$this->toZoomTimeFormat($data['start_time']);
                $video->meeting_id=$validated['meeting_id'];
                $video->status=1;
                $video->video_link=$link;
                $res=$video->save();
                if($res){
                    foreach($validated['package'] as $package){
                        $pack_video= new PackageRecordedVideo();
                        $pack_video->package_id=$package;
                        $pack_video->recorded_video_id=$video->id;
                        $res=$pack_video->save();
                    }
                }
                return redirect()->route('admin.recorded-videos.index')->with('success', 'Succesfully Transferred.');
            }
        }else{
            return redirect()->back()->with('error', 'No Recording Exists');
        }

    }

    public function show(RecordedVideo $recordedVideo)
    {
        return view('admin.recorded-videos.show',['video' => $recordedVideo]);
    }

    public function edit(Request $request, $uuid)
    {
        $video = RecordedVideo::where('id', $uuid)->firstorFail();
        $packages=Package::where('status',1)->get();
        $category=Category::all();
        return view('admin.recorded-videos.edit', ['data' => $video,'packages'=>$packages,'categories'=>$category]);
    }

    public function update(Request $request, RecordedVideo $recordedVideo)
    {
        $user = auth()->guard('admin')->user();
        $validated = $request->validate([
            'video' => 'required|url',
            'title' => 'required',
            // 'status' => 'required',
            'description' => 'required',
            'category' => 'required',
            'package' => 'required',
            'date' => 'required|date',
            // 'thumb' => 'image|mimes:jpeg,png,jpg,gif,svg|dimensions:max_width=450,max_height=250|max:2048',
        ]);
        $recordedVideo->video_link=$validated['video'];
        $recordedVideo->title=$validated['title'];
        $recordedVideo->description=$validated['description'];
        $recordedVideo->schedule=$validated['date'];
        // $recordedVideo->status=$validated['status'];
        $recordedVideo->status=1;
        $recordedVideo->category_id=$validated['category'];
        $recordedVideo->video_link=$validated['video'];
        // if ($request->hasFile('thumb')) {
        //     Storage::disk('public')->delete($recordedVideo->thumb);
        //     $path = $request->file('thumb')->storeAs(
        //         'upload/thumbnails',
        //         time() . '_' . $request->thumb->getClientOriginalName(),
        //         'public'
        //     );
        //     $recordedVideo->thumb = $path;
        // }
        $res=$recordedVideo->save();

        if($res){
            PackageRecordedVideo::where('recorded_video_id',$recordedVideo->id)->delete();
            foreach($validated['package'] as $package){
                $pack_video= new PackageRecordedVideo();
                $pack_video->package_id=$package;
                $pack_video->recorded_video_id=$recordedVideo->id;
                $res=$pack_video->save();
            }
        }
        if ($res) {
            activity('admin')->performedOn($user)->causedBy($user)->withProperties(['data' => $request])->log('Updated Profile.');
            return redirect()->route('admin.recorded-videos.index')->with('success', 'Successfully saved your video.');
        } else {
            return redirect()->back()->with('error', 'Failed to save your video. Please try again.');
        }
    }

    public function destroy(RecordedVideo $recordedVideo,Request $request)
    {
        $res=RecordedVideo::where('id',$recordedVideo->id)->delete();
        if ($res) {
            return redirect()->back()->with('success', 'Successfully deleted the video.');
        } else {
            return redirect()->back()->with('error', 'Failed to delete the video. Please try again.');
        }
    }
    public function fetchVideo(Request $request)
    {
        $pack_video=RecordedVideo::select('recorded_videos.*')
            ->join('package_recorded_video', 'recorded_videos.id', '=', 'package_recorded_video.recorded_video_id')
            ->where('package_recorded_video.package_id', '=', $request->id)->get();
        return response()->json(['data'=>$pack_video]);
    }
}
