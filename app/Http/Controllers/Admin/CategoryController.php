<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    
    public function index()
    {
        return view('admin.category.index',['category' => Category::orderby("created_at","asc")->paginate(10)]);
    }

    public function create()
    {
        return view('admin.category.create');
    }

    public function store(Request $request)
    {
        $user = auth()->guard('admin')->user();
        $validated = $request->validate([
            'name' => 'required|unique:categories,category',
            'note' => 'required',
            'status' => 'required',
        ]);
        $category = new Category();
        $category->category = $validated['name'];
        $category->note = $validated['note'];
        $category->status = $validated['status'];
        $res = $category->save();
        if ($res) {
            return redirect()->back()->with('success', 'Successfully added new category.');
        } else {
            return redirect()->back()->with('error', 'Failed to add new category. Please try again.');
        }
    }

    public function edit(Category $category)
    {
        return view('admin.category.edit', ['data' => $category]);
    }

    public function update(Request $request, Category $category)
    {
        $user = auth()->guard('admin')->user();
        $validated = $request->validate([
            'name' => 'required',
            'note' => 'required',
            'status' => 'required',
        ]);
        $category->category = $validated['name'];
        $category->note = $validated['note'];
        $category->status = $validated['status'];
        $res = $category->save();
        if ($res) {
            return redirect()->back()->with('success', 'Successfully updated the category.');
        } else {
            return redirect()->back()->with('error', 'Failed to update the category. Please try again.');
        }
    }

    public function destroy(Category $category)
    {
        $res = $category->delete();
        if ($res) {
            return redirect()->back()->with('success', 'Successfully deleted the category.');
        } else {
            return redirect()->back()->with('error', 'Failed to delete the category. Please try again.');
        }
    }
}
