<?php

namespace App\Http\Controllers\Admin;

use Mail;
use App\Http\Controllers\Controller;
use App\LiveVideoPackage;
use App\Package;
use App\LiveVideo;
use App\Category;
use App\Http\Traits\ZoomJWT;
use Carbon\Carbon;
use Validator;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Mail\UserAccountActivation;


class LiveVideoController extends Controller
{
    use ZoomJWT;

    public function index()
    {
        $videos=LiveVideo::orderby("schedule", 'desc')->where('schedule','>=',Carbon::now()->subMinutes(30))->paginate(10, ['*'], 'videos');
        $inactives=LiveVideo::orderby("schedule", 'desc')->where('schedule','<',Carbon::now())->paginate(10, ['*'], 'inactives');
        $packages=Package::where('status',1)->get();
        return view('admin.live-videos.index',['videos'=>$videos,'packages'=>$packages,'inactives'=>$inactives]);
    }

    public function create()
    {
        $packages=Package::where('status',1)->get();
        $category=Category::all();
        return view('admin.live-videos.create', ['packages'=>$packages,'categories'=>$category]);
    }
    public function store(Request $request)
    {
       
        $user = auth()->guard('admin')->user();
        $validated = $request->validate([
            'video' => 'required|url',
            'start' => 'required|url',
            'title' => 'required',
            'meeting_id' => 'required',
            'passcode' => 'required',
            // 'status' => 'required',
            'category' => 'required',
            'description' => 'required',
            'package.*' => 'required',
            'date' => 'required|date',
            ],
           
        );
        //dd($validated);exit;
        $video=new LiveVideo();
        $video->video_link=$validated['video'];
        $video->start_link=$validated['start'];
        $video->title=$validated['title'];
        $video->meeting_id=$validated['meeting_id'];
        $video->passcode=$validated['passcode'];
        $video->description=$validated['description'];
        $video->schedule=$validated['date'];
        // $video->status=$validated['status'];
        $video->status=1;
        $video->video_link=$validated['video'];
        $video->category_id=$validated['category'];
        $res=$video->save();
        if($res){
            foreach($validated['package'] as $package){
                $pack_video= new LiveVideoPackage();
                $pack_video->live_video_id=$video->id;
                $pack_video->package_id=$package;
                $res=$pack_video->save();
            }
        }
        if ($res) {
            
            //activity('admin')->performedOn($user)->causedBy($user)->withProperties(['data' => $request])->log('Updated Profile.');
            return redirect()->route('admin.live-videos.index')->with('success', 'Meeting Scheduled successfully.');
        } else {
            return redirect()->back()->with('error', 'Failed to schedule meeting. Please try again.');
        }
    }

    public function edit(Request $request, $uuid)
    {
        $video = LiveVideo::where('id', $uuid)->firstorFail();
        $packages=Package::where('status',1)->get();
        $category=Category::all();
        return view('admin.live-videos.edit', ['data' => $video,'packages'=>$packages,'categories'=>$category]);
    }

    public function update(Request $request, LiveVideo $liveVideo)
    {   
        $user = auth()->guard('admin')->user();
        $validated = $request->validate([
            'video' => 'required|url',
            'title' => 'required',
            'meeting_id' => 'required',
            'passcode' => 'required',
            // 'status' => 'required',
            'category' => 'required',
            'description' => 'required',
            'package.*' => 'required',
            'date' => 'required|date',
        ]);
        $liveVideo->video_link=$validated['video'];
        $liveVideo->title=$validated['title'];
        $liveVideo->meeting_id=$validated['meeting_id'];
        $liveVideo->passcode=$validated['passcode'];
        $liveVideo->description=$validated['description'];
        $liveVideo->schedule=$validated['date'];
        // $liveVideo->status=$validated['status'];
        $liveVideo->status=1;
        $liveVideo->category_id=$validated['category'];
        $liveVideo->video_link=$validated['video'];
        $res=$liveVideo->save();

        if($res){
            LiveVideoPackage::where('live_video_id',$liveVideo->id)->delete();
            foreach($validated['package'] as $package){
                $pack_video= new LiveVideoPackage();
                $pack_video->live_video_id=$liveVideo->id;
                $pack_video->package_id=$package;
                $res=$pack_video->save();
            }
        }
        if ($res) {
            //activity('admin')->performedOn($user)->causedBy($user)->withProperties(['data' => $request])->log('Updated Profile.');
            return redirect()->route('admin.live-videos.index')->with('success', 'Successfully updated meeting.');
        } else {
            return redirect()->back()->with('error', 'Failed to update meeting. Please try again.');
        }
    }

    public function destroy(LiveVideo $liveVideo, Request $request)
    {
        $id=$liveVideo->meeting_id;
        $path = 'meetings/' . $id;
        $this->zoomDelete($path);
        $res = $liveVideo->delete();
        if ($res) {
            return redirect()->back()->with('success', 'Successfully deleted the meeting.');
        } else {
            return redirect()->back()->with('error', 'Failed to delete the meeting. Please try again.');
        }
    }

}
