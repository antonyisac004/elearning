<?php

namespace App\Http\Controllers\Admin;
use App\UserActivityLog;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UserActivityController extends Controller
{
    public function index()
    {
        $data=UserActivityLog::latest("updated_at")->paginate(10);
        return view('admin.user-activity.index',['datas' => UserActivityLog::latest("updated_at")->paginate(10)]);
    }
    public function destroy($id)
    {
        $res = UserActivityLog::where('id',$id)->delete();
        if ($res) {
            return redirect()->back()->with('success', 'Successfully deleted the activity.');
        } else {
            return redirect()->back()->with('error', 'Failed to delete the activity. Please try again.');
        }
    }
    public function massDelete()
    {
        $res = UserActivityLog::whereNotNull('id')->delete();
        if($res){
            return response()->json(['success'=>'Category Updated Successfully']);
        }
    }
}
