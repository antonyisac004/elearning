<?php

namespace App\Http\Controllers\Admin;


use App\User;
use App\Models\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Zoom;

class HomeController extends Controller
{
    public function index()
    {
        // $user = Zoom::user();
        // dd($user);
        //return response($user);
        return view('admin.home');
    }
}
