<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Payment;
use Carbon\Carbon;
use Validator;
use App\Exports\PaymentsExports;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    public function index()
    {
        $payments=Payment::paginate(10);
        return view('admin.reports.index',['payment'=>$payments,'message'=>"Payment Report",'from'=>"",'to'=>""]);
    }

    public function store(Request $request)
    {   
      
        $from=$request->from;
        $to=$request->to;
        $payments=Payment::whereBetween('created_at', [$from.' 00:00:00',$to.' 23:59:59'])->paginate(10);
        $from=Carbon::parse($request->from)->format('d-M-Y');
        $to=Carbon::parse($request->to)->format('d-M-Y');
        return view('admin.reports.index',['payment'=>$payments,'message'=>"Payment Report from $from to $to",'from'=>$request->from,'to'=>$request->to]);
        
    }

    
}
