<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Validator;
use App\User;
use App\Referral;
use Illuminate\Support\Facades\DB;
use Storage;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

class UserController extends Controller
{

    public function index()
    {   
        return view('admin.users.index',['users' => User::latest("updated_at")->paginate(10)]);
    }

    public function show($id)
    {
        $user = User::where('id',$id)->first();
        return view('admin.users.show', ['user' => $user]);
    }

    public function edit($id)
    {
        $user = User::where('id',$id)->first();
        return view('admin.users.edit', ['data' => $user]);
    }

    public function update(Request $request, $id)
    {
        $validated = $request->validate([
            'first_name' => 'required|alpha|max:255',
            'last_name' => 'required|alpha|max:255',
            'phone' => 'required|numeric|digits:10',
            'status' => 'required',
            'email' => 'required|email',
        ]);
       
            $user=User::where('id', $id)->firstorFail();
            $user->first_name = $validated['first_name'];
            $user->last_name = $validated['last_name'];
            $user->phone = $validated['phone'];
            $user->status = $validated['status'];
            $user->email = $validated['email'];
            $res = $user->save();
        if ($res) {
            return redirect()->back()->with('success', 'Successfully updated the user.');
        } else {
            return redirect()->back()->with('error', 'Failed to update the user. Please try again.');
        }
    }

    public function destroy($id)
    {
        $user=User::where('id', $id)->firstorfail();
        Storage::disk('public')->delete($user->photo);
        $res = $user->delete();
        if ($res) {
            return redirect()->back()->with('success', 'Successfully deleted the user.');
        } else {
            return redirect()->back()->with('error', 'Failed to delete the user. Please try again.');
        }
    }
}
