<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Package;
use Carbon\Carbon;
use Validator;
use Illuminate\Http\Request;

class PackageController extends Controller
{

    public function index()
    {
        return view('admin.packages.index',['packages' => Package::orderby("created_at","asc")->paginate(10)]);
    }

    public function create()
    {
        return view('admin.packages.create');
    }
    public function store(Request $request)
    {
        $user = auth()->guard('admin')->user();
        $validated = $request->validate([
            'name' => 'required|alpha|unique:packages,name,slug|min:4',
            'price' => 'required|numeric',
            'status' => 'required',
            'duration' => 'required|numeric|min:1',
        ]);
        $package = new Package;
        $package->name = $validated['name'];
        $package->price = $validated['price'];
        $package->status = $validated['status'];
        $package->duration = $validated['duration'];
        $package->slug =  $package->name;
        $res = $package->save();
        if ($res) {
            return redirect()->back()->with('success', 'Successfully added new package.');
        } else {
            return redirect()->back()->with('error', 'Failed to add new package. Please try again.');
        }
    }

    public function show(Package $package)
    {
        return view('admin.packages.show', ['data' => $package]);
    }

    public function edit(Request $request, $uuid)
    {
        $packages = Package::where('id', $uuid)->firstorFail();
        return view('admin.packages.edit', ['data' => $packages]);
    }

    public function update(Package $package, Request $request)
    {
        $validated = $request->validate([
            'name' => 'required|alpha',
            'price' => 'required|numeric',
            'status' => 'required',
            'duration' => 'required|numeric|min:1',
        ]);
        $package->name = $validated['name'];
        $package->price = $validated['price'];
        $package->status = $validated['status'];
        $package->duration = $validated['duration'];
        $package->slug = $package->name;
       
        $res = $package->save();
        if ($res) {
            return redirect()->back()->with('success', 'Successfully updated the package.');
        } else {
            return redirect()->back()->with('error', 'Failed to update the package. Please try again.');
        }
    }

    public function destroy(Package $package, Request $request)
    {
        $res = $package->delete();
        if ($res) {
            return redirect()->back()->with('success', 'Successfully deleted the package.');
        } else {
            return redirect()->back()->with('error', 'Failed to delete the package. Please try again.');
        }
    }

    

}
