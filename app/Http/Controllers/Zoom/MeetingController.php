<?php

namespace App\Http\Controllers\Zoom;

use App\Http\Controllers\Controller;
use App\Http\Traits\ZoomJWT;
use App\RecordedVideo;
use App\PackageRecordedVideo;
use App\LiveVideo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class MeetingController extends Controller
{
    use ZoomJWT;

    const MEETING_TYPE_INSTANT = 1;
    const MEETING_TYPE_SCHEDULE = 2;
    const MEETING_TYPE_RECURRING = 3;
    const MEETING_TYPE_FIXED_RECURRING_FIXED = 8;

    public function list(Request $request)
    {
        $path = 'users/me/meetings';
        $response = $this->zoomGet($path);

        $data = json_decode($response->body(), true);
        $data['meetings'] = array_map(function (&$m) {
            $m['start_at'] = $this->toUnixTimeStamp($m['start_time'], $m['timezone']);
            return $m;
        }, $data['meetings']);

        return [
            'success' => $response->ok(),
            'data' => $data,
        ];
    }

    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'topic' => 'required|string',
            'duration' => 'required',
            'password' => 'required',
            'start_time' => 'required|date',
            'agenda' => 'string|nullable',
        ]);
        
        if ($validator->fails()) {
            return [
                'success' => false,
                'data' => $validator->errors(),
            ];
        }
        $data = $validator->validated();
        $path = "users/me/meetings";
        $response = $this->zoomPost($path, [
            'topic' => $data['topic'],
            'type' => self::MEETING_TYPE_SCHEDULE,
            'start_time' => $this->toZoomTimeFormat($data['start_time']),
            'duration' => $data['duration'],
            'agenda' => $data['agenda'],
            'password' => $data['password'],
            'settings' => [
                'host_video' => false,
                'participant_video' => false,
                'waiting_room' => true,
            ]
        ]);


        return [
            'success' => $response->status() === 201,
            'data' => json_decode($response->body(), true),
        ];
    }

    public function get(Request $request)
    {
        $path = 'meetings/' . $request->id;
        $response = $this->zoomGet($path);

        $data = json_decode($response->body(), true);
        if ($response->ok()) {
            $data['start_at'] = $this->toUnixTimeStamp($data['start_time'], $data['timezone']);
        }

        return [
            'success' => $response->ok(),
            'data' => $data,
        ];
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'topic' => 'required|string',
            'start_time' => 'required|date',
            'agenda' => 'string|nullable',
            'duration' => 'required|numeric',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return [
                'success' => false,
                'data' => $validator->errors(),
            ];
        }
        $data = $validator->validated();
        $liveVideo = LiveVideo::where('meeting_id', $request->id)->first();
        if($liveVideo!=null){
            $liveVideo->title=$data['topic'];
            $liveVideo->description=$data['agenda'];
            $liveVideo->schedule=date('Y-m-d\TH:i', strtotime($data['start_time']));
            $liveVideo->save();
        }
        $path = 'meetings/' . $request->id;
        $response = $this->zoomPatch($path, [
            'topic' => $data['topic'],
            'type' => self::MEETING_TYPE_SCHEDULE,
            'start_time' => (new \DateTime($data['start_time']))->format('Y-m-d\TH:i:s'),
            'duration' => $data['duration'],
            'agenda' => $data['agenda'],
            'password' => $data['password'],
            'settings' => [
                'host_video' => false,
                'participant_video' => false,
                'waiting_room' => true,
            ]
        ]);
        dd(json_decode($response->body()));exit;
        return [
            'success' => $response->status() === 204,
            'data' => json_decode($response->body(), true),
        ];
    }

    public function delete(Request $request, string $id)
    {
        $path = 'meetings/' . $id;
        $response = $this->zoomDelete($path);

        return [
            'success' => $response->status() === 204,
            'data' => json_decode($response->body(), true),
        ];
    }

    public function edit($id)
    {
        return view("edit",['id'=>$id]);
    }

    public function getRec(Request $request)
    {
        
        $path = 'https://api.zoom.us/v2/meetings/'.$request->id.'/recordings';
        $response = $this->zoomRecGet($path);
      
        //dd($response->getStatusCode());exit;
        $data = json_decode($response->body(), true);
        //dd($data);exit;
        if(($response->getStatusCode()!=404) && ($response->getStatusCode()!=400))
        {
            $recs=$data["recording_files"];
            $video=$recs[0];
            $recorded_video=RecordedVideo::where('meeting_id',$request->id)->first();
            if($recorded_video!=null)
            {
                //dd('Exists in DB');
                return [
                    // 'success' => $response->ok(),
                    'success' => "Data already exists in our Recordings list.",
                    'data' => $data,
                ]; 
            }else{
                //dd('Not Exists in DB');
                $live_video=LiveVideo::where('id',$request->video)->firstorfail();
                // dd($live_video->packages[0]);
                // dd($video["download_url"]);
                $link=$video["download_url"];
                $video=new RecordedVideo();
                $video->title=$data["topic"];
                $video->description=$data["topic"];
                $video->schedule=$live_video->schedule;
                $video->meeting_id=$request->id;
                $video->status=1;
                $video->video_link=$link;
                $res=$video->save();
                if($res){
                    foreach($live_video->packages as $package){
                        $pack_video= new PackageRecordedVideo();
                        $pack_video->package_id=$package->id;
                        $pack_video->recorded_video_id=$video->id;
                        $res=$pack_video->save();
                    }
                }
                return [
                    // 'success' => $response->ok(),
                    'success' => "Succesfully Transferred",
                    'data' => $data,
                ];
            }
        }else{
            return [
                'success' => "No Recording Exists",
                'data' => $data,
            ];
        }
    }
}
