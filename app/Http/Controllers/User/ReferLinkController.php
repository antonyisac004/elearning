<?php

namespace App\Http\Controllers\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Referral;
use App\ReferLink;
use Illuminate\Http\Request;
use Mail;
use App\Mail\UserReferlinkMail;

class ReferLinkController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $user=Auth::user();
        $link=ReferLink::where('user_id',$user->id)->firstorfail();
        return view('user.referral_link.index',['link'=>$link->link]);
    }
    public function referralMail(Request $request)
    {
        Mail::to($request->email)->send(new UserReferlinkMail);
        return response()->json(['success'=>'Mail forwarded Successfully']);
    }
}
