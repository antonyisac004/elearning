<?php

namespace App\Http\Controllers\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\RecordedVideo;
use App\PackageVideo;
use App\Package;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RecordedCourseController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {   
        $pack_video=RecordedVideo::whereHas('package_recorded_video', function($q){
            $q->where('package_id', '=', Auth::user()->package->id);
            })
            ->orderby("schedule", 'desc')
            ->where('status', '=', 1)
            ->get();
        $package=Package::where('id',Auth::user()->package_id)->firstorfail();
        return view('user.recorded-course.index',['videos'=>$pack_video,'user'=>Auth::user(),'package'=>$package->name]);
    }

    public function show(Request $request)
    {
        $recordedVideo=RecordedVideo::where('id',$request->id)->firstorfail();
        return view('user.recorded-course.show',['video' => $recordedVideo]);
    }
}
