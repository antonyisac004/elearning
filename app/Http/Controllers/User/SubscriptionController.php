<?php

namespace App\Http\Controllers\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\User;
use App\Package;
use Illuminate\Support\Facades\Auth;

class SubscriptionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {   
        $user=Auth::user();
        $userpackage=Package::where('id',$user->package_id)->firstorfail();
        $packages=Package::where("status",1)->orderBy("created_at", "asc")->get();
        return view('user.subscriptions.index',['packages'=>$packages,'userpackage'=>$userpackage,'user'=>$user]);
    }
}
