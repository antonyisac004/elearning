<?php

namespace App\Http\Controllers\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Package;
use App\Referral;
use App\ReferLink;
use Illuminate\Http\Request;

class ReferralController extends Controller
{
    public function refferal($code)
    {
        session(['ref'=>$code]);
        $ref_user=ReferLink::where('code',$code)->firstorfail();
        session(['ref_user'=>$ref_user->user_id]);
        return view('referral_signup.index',['packages' => Package::where("status",1)->orderBy("created_at", "asc")->get()]);
    }
}
