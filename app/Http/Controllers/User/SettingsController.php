<?php

namespace App\Http\Controllers\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Http\Request;

class SettingsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $user=Auth::user();
        return view('user.profile.settings',['data'=>$user]);
    }

    public function update(Request $request)
    {
        $validated = $request->validate([
            'password' => 'min:8|required_with:password_confirmation|string|confirmed|
            regex:/[a-z]/|regex:/[A-Z]/|regex:/[0-9]/|regex:/[@$!%*#?&]/',
        ]);
        $user=Auth::user();
        $user->password = bcrypt($validated['password']);
        $res=$user->save();
        if ($res) {
            return redirect()->back()->with('success', 'Successfully resetted your password.');
        } else {
            return redirect()->back()->with('error', 'Failed to reset your password. Please try again.');
        }
    }

}
