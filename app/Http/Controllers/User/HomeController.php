<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\LiveVideoPackage;
use App\PackageRecordedVideo;
use App\Package;
use App\LiveVideo;
use App\RecordedVideo;
use App\User;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {   
        $user=Auth::user();
        $now=Carbon::now();
        $renewal_date = Carbon::parse(Auth::user()->renewal);
        if($renewal_date >= $now){
        $live_video=LiveVideo::whereHas('live_video_package', function($q){
                                $q->where('package_id', '=', Auth::user()->package->id);
                                })
                            ->where('status', '=', 1)
                            ->where( 'schedule', '>=', Carbon::now()->subMinutes(30)->toDateTimeString())
                            ->get();
        $recorded_videos=RecordedVideo::whereHas('package_recorded_video', function($q){
                                $q->where('package_id', '=', Auth::user()->package->id);
                                })
                            ->orderby("schedule", 'desc')
                            ->where('status', '=', 1)
                            ->get();
                            
        $packages=Package::where('status',1)->orderby('price','asc')->get();
        return view('user.home',['user'=>$user,'packages' => $packages,'live_video'=>$live_video,
                                'recorded_videos'=>$recorded_videos]);
        }else{
        $packages=Package::where('status',1)->orderby('price','asc')->get();
        return view('user.renewal',['user'=>$user,'packages' => $packages,]);

        }
 
    }
}
