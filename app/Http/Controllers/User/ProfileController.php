<?php

namespace App\Http\Controllers\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\User;
use Carbon\Carbon;
use App\Package;
use Storage;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $user=Auth::user();
        $activities = \LogUserActivity::logActivityLists(5);
        $package=Package::where('id',$user->package_id)->firstorfail();
        if(session('opt')==1){
            session(['opt'=>""]);
            return view('user.profile.index',['user'=>$user,'package'=>$package->name,'activities'=>$activities,'message'=>"Profile updated successfully!"]);
        }else{
            return view('user.profile.index',['user'=>$user,'package'=>$package->name,'activities'=>$activities,'message'=>""]);
        }
        
    }
   
    public function edit()
    {
        $user=Auth::user();
        return view('user.profile.edit',['data'=>$user]);
    }

    public function update(Request $request)
    {
        $validated = $request->validate([
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'phone' => 'required|min:10|numeric',
            'email' => 'required|email',
            'photo' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg',
        ]);
        
        $user=Auth::user();
        $user->first_name=$validated['first_name'];
        $user->last_name=$validated['last_name'];
        $user->phone=$validated['phone'];
        $user->email=$validated['email'];

        if ($request->hasFile('photo')) {
            Storage::disk('public')->delete($user->photo);
            $path = $request->file('photo')->storeAs(
                'upload/user',
                time() . '_' . $request->photo->getClientOriginalName(),
                'public'
            );
            $user->photo = $path;
        }
        $res=$user->save();
        if ($res) {
            \LogUserActivity::addToLog('Updated Profile.');
            return redirect('user/profile')->with('success', 'Successfully updated your profile.');
        } else {
            return redirect()->back()->with('error', 'Failed to update the profile. Please try again.');
        }
    }
}
