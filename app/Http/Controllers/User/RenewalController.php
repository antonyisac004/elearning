<?php

namespace App\Http\Controllers\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\User;
use Carbon\Carbon;
use App\Package;
use App\Payment;
use Illuminate\Http\Request;

class RenewalController extends Controller
{
    public function savePayment(Request $request) 
    {
        $payment = new Payment;
        $payment->payment_id=$request->razorpay_payment_id;
        $payment->user_id=Auth::user()->id;
        $payment->amount=$request->amount;
        $res = $payment->save();
        // print_r($input);
        if($res){
            $user=Auth::user();
            $user->package_id=$request->subscription;
            $pack=Package::where('id',$request->subscription)->firstorfail();
            $date=Carbon::now()->addMonths($pack->duration)->toDateString();
            $user->renewal = $date;
            $user->update();
        }
        exit;
    }

    public function getData(Request $request)
    {
        $package=Package::where('id',$request->id)->firstorfail();
        return response()->json(['data'=>$package]);
    }
}
