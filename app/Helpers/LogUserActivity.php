<?php


namespace App\Helpers;

use Request;
use App\UserActivityLog as LogActivityModel;


class LogUserActivity
{
    public static function addToLog($subject)
    {
        $post = Request::all();
        if (array_key_exists("password", $post)) {
            unset($post['password']);
        }
        if (array_key_exists("password_confirmation", $post)) {
            unset($post['password_confirmation']);
        }
        if (array_key_exists("old_password", $post)) {
            unset($post['old_password']);
        }
        if (array_key_exists("confirm_password", $post)) {
            unset($post['confirm_password']);
        }
        $log = [];
        $log['subject'] = $subject;
        $log['url'] = Request::fullUrl();
        $log['method'] = Request::method();
        $log['ip'] = Request::ip();
        $log['request'] = json_encode($post);
        $log['agent'] = Request::header('user-agent');
        $log['user_id'] = auth()->check() ? auth()->user()->id : NULL;
        LogActivityModel::create($log);
    }


    public static function logActivityLists($limit = 10)
    {
        return LogActivityModel::latest()->where('user_id', auth()->user()->id)->paginate($limit)->onEachSide(1);
    }
    public static function logActivityListsbyId($id, $limit = 10)
    {
        return LogActivityModel::latest()->where('user_id', $id)->paginate($limit)->onEachSide(1);
    }

}