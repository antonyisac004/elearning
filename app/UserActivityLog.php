<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserActivityLog extends Model
{
    protected $fillable = [
        'subject', 'url', 'method', 'ip', 'agent', 'user_id', 'request'
    ];

    public function administrator()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
}
