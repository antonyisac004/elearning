<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    protected $table="packages";
    use Uuids;
    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','slug','name', 'price', 'status', 
    ];

    public function livevideos()
    {
        return $this->belongsToMany('App\LiveVideo');
    }

    public function recordedvideos()
    {
        return $this->belongsToMany('App\RecordedVideo');
    }

    public function packageFeatures()
    {
        return $this->hasMany('App\PackageFeature')->where('status',1);
    }
}
